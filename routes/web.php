<?php

use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\DiaryController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\JournalController;
use App\Http\Controllers\JournalSubjectPageController;
use App\Http\Controllers\LessonController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\ScheduleController;
use App\Http\Controllers\SchoolClassController;
use App\Http\Controllers\SetItemsPerPage;
use App\Http\Controllers\StudyYearController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\TeacherFeedbackController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'pages.home')->name('home');

Route::view('/history', 'pages.history');

Route::view('/contact_us', 'pages.contact_us');

Route::view('/not_available', 'pages.not_available');

Route::post('/items_per_age', SetItemsPerPage::class);

Route::post('/contact_us', [ContactUsController::class, 'send'])->name('contact-us');

Route::middleware('auth')->group(function () {
    Route::resource('users', UserController::class)->except(['create', 'edit']);

    Route::get('/users/create/{type}/{user?}', [UserController::class, 'create'])
        ->name('users.create')
        ->where('type', '^parent|student|employee$');

    Route::get('/users/{user}/edit/{type}', [UserController::class, 'edit'])
        ->name('users.edit')
        ->where('type', '^parent|student|employee$');
});

Route::middleware('auth')->group(function () {
    Route::resource('news', NewsController::class)->except(['index', 'show']);
});

Route::get('/news/sorted', [NewsController::class, 'indexSorted'])
    ->name('news.sorted');

Route::get('/news/search', [NewsController::class, 'search'])
    ->name('news.search');

Route::get('/news/category/{category}', [NewsController::class, 'getByCategoryName'])
    ->name('news.category-name')
    ->where(['category' => '^[АаБбВвГгҐґДдЕеЄєЖжЗзИиІіЇїЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЬьЮюЯя\'` -]+$']);

Route::resource('news', NewsController::class)->only(['index', 'show']);

Route::middleware('auth')->group(function () {
    Route::resource('events', EventController::class)->except(['index', 'show']);
});

Route::resource('events', EventController::class)->only(['index']);

Route::get('/events/sorted', [EventController::class, 'indexSorted'])
    ->name('events.sorted');

Route::get('/events/archive', [EventController::class, 'indexTrashed'])
    ->name('events.archive');

Route::get('/events/search', [EventController::class, 'search'])
    ->name('events.search');

Route::get('/events/category/{category}', [EventController::class, 'getByCategoryName'])
    ->name('events.category-name')
    ->where(['category' => '^[АаБбВвГгҐґДдЕеЄєЖжЗзИиІіЇїЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЬьЮюЯя\'` -]+$']);

Route::middleware('auth')->group(function () {
    Route::resource('subjects', SubjectController::class)->except(['index', 'show']);
});

Route::get('/subjects/class/{classNum?}', [SubjectController::class, 'index'])
    ->name('subjects.index');

Route::resource('subjects', SubjectController::class)->only(['show']);

Route::get('/employees', [EmployeeController::class, 'index'])
    ->name('employees.index');

Route::get('/employees/{employee}', [EmployeeController::class, 'show'])
    ->name('employees.show');

Route::middleware('auth')->group(function () {
    Route::resource('school-classes', SchoolClassController::class)->except(['index', 'show']);
});

Route::resource('school-classes', SchoolClassController::class)->only(['index', 'show']);

Route::middleware('auth')->group(function () {
    Route::get('/journals/show/{school_class}/{study_year}', [JournalController::class, 'show'])
        ->name('journals.show');
    Route::get('/journals/edit/{school_class}/{study_year}', [JournalController::class, 'edit'])
        ->name('journals.edit');
    Route::put('/journals/update/{school_class}/{study_year}', [JournalController::class, 'update'])
        ->name('journals.update');
    Route::delete('/journals/delete/{school_class}/{study_year}', [JournalController::class, 'destroy'])
        ->name('journals.destroy');
    Route::get('/journals/index-type/{type}', [JournalController::class, 'index'])
        ->name('journals.index')
        ->where('type', '^myJournals|allJournals$');
    Route::get('/journals/study-year/{study_year}/{is_journal_create}', [JournalController::class, 'getStudyYearClasses']);
    Route::resource('journals', JournalController::class)->only(['create', 'store']);

    Route::get('/journal-subject-pages/{journalSubjectPage}', [JournalSubjectPageController::class, 'show'])
        ->name('journal-subject-pages.show');
    Route::patch('/lessons/{lesson}', [LessonController::class, 'update'])
        ->name('lessons.update');
    Route::patch('/journal-cells', [LessonController::class, 'updateJournalCell'])
        ->name('journal-cells.update');
});

Route::middleware('auth')->group(function () {
    Route::resource('schedules', ScheduleController::class)->except(['index', 'show']);
});
Route::get('/schedules/{type}', [ScheduleController::class, 'index'])
    ->name('schedules.index')
    ->where('type', '^teachers|classes$');
Route::get('/schedules/{type}/{schedule}', [ScheduleController::class, 'show'])
    ->name('schedules.show')
    ->where('type', '^users|classes|usersOwn$');
Route::middleware('auth')->group(function () {
    Route::get('/diary/{studentId}/{weekNumber?}', [DiaryController::class, 'show'])
        ->name('diary.show');
    Route::post('/feedbacks}', [TeacherFeedbackController::class, 'store'])
        ->name('feedbacks.store');
    Route::get('study-years', [StudyYearController::class, 'create'])
        ->name('study-years.create');
    Route::post('study-years', [StudyYearController::class, 'store'])
        ->name('study-years.store');
});


require __DIR__ . '/auth.php';
