## Встановлення ПЗ

Для коректної роботи ПЗ потрібно встановити всі необхідні залежності та виконати їх налаштування:
- Встановити PHP і composer залежності
- Налаштувати сервер для обробки запитів клієнтів.
- Отримати та використати в ПЗ ключі API від сервісу Mailjet.
- Отримати ключ Google Maps API та використати його в програмному продукті.
- Налаштувати сервер бази даних MySQL та приєднати його до ПЗ.
- Заповнити базу даних необхідною інформацією.

## PHP

Для встановлення пакетів PHP можна скористатися наступною інструкцією https://www.php.net/manual/en/install.php.
Крім того, потрібно встановити composer за інструкцією: https://getcomposer.org/download/
Після цього в директорії проекту потрібно виконати наступну команду для встановлення залежностей:
```
composer install
```

## Сервер для обробки запитів клієнтів

Для обробки клієнтських запитів потрібно розгорнути веб-сервер та налаштувати підключення ПЗ до нього. Для цього можуть 
бути використанні **[Apache](https://httpd.apache.org/docs/2.4/install.html)** або
**[Nginx](https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-open-source/)**

Також для тестування можна використати вбудовану в Laravel команду,
яка автоматично приєднає ПЗ до локального сервера.
```
php artisan serve
```

## Сервіс Mailjet

Для отримання ключів API з сервісу Mailjet, які потрібні для коректної роботи програми, потрібно зареєструватися в 
сервісі або використати існуючий акаунт. Це можна зробити за наступним посиланням: https://app.mailjet.com/signin.
Потім потрібно отримати ключі API за посиланням: https://app.mailjet.com/account/apikeys.
Після цього, використовуючи ключі, потрібно заповнити змінні в файл .env наступним чином: 
```
MAIL_MAILER=smtp
MAIL_HOST=in-v3.mailjet.com
MAIL_PORT=587
MAIL_USERNAME=ea25b4961111111b3880fa1a1cf57e06
MAIL_PASSWORD=cb27814ed11111905520398e058cfa78
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS="email@gmail.com"
MAIL_FROM_NAME="${APP_NAME}"

MAILJET_APIKEY=ea25b4961b75663b3880fa1a1cf57e06
MAILJET_APISECRET=cb27814ed19241905520398e058cfa78
```

## Google Maps API

Для коректної роботи карти, яка знаходиться на сторінці зворотного зв'язку потрібно використати ключ Google Maps API. 
Його можна отримати, використавши інструкцію за посилання: https://developers.google.com/maps/get-started#enable-api-sdk.
Після генерації ключа його потрібно вставити в текст ПЗ у файлі, що розташований за шляхом: 
resources/views/basic_layout/scripts_links.blade.php замість API_KEY, так як прикладі нижче:
```
<script async
        src="https://maps.googleapis.com/maps/api/js?key=API_KEYcallback=initMap">
</script>
```
## База даних MySQL

Для розгортання сервера баз даних MySQL потрібно використати інструкцію за посиланням:
https://dev.mysql.com/doc/mysql-getting-started/en/.
Після налаштування сервера потрібно ввести дані бази даних у файл .env схожим чином:
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=school_platform
DB_USERNAME=root
DB_PASSWORD=password
```

## Наповнення бази даних

У разі використання дампу з тестовими даними (приєднаний до архіву з кодом ПЗ) потрібно виконати наступну команду, для цього потрібно мати встановлену 
утиліту mysqldump:
```
mysql -u username -p target_database < dump.sql
```
В іншому разі потрібно виконати наступну команду, яка створить всі необхідні таблиці та заповнить їх даними, використовуючи програмні сідери:
```
php artisan migrate --seed
```
