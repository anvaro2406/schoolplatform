<?php

namespace App\Models;

use App\Notifications\CustomResetPasswordNotification;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'photo_filepath',
        'firstname',
        'lastname',
        'patronymic',
        'gender',
        'birth_date',
        'address',
        'phone',
        'email',
        'password',
        'group',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the employee properties associated with the user.
     *
     * @return HasOne
     */
    public function employeeProperty(): HasOne
    {
        return $this->hasOne(EmployeeProperty::class);
    }

    /**
     * @return BelongsToMany
     */
    public function parents(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'parent_has_children', 'child_id', 'parent_id');
    }

    /**
     * @return BelongsToMany
     */
    public function children(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'parent_has_children', 'parent_id', 'child_id');
    }

    /**
     * Send a password reset notification to the user.
     *
     * @param string $token
     * @return void
     */
    public function sendPasswordResetNotification($token): void
    {
        $url = url(route('password.reset', [
            'token' => $token,
            'email' => $this->email,
        ], false));

        $this->notify(new CustomResetPasswordNotification($url));
    }

    /**
     * Get the news written by the user.
     *
     * @return HasMany
     */
    public function news(): HasMany
    {
        return $this->hasMany(News::class);
    }

    /**
     * Get the abstract subjects taught by the user.
     *
     * @return BelongsToMany
     */
    public function abstractSubjects(): BelongsToMany
    {
        return $this->belongsToMany(AbstractSubject::class, 'abstract_subject_teacher', 'teacher_id', 'abstract_subject_id');
    }

    /**
     * Get the school class in which student is studying or head teacher is teaching.
     *
     * @return BelongsTo
     */
    public function schoolClass(): BelongsTo
    {
        return $this->belongsTo(SchoolClass::class, 'class_id');
    }

    /**
     * Get the subjects that the user (teacher) is taught.
     *
     * @return HasMany
     */
    public function subjects(): HasMany
    {
        return $this->hasMany(Subject::class, 'teacher_id');
    }

    /**
     * Get the feedbacks that the user (teacher) wrote.
     *
     * @return HasMany
     */
    public function writtenFeedbacks(): HasMany
    {
        return $this->hasMany(TeacherFeedback::class, 'teacher_id');
    }

    /**
     * Get the feedbacks that the user (student) received.
     *
     * @return HasMany
     */
    public function receivedFeedbacks(): HasMany
    {
        return $this->hasMany(TeacherFeedback::class, 'student_id');
    }

    /**
     * Get the lessons that the user (student) is studying.
     *
     * @return BelongsToMany
     */
    public function lessons(): BelongsToMany
    {
        return $this->belongsToMany(Lesson::class, 'student_lesson', 'student_id', 'lesson_id')
            ->withPivot('mark', 'is_absent')
            ->as('journalCell');
    }

    /**
     * Get the full name of the user.
     *
     * @return Attribute
     */
    protected function fullName(): Attribute
    {
        return Attribute::make(
            get: fn () => "$this->lastname $this->firstname $this->patronymic" ,
        );
    }
}
