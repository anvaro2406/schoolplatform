<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class AbstractSubject extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'class_number',
        'year_studyhours',
        'week_studyhours',
        'description',
        'title',
    ];

    /**
     * Get teachers who taught the subject
     *
     * @return BelongsToMany
     */
    public function teachers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'abstract_subject_teacher', 'abstract_subject_id', 'teacher_id');
    }

    /**
     * Get the school program that owns the subject.
     *
     * @return BelongsTo
     */
    public function schoolProgram(): BelongsTo
    {
        return $this->belongsTo(SchoolProgram::class);
    }

    /**
     * Get subjects that are created using this abstract subject.
     *
     * @return HasMany
     */
    public function subjects(): HasMany
    {
        return $this->hasMany(Subject::class, 'abstract_subject_id');
    }
}
