<?php

namespace App\Models;

use App\Enums\DayEnum;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;

class Schedule extends Model
{

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'day' => DayEnum::class,
    ];

    /**
     * Get the formatted schedule for a list of subjects.
     *
     * @param $subjects
     * @return Collection
     */
    private static function getFormattedScheduleForSubjects($subjects): Collection
    {
        $subjectsIds = $subjects->pluck('id');
        $scheduleSlots = static::whereIn('subject_id', $subjectsIds)->get();
        $scheduleSlots = collect($scheduleSlots->toArray());

        return $scheduleSlots->groupBy(['day', 'time_slot_id'])->map(function ($timeSlots) {
            return $timeSlots->map(function ($subjects) {
                return [
                    'timeSlot' => TimeSlot::findOrFail($subjects->first()['time_slot_id']),
                    'subjects' => Subject::whereIn('id', $subjects->pluck('subject_id'))->get(),
                ];
            });
        });
    }

    /**
     * Get the subject which is taught in this schedule slot.
     *
     * @return BelongsTo
     */
    public function subject(): BelongsTo
    {
        return $this->belongsTo(Subject::class);
    }

    /**
     * Get the time slot which is used for this schedule slot.
     *
     * @return BelongsTo
     */
    public function timeSlot(): BelongsTo
    {
        return $this->belongsTo(TimeSlot::class);
    }

    /**
     * Get all schedule slots formatted in the array for specified user.
     *
     * @param $userId
     * @return Collection
     * @throws Exception
     */
    public static function getPersonalSchedule($userId): Collection
    {
        $user = User::findOrFail($userId);

        if ($user->hasRole('student')) {
            $subjects = $user->schoolClass->subjects->filter(function (Subject $subject) use ($user) {
                return $subject->group == 'no_groups' || $subject->group == $user->group;
            });

            return static::getFormattedScheduleForSubjects($subjects);
        } elseif ($user->hasRole('teacher')) {
            $subjects = $user->subjects;

            return static::getFormattedScheduleForSubjects($subjects);
        } else {
            throw new Exception('User model does not correspond to any required teacher or student role.' .
                ' Model ID: ' . $userId);
        }
    }

    /**
     * Get all schedule slots formatted in the array for specified school class.
     *
     * @param $schoolClassId
     * @return Collection
     */
    public static function getSchoolClassSchedule($schoolClassId): Collection
    {
        $schoolClass = SchoolClass::findOrFail($schoolClassId);
        $subjects = $schoolClass->subjects;

        return static::getFormattedScheduleForSubjects($subjects);
    }
}
