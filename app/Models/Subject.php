<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Subject extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'group',
        'video_conference_link',
        'class_id',
        'abstract_subject_id',
        'teacher_id',
    ];

    /**
     * Get the school class for which subject is taught.
     *
     * @return BelongsTo
     */
    public function schoolClass(): BelongsTo
    {
        return $this->belongsTo(SchoolClass::class, 'class_id');
    }

    /**
     * Get the abstract subject to which this subject is belonged.
     *
     * @return BelongsTo
     */
    public function abstractSubject(): BelongsTo
    {
        return $this->belongsTo(AbstractSubject::class, 'abstract_subject_id');
    }

    /**
     * Get the teacher who is taught this subject.
     *
     * @return BelongsTo
     */
    public function teacher(): BelongsTo
    {
        return $this->belongsTo(User::class, 'teacher_id');
    }

    /**
     * Get the study year in which the subject was created.
     *
     * @return BelongsTo
     */
    public function studyYear(): BelongsTo
    {
        return $this->belongsTo(StudyYear::class, 'study_year_id');
    }

    /**
     * Get the lessons that are taught in this subject.
     *
     * @return HasMany
     */
    public function lessons(): HasMany
    {
        return $this->hasMany(Lesson::class);
    }

    /**
     * Get the schedules slots in which the subject is taught.
     *
     * @return HasMany
     */
    public function schedules(): HasMany
    {
        return $this->hasMany(Schedule::class);
    }
}
