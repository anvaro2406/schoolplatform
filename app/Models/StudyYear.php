<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class StudyYear extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'start_date',
        'end_date',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'start_date' => 'date',
        'end_date' => 'date',
    ];

    /**
     * Get the school classes created in the study year.
     *
     * @return HasMany|SchoolClass
     */
    public function schoolClasses(): HasMany|SchoolClass
    {
        return $this->hasMany(SchoolClass::class, 'study_year_id');
    }

    /**
     * Get the subjects created in the study year.
     *
     * @return HasMany|SchoolClass
     */
    public function subjects(): HasMany|SchoolClass
    {
        return $this->hasMany(Subject::class, 'study_year_id');
    }

    /**
     * Get current study year.
     *
     * @return StudyYear
     * @throws Exception
     */
    public static function getCurrentStudyYear(): StudyYear
    {
        $studyYear = static::orderByDesc('start_date')->first();

        if (Carbon::now()->between($studyYear->start_date, $studyYear->end_date) ||
            Carbon::now()->year >= $studyYear->end_date->year) {
            return $studyYear;
        } else {
            Log::error("Current study year error.");
            throw new Exception("Current study year error.", 500);
        }
    }

    /**
     * @return Collection
     */
    public function getOrderedWeeks(): Collection
    {
        $startDate = $this->start_date;
        $endDate = $this->end_date;
        $orderNumber = 1;
        $weeks = collect();

        while ($startDate->lt($endDate)) {
            $weekStart = $startDate->copy();

            if ($startDate->copy()->endOfWeek()->gt($endDate)) {
                $weekEnd = $endDate;
            } else {
                $weekEnd = $startDate->copy()->endOfWeek();
            }

            $daysDates = [];
            $weekStartCopy = $weekStart->copy();
            while ($weekStartCopy->lte($weekEnd)) {
                $dayName = $weekStartCopy->englishDayOfWeek;
                $daysDates[strtolower($dayName)] = $weekStartCopy->copy();
                $weekStartCopy->addDay();
            }

            $weeks->push(array_merge(
                [
                    'orderNumber' => $orderNumber,
                    'startDate' => $weekStart,
                    'endDate' => $weekEnd,
                ],
                $daysDates
            ));

            $startDate->endOfWeek()->addDay()->startOfDay();
            $orderNumber++;
        }

        return $weeks;
    }

    /**
     * @return array
     */
    public function getCurrentWeek(): array
    {
        $startDate = Carbon::now()->startOfWeek();
        $weeks = $this->getOrderedWeeks();

        $currentWeek = $weeks->filter(function (array $range) use ($startDate) {
            return $range['startDate'] == $startDate;
        })->first();

        if ($currentWeek == null) {
            return $weeks->last();
        } else {
            return $currentWeek;
        }
    }

    /**
     * Get the title of the study year.
     *
     * @return Attribute
     */
    protected function name(): Attribute
    {
        return Attribute::make(
            get: fn() => $this->start_date->year . "/" . $this->end_date->year,
        );
    }
}
