<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

class SchoolClass extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'classes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'study_year_id',
        'letter',
    ];

    /**
     * Get the study year in which the school class was created.
     *
     * @return BelongsTo
     */
    public function studyYear(): BelongsTo
    {
        return $this->belongsTo(StudyYear::class);
    }

    /**
     * Get the students studying in the school class.
     *
     * @return HasMany
     */
    public function students(): HasMany
    {
        return $this->hasMany(User::class, 'class_id')->whereHas('roles', function ($query) {
            return $query->where('name', 'student');
        })->orderBy('lastname')->orderBy('firstname')->orderBy('patronymic');
    }

    /**
     * Get the head teacher of the school class.
     *
     * @return HasOne
     */
    public function headTeacher(): HasOne
    {
        return $this->hasOne(User::class, 'class_id')->whereHas('roles', function ($query) {
            return $query->where('name', 'teacher');
        });
    }

    /**
     * Get the subjects that are taught to this school class.
     *
     * @return HasMany
     */
    public function subjects(): HasMany
    {
        return $this->hasMany(Subject::class, 'class_id');
    }

    /**
     * Get the number of the school class in specified study year.
     *
     * @param int $studyYearId
     * @return int
     */
    public function numberInStudyYear(int $studyYearId): int
    {
        $unFormattedNumberWithStudyYear = 1 +
            StudyYear::findOrFail($studyYearId)->start_date->year -
            $this->studyYear->start_date->year;
        if($this->trashed()){
            $unFormattedNumberWithDeletedAt = $this->deleted_at->year - $this->studyYear->start_date->year;
            if($unFormattedNumberWithDeletedAt < $unFormattedNumberWithStudyYear) {
                return 0;
            }
        }

        if($unFormattedNumberWithStudyYear > 11 || $unFormattedNumberWithStudyYear < 1){
            return 0;
        }

        return $unFormattedNumberWithStudyYear;
    }

    /**
     * Get the name of the school class in specified study year.
     *
     * @param int $studyYearId
     * @return string
     */
    public function nameInStudyYear(int $studyYearId): string
    {
        if($this->numberInStudyYear($studyYearId) == 0){
            return '';
        }

        return $this->numberInStudyYear($studyYearId) . "-" . $this->letter;
    }

    /**
     * Get the collection of school classes which were actual for provided study year.
     *
     * @param int $studyYearId
     * @return Collection
     */
    public static function getActualSchoolClasses(int $studyYearId): Collection
    {
        return static::withTrashed()->get()->filter(function (SchoolClass $schoolClass) use ($studyYearId) {
            return $schoolClass->numberInStudyYear($studyYearId) != 0;
        });
    }

    /**
     * Get the number of the school class.
     *
     * @return Attribute
     */
    protected function number(): Attribute
    {
        return Attribute::make(
            get: fn () => 1 + StudyYear::getCurrentStudyYear()->start_date->year - $this->studyYear->start_date->year,
        );
    }

    /**
     * Get the title of the school class.
     *
     * @return Attribute
     */
    protected function name(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->number . "-" . $this->letter,
        );
    }
}
