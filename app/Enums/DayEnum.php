<?php

namespace App\Enums;

use \Illuminate\Support\Collection;

enum DayEnum:string {
    case Monday = 'monday';
    case Tuesday = 'tuesday';
    case Wednesday = 'wednesday';
    case Thursday = 'thursday';
    case Friday = 'friday';
    case Saturday = 'saturday';

    public static function basicWorkDays(): Collection
    {
        $allDays = array_column(self::cases(), 'value');
        array_pop($allDays);

        return collect($allDays);
    }
}
