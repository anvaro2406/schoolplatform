<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::defaultView('basic_layout.pagination');

        setlocale(LC_TIME, config('app.locale'));

        Blade::directive('storageAsset', function ($path) {
            return "<?php echo asset('storage/' . $path ); ?>";
        });
    }
}
