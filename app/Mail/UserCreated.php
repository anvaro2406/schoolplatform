<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * User password.
     *
     * @var string
     */
    protected string $password;

    /**
     * User who send the mail.
     */
    protected User $sender;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $password, User $sender)
    {
        $this->password = $password;
        $this->sender = $sender;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->sender->email, $this->sender->firstname)
            ->subject('Створення облікового запису')
            ->view('emails.user_created')->with([
                'password' => $this->password,
                'sender' => $this->sender,
            ]);
    }
}
