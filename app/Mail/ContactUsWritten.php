<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUsWritten extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Mail subject.
     *
     * @var string
     */
    protected string $userMessageSubject;

    /**
     * Mail body.
     *
     * @var string
     */
    protected string $userMessageBody;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $userMessageSubject, string $userMessageBody)
    {
        $this->userMessageSubject = $userMessageSubject;
        $this->userMessageBody = $userMessageBody;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->subject($this->userMessageSubject)
            ->view('emails.contact_us_written')->with([
                'body' => $this->userMessageBody,
            ]);
    }
}
