<?php

namespace App\Rules;

use App\Models\AbstractSubject;
use Closure;
use Illuminate\Contracts\Validation\InvokableRule;
use Illuminate\Translation\PotentiallyTranslatedString;

class JournalSubjects implements InvokableRule
{
    /**
     * Run the validation rule.
     *
     * @param string $attribute
     * @param mixed $value
     * @param Closure(string): PotentiallyTranslatedString $fail
     * @return void
     */
    public function __invoke($attribute, $value, $fail): void
    {
        if (!is_numeric($value['id']) || !is_numeric($value['teacherId']) || $value['id'] < 1 || $value['teacherId'] < 1) {
            $fail('validation.ids')->translate();
        }

        $abstractSubjectsId = AbstractSubject::all()->pluck('id');
        $abstractSubject = AbstractSubject::find($value['id']);
        $teachersId = $abstractSubject->teachers->pluck('id');
        $groupEnum = ['no_groups', 'first_group', 'second_group'];

        if (!$abstractSubjectsId->contains($value['id'])) {
            $fail('validation.journal_abstract_subjects')->translate();
        }
        if (!in_array($value['group'], $groupEnum)) {
            $fail('validation.journal_group_enum')->translate();
        }
        if (!$teachersId->contains($value['teacherId'])) {
            $fail('validation.journal_teachers')->translate();
        }
    }
}
