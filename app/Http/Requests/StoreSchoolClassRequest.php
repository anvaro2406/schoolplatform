<?php

namespace App\Http\Requests;

use App\Models\SchoolClass;
use App\Models\StudyYear;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Validator as ValidatorFacade;

class StoreSchoolClassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->user()->hasPermissionTo('create class');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        $studentsId = User::role('student')->pluck('id');
        $headTeachersId = User::role('head-teacher')->pluck('id');

        return [
            'letter' => [
                'required',
                'regex:/^[АаБбВвГгҐґДдЕеЄєЖжЗзИиІіЇїЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЮюЯя]$/u',
                'max:1',
            ],
            'checkboxes' => ['nullable', 'array'],
            'checkboxes.*' => ['accepted'],
            'studyYear' => ['required', 'numeric', 'exists:study_years,id'],
            'isAutoGrouping' => ['nullable', 'in:yes,on,1,true'],
            'students' => ['required', 'array', 'min:1', 'max:30'],
            'students.*.id' => ['required', 'numeric', Rule::in($studentsId)],
            'students.*.group' => ['required_unless:isAutoGrouping,on', 'nullable', 'in:first_group,second_group'],
            'headTeacher' => ['required', Rule::in($headTeachersId)],
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param Validator $validator
     * @return void
     */
    public function withValidator(Validator $validator): void
    {
        $validator->after(function ($validator) {
            if ($validator->failed()) {
                return;
            }

            $schoolClassesLetters = SchoolClass::where('study_year_id', $this->input('studyYear'))
                ->get()->pluck('letter');

            if ($schoolClassesLetters->contains($this->input('letter'))) {
                $validator->errors()->add(
                    'letter',
                    trans('validation.custom.letter.not_in')
                );
            }
        });
    }
}
