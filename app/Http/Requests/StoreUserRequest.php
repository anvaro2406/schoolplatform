<?php

namespace App\Http\Requests;

use App\Rules\Words;
use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->user()->hasPermissionTo('create user');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'type' => ['required', 'regex:/^parent|student|employee$/'],
            'photo' => ['nullable', 'mimes:jpg,bmp,png,gif'],
            'firstname' => ['required', new Words(), 'max:255'],
            'lastname' => ['required', new Words(), 'max:255'],
            'patronymic' => ['required', new Words(), 'max:255'],
            'gender' => ['required', 'alpha', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'phone' => ['required_if:type,parent,employee', 'max:16', 'string', 'nullable'],
            'email' => ['required', 'string', 'email', 'unique:users', 'max:255'],
            'birth_date' => ['required', 'date', 'after:1900-01-01'],
            'studentId' => ['required_if:type,parent', 'numeric', 'min:1'],
            'roles' => ['required_if:type,employee', 'array', 'min:1'],
            'roles.*' => ['exists:roles,name'],
            'position' => ['required_if:type,employee', new Words(), 'max:255'],
            'science_degree' => [new Words(), 'max:255', 'nullable'],
            'experience' => ['required_if:type,employee', 'numeric', 'min:0', 'max:100'],
            'biography' => ['string', 'max:65535', 'nullable'],
        ];
    }
}
