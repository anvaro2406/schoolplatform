<?php

namespace App\Http\Requests;

use App\Rules\Sentences;
use App\Rules\Words;
use Illuminate\Foundation\Http\FormRequest;

class StoreNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->user()->hasPermissionTo('create news');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'category' => ['required', new Words()],
            'title' => ['required', new Sentences(), 'max:255'],
            'content' => ['required', 'max:16777215'],
            'description' => ['string', 'nullable', 'max:1024'],
        ];
    }
}
