<?php

namespace App\Http\Requests;

use App\Rules\Sentences;
use App\Rules\Words;
use Illuminate\Foundation\Http\FormRequest;

class UpdateEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('update event');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'category' => ['required', new Words()],
            'title' => ['required', new Sentences(), 'max:256'],
            'image' => ['nullable', 'mimes:jpg,bmp,png,gif'],
            'startDatetime' => ['required', 'date', 'after:tomorrow'],
            'endDatetime' => ['required', 'date', 'after:startDatetime'],
            'description' => ['required', 'string', 'max:1024'],
        ];
    }
}
