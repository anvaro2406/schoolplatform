<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class UpdateJournalCellRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->user()->hasPermissionTo('update lessons progress');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'lessonId' => ['required', 'exists:lessons,id'],
            'value' => ['string', 'regex:/^([1-9]|1[0-2]|н|в|Н|В)$/u', 'nullable'],
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param Validator $validator
     * @return void
     */
    public function withValidator(Validator $validator): void
    {
        $validator->after(function ($validator) {
            if ($validator->failed()) {
                return;
            }

            $studentsIds = DB::table('student_lesson')
                ->where('lesson_id', $this->input('lessonId'))
                ->get()->pluck('student_id');

            if ($studentsIds->contains($this->input('letterId'))) {
                $validator->errors()->add(
                    'studentId',
                    trans('validation.exists')
                );
            }
        });
    }
}
