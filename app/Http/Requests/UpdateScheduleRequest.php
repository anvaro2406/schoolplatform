<?php

namespace App\Http\Requests;

use App\Enums\DayEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->user()->hasPermissionTo('update schedule');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        $schoolClassId = $this->route('schedule');

        return [
            'checkboxes' => ['nullable', 'array'],
            'checkboxes.*' => ['accepted'],
            'timeSlots' => ['required', 'array'],
            'timeSlots.*.timeSlotId' => ['required', 'numeric', 'exists:time_slots,id'],
            'timeSlots.*.day' => ['required', 'string', Rule::in(DayEnum::basicWorkDays())],
            'timeSlots.*.subjectId' => [
                'nullable', 'numeric', Rule::exists('subjects', 'id')->where(function ($query) use ($schoolClassId) {
                    return $query->where('class_id', $schoolClassId);
                }),
            ],
        ];
    }
}
