<?php

namespace App\Http\Requests;

use App\Models\SchoolClass;
use App\Rules\JournalSubjects;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreJournalRequest extends FormRequest
{
    /**
     * Indicates if the validator should stop on the first rule failure.
     *
     * @var bool
     */
    protected $stopOnFirstFailure = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->user()->hasPermissionTo('create journal');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'studyYear' => ['required', 'numeric', 'exists:study_years,id'],
            'schoolClass' => ['required', 'numeric', 'exists:classes,id'],
            'checkboxes' => ['nullable', 'array'],
            'checkboxes.*' => ['accepted'],
            'subjects' => ['required', 'array', 'min:1'],
            'subjects.*' => [new JournalSubjects()],
        ];
    }
}
