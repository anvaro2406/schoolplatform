<?php

namespace App\Http\Requests;

use App\Models\User;
use App\Rules\Sentences;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreTeacherFeedbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->user()->hasPermissionTo('create teacher feedback');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        $studentsIds = User::role('student')->get()->pluck('id');

        return [
            'studentId' => ['required', 'numeric', Rule::in($studentsIds)],
            'subject' => ['required', new Sentences(), 'max:64', 'min:8'],
            'description' => ['required', new Sentences(), 'max:400', 'min:32'],
        ];
    }
}
