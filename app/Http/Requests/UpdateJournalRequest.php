<?php

namespace App\Http\Requests;

use App\Rules\JournalSubjects;
use Illuminate\Foundation\Http\FormRequest;

class UpdateJournalRequest extends FormRequest
{
    /**
     * Indicates if the validator should stop on the first rule failure.
     *
     * @var bool
     */
    protected $stopOnFirstFailure = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->user()->hasPermissionTo('update journal');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'checkboxes' => ['nullable', 'array'],
            'checkboxes.*' => ['accepted'],
            'subjects' => ['required', 'array', 'min:1'],
            'subjects.*' => [new JournalSubjects()],
        ];
    }
}
