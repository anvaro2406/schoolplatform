<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateLessonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->user()->hasPermissionTo('update lessons progress');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        $fieldName = $this->request->get('fieldName');
        $rules = [
            'fieldName' => ['required', 'string', Rule::in(['date', 'theme', 'homework'])],
        ];

        if ($fieldName === 'date') {
            $rules['value'] = ['date', 'nullable']; // TODO skip dates which are not in the needed study year
        } elseif ($fieldName === 'theme' || $fieldName === 'homework') {
            $rules['value'] = ['string', 'max:256', 'nullable'];
        }

        return $rules;
    }
}
