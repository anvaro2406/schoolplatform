<?php

namespace App\Http\Requests;

use App\Models\User;
use App\Rules\Sentences;
use App\Rules\Words;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateSubjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('update subject');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $teachersId = User::role('teacher')->pluck('id');

        return [
            'schoolProgram' => ['required', new Words()],
            'title' => ['required', new Sentences(), 'max:256'],
            'yearStudyHours' => ['required', 'numeric', 'min:8.75', 'max:525'],
            'weekStudyHours' => ['required', 'numeric', 'min:0.25', 'max:15'],
            'classNum' => ['required', 'numeric', 'regex:^(?:[1-9]|1[0-1])$^'],
            'description' => ['required', 'max:65535'],
            'teachers' => ['required', 'array', 'min:1'],
            'teachers.*' => [Rule::in($teachersId)],
        ];
    }
}
