<?php

namespace App\Http\Requests;

use App\Rules\Words;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $id = $this->route('user');

        return auth()->user()->hasPermissionTo('update user') || auth()->user()->id == $id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        $id = $this->route('user');

        return [
            'photo' => ['nullable', 'mimes:jpg,bmp,png,gif'],
            'firstname' => ['required', new Words(), 'max:255'],
            'lastname' => ['required', new Words(), 'max:255'],
            'patronymic' => ['required', new Words(), 'max:255'],
            'gender' => ['required', 'alpha', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'phone' => ['required_if:type,parent,employee', 'max:16', 'string', 'nullable'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($id)],
            'birth_date' => ['required', 'date', 'after:1900-01-01'],
            'roles' => ['required_if:type,employee', 'array', 'min:1'],
            'roles.*' => ['exists:roles,name'],
            'position' => ['required_if:type,employee', new Words(), 'max:255'],
            'science_degree' => [new Words(), 'max:255', 'nullable'],
            'experience' => ['required_if:type,employee', 'numeric', 'min:0', 'max:100'],
            'biography' => ['string', 'max:65535', 'nullable'],
        ];
    }
}
