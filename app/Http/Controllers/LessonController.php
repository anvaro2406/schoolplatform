<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateJournalCellRequest;
use App\Http\Requests\UpdateLessonRequest;
use App\Models\Lesson;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

class LessonController extends Controller
{
    /**
     * Update the lesson in storage.
     *
     * @param UpdateLessonRequest $request
     * @param $id
     * @return JsonResponse
     */
    public function update(UpdateLessonRequest $request, $id): JsonResponse
    {
        try {
            DB::beginTransaction();

            $validatedRequest = $request->safe();
            Lesson::findOrFail($id)->update([
                $validatedRequest['fieldName'] => $validatedRequest['value'],
            ]);

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return response()->json([
                'errors' => true,
                'message' => 'Виникла помилка під час зміни поля. Зверніться до системного адміністратора.'
            ]);
        }
        return response()->json(['message' => 'Зміна відбулась успішно.']);
    }

    /**
     * Update the journal cell in storage.
     *
     * @param UpdateJournalCellRequest $request
     * @return JsonResponse
     */
    public function updateJournalCell(UpdateJournalCellRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();

            $validatedRequest = $request->safe();
            $lesson = Lesson::findOrFail($validatedRequest['lessonId']);

            if (is_numeric($validatedRequest['value'])) {
                $lesson->students()->updateExistingPivot(
                    $validatedRequest['studentId'],
                    ['mark' => $validatedRequest['value'], 'is_absent' => true]
                );
            } else {
                $lesson->students()->updateExistingPivot(
                    $validatedRequest['studentId'],
                    ['is_absent' => $validatedRequest['value'] == null, 'mark' => null]
                );
            }

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return response()->json([
                'errors' => true,
                'message' => 'Виникла помилка під час зміни поля. Зверніться до системного адміністратора.'
            ]);
        }
        return response()->json(['message' => 'Зміна відбулась успішно.']);
    }
}
