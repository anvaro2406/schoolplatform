<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use App\Models\Category;
use App\Models\News;
use App\Models\User;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Throwable;
use function PHPUnit\Framework\isEmpty;

class NewsController extends Controller
{
    /**
     * Display a listing of the news.
     *
     * @return RedirectResponse | View
     */
    public function index(): View|RedirectResponse
    {
        $itemsPerPage = session('itemsPerPage', 10);
        $sortColumn = session('sortNewsColumn', 'dateNew');

        $news = match ($sortColumn) {
            'dateNew' => News::orderByDesc('updated_at'),
            'dateOld' => News::orderBy('updated_at'),
            'title' => News::orderBy('title'),
            'category' => News::orderBy(
                Category::select('name')
                    ->whereColumn('id', 'news.category_id')
                    ->orderByDesc('name')
                    ->limit(1)
            ),
            'author' => News::orderBy(
                User::select('firstname')
                    ->whereColumn('id', 'news.user_id')
                    ->orderByDesc('firstname')
                    ->limit(1)
            )->orderBy(
                User::select('lastname')
                    ->whereColumn('id', 'news.user_id')
                    ->orderByDesc('lastname')
                    ->limit(1)
            ),
        };

        return view('pages.news_management.news_catalogue', [
            'news' => $news->paginate($itemsPerPage),
            'categories' => Category::all(),
            'sortColumn' => $sortColumn
        ]);
    }

    /**
     * Show the form for creating news.
     *
     * @return View|RedirectResponse
     */
    public function create(): View|RedirectResponse
    {
        if (!auth()->user()->hasPermissionTo('create news')) {
            return redirect()->route('news.index');
        }

        return view(
            'pages.news_management.create_news', ['categories' => Category::all()]
        );
    }

    /**
     * Store created news in storage.
     *
     * @param StoreNewsRequest $request
     * @return string
     */
    public function store(StoreNewsRequest $request): string
    {
        try {
            DB::beginTransaction();

            $validatedNews = $request->safe()->only(['category', 'title', 'content', 'description']);

            $news = new News([
                'title' => $validatedNews['title'],
                'content' => $validatedNews['content'],
                'description' => $validatedNews['description'],
            ]);

            $category = Category::firstOrCreate(['name' => $validatedNews['category']]);

            if (auth()->user() == null) {
                throw new Exception("User not found.", 404);
            }

            $news->author()->associate(auth()->user());
            $news->category()->associate($category);
            $news->save();

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage());

            return back()->withInput()
                ->withErrors('Виникла помилка при створенні новини. Зверніться до системного адміністратора.');
        }

        return redirect()->route('news.index');
    }

    /**
     * Display the specified news.
     *
     * @param int $id
     * @return View
     */
    public function show(int $id): View
    {
        return view('pages.news_management.news_page', [
            'news' => News::find($id),
            'categories' => Category::all(),
        ]);
    }

    /**
     * Show the form for editing the specified news.
     *
     * @param int $id
     * @return RedirectResponse|View
     */
    public function edit(int $id): RedirectResponse|View
    {
        if (!auth()->user()->hasPermissionTo('update news')) {
            return redirect()->route('news.show', ['news' => $id]);
        }

        return view('pages.news_management.update_news', [
            'news' => News::find($id),
            'categories' => Category::all(),
        ]);
    }

    /**
     * Update the specified news in storage.
     *
     * @param UpdateNewsRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(UpdateNewsRequest $request, int $id): RedirectResponse
    {
        try {
            DB::beginTransaction();

            $validatedNews = $request->safe()->only(['category', 'title', 'content', 'description']);

            $news = News::findOrFail($id);

            $news->fill([
                'title' => $validatedNews['title'],
                'content' => $validatedNews['content'],
                'description' => $validatedNews['description'],
            ]);

            $category = Category::firstOrCreate(['name' => $validatedNews['category']]);

            if (auth()->user() == null) {
                throw new Exception("User not found.", 404);
            }

            $news->author()->associate(auth()->user());
            $news->category()->associate($category);
            $news->save();

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage());

            return back()->withInput()
                ->withErrors('Виникла помилка при оновлені даних новини. Зверніться до системного адміністратора.');
        }

        return redirect()->route('news.show', ['news' => $id]);
    }

    /**
     * Remove the specified news from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        if (!auth()->user()->hasPermissionTo('delete news')) {
            return redirect()->route('news.index');
        }

        try {
            if (!News::destroy($id)) {
                throw new Exception("Model not found.", 404);
            }
        } catch (Throwable $e) {
            Log::error($e->getMessage());

            return back()->withErrors(
                'Виникла помилка при видаленні новини. Зверніться до системного адміністратора.'
            );
        }

        return redirect()->route('news.index');
    }

    /**
     * Display a listing of the news by specified category.
     *
     * @param string $categoryName
     * @return View
     */
    public function getByCategoryName(string $categoryName): View
    {
        $category = Category::where('name', $categoryName)->first();

        $itemsPerPage = session('itemsPerPage', 10);

        return view('pages.news_management.news_catalogue', [
            'news' => $category->news()->paginate($itemsPerPage),
            'categories' => Category::all(),
        ]);
    }

    /**
     * Set parameter for sorting.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function indexSorted(Request $request): RedirectResponse
    {
        $validatedInput = $request->validate([
            'sortColumn' => ['required', 'regex:/^dateOld|dateNew|title|category|author$/']
        ]);

        session(['sortNewsColumn' => $validatedInput['sortColumn']]);

        return redirect()->route('news.index');
    }

    /**
     * Display a listing of the news found by specified parameter.
     *
     * @param Request $request
     * @return RedirectResponse|View
     */
    public function search(Request $request): RedirectResponse|View
    {
        $validatedInput = $request->validate([
            'searchParameter' => ['string']
        ]);

        $news = News::where('title', 'like', '%' . $validatedInput['searchParameter'] . '%')
            ->orWhere('description', 'like', '%' . $validatedInput['searchParameter'] . '%')->get();

        if ($news->isEmpty()) {
            return view('pages.news_management.news_catalogue', [
                'categories' => Category::all(),
                'isSearch' => true,
                'noResult' => true,
            ]);
        }

        return view('pages.news_management.news_catalogue', [
            'news' => $news->toQuery()->paginate(session('itemsPerPage')),
            'categories' => Category::all(),
            'isSearch' => true,
        ]);
    }
}
