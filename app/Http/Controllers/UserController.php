<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Mail\UserCreated;
use App\Models\EmployeeProperty;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;
use Throwable;

class UserController extends Controller
{
    /**
     * Store the photo of user in Storage and return filepath. If $photo is null then return empty string
     *
     * @param $photo
     * @return string
     */
    private function storePhoto($photo): string
    {
        if (isset($photo)) {
            return $photo->store('users_photo', 'public');
        } else {
            return "";
        }
    }

    /**
     * Display a listing of the users.
     *
     * @return RedirectResponse|View
     */
    public function index(): RedirectResponse|View
    {
        if (!auth()->user()->hasPermissionTo('view user list')) {
            return redirect()->route('home');
        }

        $itemsPerPage = session('itemsPerPage', 10);

        return view('pages.user_management.users_catalogue', ['users' => User::paginate($itemsPerPage)]);
    }

    /**
     * Show the form for creating a new user.
     *
     * @param $type
     * @param null $id
     * @return RedirectResponse|View
     */
    public function create($type, $id = null): RedirectResponse|View
    {
        if (!auth()->user()->hasPermissionTo('create user')) {
            return redirect()->route('home');
        }

        return view(
            'pages.user_management.create_user',
            [
                'roles' => Role::whereNotIn('name', ['parent', 'student'])->get(),
                'type' => $type,
                'studentId' => $id
            ]
        );
    }

    /**
     * Store a newly created user in storage.
     *
     * @param StoreUserRequest $request
     * @return RedirectResponse
     */
    public function store(StoreUserRequest $request): RedirectResponse
    {
        try {
            DB::beginTransaction();

            $password = Str::random(8);

            $user = User::create($request->safe()->merge([
                'password' => Hash::make($password),
                'photo_filepath' => $this->storePhoto($request->file('photo'))
            ])->only([
                'firstname', 'lastname', 'patronymic', 'gender', 'address', 'phone', 'email', 'birth_date',
                'password', 'photo_filepath'
            ]));

            $userType = $request->safe()->only('type');

            if ($userType['type'] == 'employee') {
                $user->syncRoles($request->safe()->only(['roles']));

                $employeeProperties = new EmployeeProperty($request->safe()->only([
                    'position', 'science_degree', 'experience', 'biography'
                ]));

                $user->employeeProperty()->save($employeeProperties);
            } elseif ($userType['type'] == 'parent') {
                $user->assignRole('parent');

                $user->children()->attach($request->safe()->only('studentId'));
            } else {
                $user->assignRole('student');
            }
            DB::commit();

            Mail::to($user)->send(new UserCreated($password, $request->user()));
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage());

            return back()->withInput()
                ->withErrors('Виникла помилка при створенні користувача. Зверніться до системного адміністратора.');
        }

        return redirect()->route('users.index');
    }

    /**
     * Display the user profile.
     *
     * @param int $id
     * @return View
     */
    public function show(int $id): View
    {
        return view('pages.user_management.user_profile', ['user' => User::find($id)]);
    }

    /**
     * Show the form for editing the user data.
     *
     * @param int $id
     * @param string $type
     * @return RedirectResponse|View
     */
    public function edit(int $id, string $type): RedirectResponse|View
    {
        if (!auth()->user()->hasPermissionTo('update user') && auth()->user()->id != $id) {
            return redirect()->route('home');
        }

        $user = User::find($id);

        return view('pages.user_management.update_user', [
            'user' => $user,
            'employeeProperties' => $user->employeeProperty(),
            'type' => $type,
            'roles' => Role::whereNotIn('name', ['parent', 'student'])->get(),
        ]);
    }

    /**
     * Update the user data in storage.
     *
     * @param UpdateUserRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(UpdateUserRequest $request, int $id): RedirectResponse
    {
        try {
            DB::beginTransaction();

            $user = User::find($id);

            Storage::disk('public')->delete($user->photo_filepath);

            $user->fill($request->safe()->merge([
                'photo_filepath' => $this->storePhoto($request->file('photo'))
            ])->only([
                'firstname', 'lastname', 'patronymic', 'gender', 'address', 'phone', 'email', 'birth_date', 'photo_filepath'
            ]));

            if ($request->filled('position')) {
                $user->syncRoles($request->safe()->only(['roles']));

                $employeeProperties = $user->employeeProperty;

                $employeeProperties->fill($request->safe()->only([
                    'position', 'science_degree', 'experience', 'biography'
                ]));

                $employeeProperties->save();
            }

            $user->save();

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage());

            return back()->withInput()
                ->withErrors('Виникла помилка при оновлені даних користувача. Зверніться до системного адміністратора.');
        }

        if (auth()->user()->hasPermissionTo('view user list'))
            return redirect()->route('users.index');
        else {
            return redirect()->route('home');
        }
    }

    /**
     * Remove the user from storage (soft delete).
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        if (!auth()->user()->hasPermissionTo('delete user') && auth()->user()->id != $id) {
            return redirect()->route('home');
        }

        try {
            User::destroy($id);
        } catch (Throwable $e) {
            Log::error($e->getMessage());

            return back()->withErrors(
                'Виникла помилка при видаленні користувача. Зверніться до системного адміністратора.'
            );
        }

        if (auth()->user()->hasPermissionTo('view user list'))
            return redirect()->route('users.index');
        else {
            Auth::guard('web')->logout();

            session()->invalidate();
            session()->regenerateToken();

            return redirect()->route('home');
        }
    }
}
