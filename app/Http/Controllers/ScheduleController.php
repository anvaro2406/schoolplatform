<?php

namespace App\Http\Controllers;

use App\Enums\DayEnum;
use App\Http\Requests\StoreScheduleRequest;
use App\Http\Requests\UpdateScheduleRequest;
use App\Models\Schedule;
use App\Models\SchoolClass;
use App\Models\StudyYear;
use App\Models\Subject;
use App\Models\TimeSlot;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Throwable;

class ScheduleController extends Controller
{
    /**
     * Insert the schedule slots in storage.
     *
     * @param $scheduleSlots
     * @return void
     */
    private function insertScheduleSlots($scheduleSlots): void
    {
        $formattedScheduleSlots = $scheduleSlots->map(function (array $scheduleSlot) {
            return [
                'subject_id' => $scheduleSlot['subjectId'],
                'time_slot_id' => $scheduleSlot['timeSlotId'],
                'day' => $scheduleSlot['day'],
            ];
        });
        Schedule::insert($formattedScheduleSlots->toArray());
    }

    /**
     * Delete the schedule slots of the specified school class from storage.
     *
     * @param $schoolClassId
     * @return void
     */
    private function deleteScheduleSlots($schoolClassId): void
    {
        $schoolClass = SchoolClass::findOrFail($schoolClassId);
        $subjectsIds = $schoolClass->subjects->pluck('id');

        Schedule::whereIn('subject_id', $subjectsIds)->delete();
    }

    /**
     * Display a listing of the schedules for the specified type of owners.
     *
     * @param $type
     * @return View
     */
    public function index($type): View
    {
        if($type == 'classes'){
            $scheduleOwners = SchoolClass::all()->pluck('name', 'id');
        }else{
            $scheduleOwners = User::role('teacher')->get()->pluck('full_name', 'id');
        }

        return view('pages.schedules.schedules_catalogue', [
            'type' => $type,
            'scheduleOwners' => $scheduleOwners,
        ]);
    }

    /**
     * Show the form for creating a new schedule.
     *
     * @return RedirectResponse|View
     */
    public function create(): RedirectResponse|View
    {
        if (!auth()->user()->hasPermissionTo('create schedule')) {
            return redirect()->route('schedules.index', ['type' => 'classes']);
        }

        $currentStudyYear =  StudyYear::getCurrentStudyYear();
        $filteredSchoolClasses = SchoolClass::all()->filter(function (SchoolClass $schoolClass) use ($currentStudyYear) {
            $subjectsIds = $schoolClass->subjects->where('study_year_id', $currentStudyYear->id)->pluck('id');
            return !$subjectsIds->isEmpty() && Schedule::whereIn('subject_id', $subjectsIds)->get()->isEmpty();
        });

        return view('pages.schedules.create_schedule', [
            'schoolClasses' => $filteredSchoolClasses,
            'wholeClassSubjects' => Subject::where('group', 'no_groups')->get(),
            'firstGroupSubjects' => Subject::where('group', 'first_group')->get(),
            'secondGroupSubjects' => Subject::where('group', 'second_group')->get(),
            'days' => DayEnum::basicWorkDays(),
            'timeSlots' => TimeSlot::all(),
            'currentStudyYearId' => $currentStudyYear->id,
        ]);
    }

    /**
     * Store a newly created schedule in storage.
     *
     * @param StoreScheduleRequest $request
     * @return RedirectResponse
     */
    public function store(StoreScheduleRequest $request): RedirectResponse
    {
        try {
            DB::beginTransaction();

            $schoolClassId = $request->safe()['schoolClass'];
            $timeSlots = collect($request->safe()['timeSlots']);

            $scheduleSlots = $timeSlots->filter(function (array $timeSlot, int $key) {
                return $timeSlot['subjectId'] != null;
            });

            $this->insertScheduleSlots($scheduleSlots);

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return back()->withInput()
                ->withErrors('Виникла помилка під час створення розкладу. Зверніться до системного адміністратора.');
        }

        return redirect()->route('schedules.show', ['type' => 'classes', 'schedule' => $schoolClassId]);
    }

    /**
     * Display the schedule of specified owner.
     *
     * @param $type
     * @param int $id
     * @return View|RedirectResponse
     */
    public function show($type, int $id): View|RedirectResponse
    {
        try {
            if ($type == 'classes') {
                $schoolClass = SchoolClass::findOrFail($id);
                $schedule = Schedule::getSchoolClassSchedule($id);

                return view('pages.schedules.school_class_schedule_page', [
                    'schoolClass' => $schoolClass,
                    'schedule' => $schedule,
                    'type' => $type,
                    'scheduleOwners' => SchoolClass::all()->pluck('name', 'id'),
                ]);
            } else {
                $user = User::findOrFail($id);
                $schedule = Schedule::getPersonalSchedule($id);

                if(!$user->hasAnyRole('teacher', 'student')){
                    return back()->withInput()->withErrors('Ви повинні вибрати тільки вчителя або учня.');
                }

                return view('pages.schedules.user_schedule_page', [
                    'user' => $user,
                    'schedule' => $schedule,
                    'type' => $type,
                    'scheduleOwners' => User::role('teacher')->get()->pluck('full_name', 'id'),
                ]);
            }
        } catch (Throwable $e) {
            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return back()->withInput()
                ->withErrors('Виникла помилка під час пошуку розкладу. Зверніться до системного адміністратора.');
        }
    }

    /**
     * Show the form for editing the specified schedule.
     *
     * @param int $schoolClassId
     * @return RedirectResponse|View
     */
    public function edit(int $schoolClassId): RedirectResponse|View
    {
        try {
            if (!auth()->user()->hasPermissionTo('update schedule')) {
                return redirect()->route('schedule.show', ['type' => 'classes', 'schedule' => $schoolClassId]);
            }

            $schoolClass = SchoolClass::findOrFail($schoolClassId);
            $schedule = Schedule::getSchoolClassSchedule($schoolClassId);
        } catch (Throwable $e) {
            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return back()->withInput()
                ->withErrors('Виникла помилка під час пошуку класу, розклад якого ви хочете змінити. ' .
                    'Зверніться до системного адміністратора.');
        }

        return view('pages.schedules.update_schedule', [
            'schoolClass' => $schoolClass,
            'schedule' => $schedule,
            'wholeClassSubjects' => $schoolClass->subjects->where('group', 'no_groups')->all(),
            'firstGroupSubjects' => $schoolClass->subjects->where('group', 'first_group')->all(),
            'secondGroupSubjects' => $schoolClass->subjects->where('group', 'second_group')->all(),
            'days' => DayEnum::basicWorkDays(),
            'timeSlots' => TimeSlot::all(),
        ]);
    }

    /**
     * Update the specified schedule in storage.
     *
     * @param UpdateScheduleRequest $request
     * @param int $schoolClassId
     * @return RedirectResponse
     */
    public function update(UpdateScheduleRequest $request, int $schoolClassId): RedirectResponse
    {
        try {
            DB::beginTransaction();

            $this->deleteScheduleSlots($schoolClassId);

            $timeSlots = collect($request->safe()['timeSlots']);
            $scheduleSlots = $timeSlots->filter(function (array $timeSlot) {
                return $timeSlot['subjectId'] != null;
            });

            $this->insertScheduleSlots($scheduleSlots);

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return back()->withInput()
                ->withErrors('Виникла помилка під час зміни розкладу. Зверніться до системного адміністратора.');
        }

        return redirect()->route('schedules.show', ['type' => 'classes', 'schedule' => $schoolClassId]);
    }

    /**
     * Remove the specified schedule from storage.
     *
     * @param int $schoolClassId
     * @return RedirectResponse
     */
    public function destroy(int $schoolClassId): RedirectResponse
    {
        if (!auth()->user()->hasPermissionTo('delete schedule')) {
            return redirect()->route('schedules.show', ['type' => 'classes', 'schedule' => $schoolClassId]);
        }

        try {
            DB::beginTransaction();

            $this->deleteScheduleSlots($schoolClassId);

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return back()->withErrors(
                'Виникла помилка при видаленні розкладу. Зверніться до системного адміністратора.'
            );
        }

        return redirect()->route('schedules.index', ['type' => 'classes']);
    }
}
