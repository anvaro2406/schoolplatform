<?php

namespace App\Http\Controllers;

use App\Models\StudyYear;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Throwable;

class StudyYearController extends Controller
{
    /**
     * Show the form for creating a new school class.
     *
     * @return View|RedirectResponse
     */
    public function create(): View|RedirectResponse
    {
        if (!auth()->user()->hasPermissionTo('create study year')) {
            return redirect()->route('home');
        }

        return view('pages.study_years.create_study_year');
    }

    /**
     * Store a newly created school class in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'end' => 'required|date',
        ]);

        try {
            DB::beginTransaction();

            $end = Carbon::parse($request->only('end')['end']);
            $start = Carbon::parse('01-09-' . Carbon::parse($end)->year - 1);

            if(StudyYear::where('start_date', $start)->first()){
                throw new Exception("There is already the study year with such start date.");
            }

            StudyYear::create([
                'start_date' => $start,
                'end_date' => $end,
            ]);

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return back()->withInput()
                ->withErrors('Виникла помилка під час створення навчального року. Зверніться до системного адміністратора.');
        }

        return redirect()->route('home');
    }
}
