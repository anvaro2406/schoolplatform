<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTeacherFeedbackRequest;
use App\Models\TeacherFeedback;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

class TeacherFeedbackController extends Controller
{
    /**
     * Store a newly created teacher feedback in storage.
     *
     * @param StoreTeacherFeedbackRequest $request
     * @return RedirectResponse
     */
    public function store(StoreTeacherFeedbackRequest $request): RedirectResponse
    {
        try {
            DB::beginTransaction();

            $validatedRequest = $request->safe();

            TeacherFeedback::create([
                'date' => Carbon::now(),
                'subject' => $validatedRequest['subject'],
                'description' => $validatedRequest['description'],
                'student_id' => $validatedRequest['studentId'],
                'teacher_id' => auth()->user()->id,
            ]);

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage());

            return back()->withInput()
                ->withErrors('Виникла помилка при створенні зауваження. Зверніться до системного адміністратора.');
        }

        return redirect()->back();
    }
}
