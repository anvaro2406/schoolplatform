<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSchoolClassRequest;
use App\Http\Requests\UpdateSchoolClassRequest;
use App\Models\Lesson;
use App\Models\Schedule;
use App\Models\SchoolClass;
use App\Models\StudyYear;
use App\Models\Subject;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Throwable;

class SchoolClassController extends Controller
{
    /**
     * Display a listing of the school classes.
     *
     * @return View
     */
    public function index(): View
    {
        return view('pages.school_class_management.school_classes_catalogue', [
            'schoolClassesList' => SchoolClass::all()->pluck('name', 'id'),
        ]);
    }

    /**
     * Show the form for creating a new school class.
     *
     * @return View|RedirectResponse
     */
    public function create(): View|RedirectResponse
    {
        if (!auth()->user()->hasPermissionTo('create class')) {
            return redirect()->route('school-classes.index');
        }

        return view('pages.school_class_management.create_school_class', [
            'teachers' => User::whereNull('class_id')->role(['head-teacher'])->orderBy('lastname')->get(),
            'students' => User::whereNull('class_id')->role(['student'])->orderBy('lastname')->get(),
            'studyYears' => StudyYear::orderBy('start_date', 'desc')->get(),
        ]);
    }

    /**
     * Store a newly created school class in storage.
     *
     * @param StoreSchoolClassRequest $request
     * @return RedirectResponse
     */
    public function store(StoreSchoolClassRequest $request): RedirectResponse
    {
        try {
            DB::beginTransaction();

            $validatedSchoolClass = $request->validated();

            $schoolClass = SchoolClass::create([
                'study_year_id' => $validatedSchoolClass['studyYear'],
                'letter' => $validatedSchoolClass['letter'],
            ]);

            $studentsIds = array_keys($validatedSchoolClass['students']);
            $students = User::orderBy('lastname')->orderBy('firstname')->findOrFail($studentsIds);

            if (array_key_exists('isAutoGrouping', $validatedSchoolClass)) {
                $studentsFirstGroupAmount = round($students->count() / 2);

                $studentsIdsWithGroups = $students->map(function (User $student, int $index) use ($studentsFirstGroupAmount) {
                    return [
                        'id' => $student->id,
                        'group' => ($index < $studentsFirstGroupAmount) ? 'first_group' : 'second_group'
                    ];
                })->toArray();
            } else {
                $studentsIdsWithGroups = $validatedSchoolClass['students'];
            }

            User::upsert(
                $studentsIdsWithGroups, ['id'], ['group']
            );
            $schoolClass->students()->saveMany($students);

            $headTeacher = User::findOrFail($validatedSchoolClass['headTeacher']);
            $schoolClass->headTeacher()->save($headTeacher);

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return back()->withInput()
                ->withErrors('Виникла помилка під час створення класу. Зверніться до системного адміністратора.');
        }

        return redirect()->route('school-classes.show', ['school_class' => $schoolClass->id]);
    }

    /**
     * Display the specified school class.
     *
     * @param int $id
     * @return RedirectResponse|View
     */
    public function show(int $id): RedirectResponse|View
    {
        try {
            $schoolClass = SchoolClass::findOrFail($id);
        } catch (Throwable $e) {
            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return back()->withInput()
                ->withErrors('Виникла помилка при знаходженні даних класу в базі даних. Зверніться до ' .
                    'системного адміністратора.');
        }

        return view('pages.school_class_management.school_class_page', [
            'schoolClass' => $schoolClass,
            'schoolClassesList' => SchoolClass::all()->pluck('name', 'id'),
        ]);
    }

    /**
     * Show the form for editing the school class.
     *
     * @param int $id
     * @return View|RedirectResponse
     */
    public function edit(int $id): View|RedirectResponse
    {
        try {
            if (!auth()->user()->hasPermissionTo('update class')) {
                return redirect()->route('school-classes.index');
            }

            $schoolClass = SchoolClass::findOrFail($id);
        } catch (Throwable $e) {
            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return back()->withInput()
                ->withErrors('Виникла помилка під час пошуку класу. Зверніться до системного адміністратора.');
        }

        return view('pages.school_class_management.update_school_class', [
            'schoolClass' => $schoolClass,
            'teachers' => User::whereNull('class_id')->orWhere('class_id', $schoolClass->id)->role(['head-teacher'])
                ->orderBy('lastname')->get(),
            'students' => User::whereNull('class_id')->orWhere('class_id', $schoolClass->id)->role(['student'])
                ->orderBy('lastname')->get(),
            'studyYears' => StudyYear::orderBy('start_date', 'desc')->get(),
        ]);
    }

    /**
     * Update the specified school class in storage.
     *
     * @param UpdateSchoolClassRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(UpdateSchoolClassRequest $request, int $id): RedirectResponse
    {
        try {
            DB::beginTransaction();

            $validatedSchoolClass = $request->validated();

            $schoolClass = SchoolClass::findOrFail($id);

            $schoolClass->update([
                'study_year_id' => $validatedSchoolClass['studyYear'],
                'letter' => $validatedSchoolClass['letter'],
            ]);

            if ($schoolClass->headTeacher->id != $validatedSchoolClass['headTeacher']) {
                $oldHeadTeacher = $schoolClass->headTeacher;
                $oldHeadTeacher->class_id = null;
                $oldHeadTeacher->save();

                $newHeadTeacher = User::findOrFail($validatedSchoolClass['headTeacher']);
                $newHeadTeacher->class_id = $id;
                $newHeadTeacher->save();
            }

            $oldStudentsIds = $schoolClass->students->pluck('id');
            $newStudentsIds = collect(array_keys($validatedSchoolClass['students']));
            $toDeleteRelationshipsIds = $oldStudentsIds->diff($newStudentsIds);
            $toSetRelationshipsIds = $newStudentsIds->diff($oldStudentsIds);

            User::whereIn('id', $toDeleteRelationshipsIds)->update(['class_id' => null, 'group' => null]);
            DB::table('student_lesson')->whereIn('student_id', $toDeleteRelationshipsIds)->delete();

            $newStudents = User::orderBy('lastname')->orderBy('firstname')->findOrFail($toSetRelationshipsIds);

            if (array_key_exists('isAutoGrouping', $validatedSchoolClass)) {
                $studentsFirstGroupAmount = round($newStudents->count() / 2);

                $studentsIdsWithGroups = $newStudents->map(function (User $student, int $index) use ($studentsFirstGroupAmount) {
                    return [
                        'id' => $student->id,
                        'group' => ($index < $studentsFirstGroupAmount) ? 'first_group' : 'second_group'
                    ];
                })->toArray();
            } else {
                $studentsIdsWithGroups = $validatedSchoolClass['students'];
            }

            User::upsert(
                $studentsIdsWithGroups, ['id'], ['group']
            );
            $schoolClass->students()->saveMany($newStudents);

            $schoolClass->subjects->each(function (Subject $subject) use ($newStudents) {
                $subject->lessons->each(function (Lesson $lesson) use ($newStudents) {
                    $lesson->students()->attach($newStudents);
                });
            });

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return back()->withInput()
                ->withErrors('Виникла помилка під час оновлення даних класу. Зверніться до системного адміністратора.');
        }

        return redirect()->route('school-classes.show', ['school_class' => $id]);
    }

    /**
     * Remove the specified school class from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        if (!auth()->user()->hasPermissionTo('delete class')) {
            return redirect()->route('school-classes.index');
        }

        try {
            DB::beginTransaction();

//            $schoolClass = SchoolClass::findOrFail($id);
//            $studentsIds = $schoolClass->students->pluck('id');
//            $subjectsIds = $schoolClass->subjects->pluck('id');
//            $usersWithRelationIds = $studentsIds->merge($schoolClass->headTeacher->id);
//
//            User::whereIn('id', $usersWithRelationIds)->update(['class_id' => null, 'group' => null]);
//            DB::table('student_lesson')->whereIn('student_id', $studentsIds)->delete();
//            Schedule::whereIn('subject_id', $subjectsIds)->delete();
//            Lesson::whereIn('subject_id', $subjectsIds)->delete();
//            Subject::whereIn('id', $subjectsIds)->delete();

            SchoolClass::destroy($id);

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return back()->withErrors(
                'Виникла помилка при видаленні класу. Зверніться до системного адміністратора.'
            );
        }

        return redirect()->route('school-classes.index');
    }
}
