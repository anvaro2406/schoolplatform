<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSubjectRequest;
use App\Http\Requests\UpdateSubjectRequest;
use App\Models\SchoolProgram;
use App\Models\AbstractSubject;
use App\Models\User;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Throwable;

class SubjectController extends Controller
{
    /**
     * Display a listing of the subjects which belong to specified class.
     *
     * @param int|null $classNum
     * @return View
     */
    public function index(int $classNum = null): View
    {
        return view('pages.subject_management.subjects_catalogue', [
            'groupedSubjects' => $classNum
                ? AbstractSubject::where('class_number', $classNum)->get()->groupBy('school_program_id')
                : null,
            'сlassNum' => $classNum,
        ]);
    }

    /**
     * Show the form for creating a new subject.
     *
     * @return View|RedirectResponse
     */
    public function create(): View|RedirectResponse
    {
        if (!auth()->user()->hasPermissionTo('create subject')) {
            return redirect()->route('subjects.index');
        }

        return view('pages.subject_management.create_subject', [
            'schoolPrograms' => SchoolProgram::all(),
            'teachers' => User::role('teacher')->get(),
        ]);
    }

    /**
     * Store a newly created subject in storage.
     *
     * @param StoreSubjectRequest $request
     * @return RedirectResponse
     */
    public function store(StoreSubjectRequest $request): RedirectResponse
    {
        try {
            DB::beginTransaction();

            $validatedSubject = $request->safe()->only(
                ['schoolProgram', 'title', 'yearStudyHours', 'weekStudyHours', 'classNum', 'description', 'teachers']
            );

            $subject = new AbstractSubject([
                'title' => $validatedSubject['title'],
                'year_studyhours' => $validatedSubject['yearStudyHours'],
                'week_studyhours' => $validatedSubject['weekStudyHours'],
                'class_number' => $validatedSubject['classNum'],
                'description' => $validatedSubject['description'],
            ]);

            $schoolProgram = SchoolProgram::firstOrCreate(['name' => $validatedSubject['schoolProgram']]);

            $subject->schoolProgram()->associate($schoolProgram);
            $subject->save();
            $subject->teachers()->attach($validatedSubject['teachers']);

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage());

            return back()->withInput()
                ->withErrors('Виникла помилка при створенні предмету. Зверніться до системного адміністратора.');
        }

        return redirect()->route('subjects.index');
    }

    /**
     * Display the specified subject.
     *
     * @param int $id
     * @return View|RedirectResponse
     */
    public function show(int $id): View|RedirectResponse
    {
        try {
            $subject = AbstractSubject::findOrFail($id);
        } catch (Throwable $e) {
            Log::error($e->getMessage());

            return back()->withInput()
                ->withErrors('Виникла помилка при знаходженні предмету в базі даних. Зверніться до ' .
                    'системного адміністратора.');
        }

        return view('pages.subject_management.subject_page', ['subject' => $subject]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return View|RedirectResponse
     */
    public function edit(int $id): View|RedirectResponse
    {
        try {
            if (!auth()->user()->hasPermissionTo('update subject')) {
                return redirect()->route('subjects.index');
            }

            $subject = AbstractSubject::findOrFail($id);
        } catch (Throwable $e) {
            Log::error($e->getMessage());

            return back()->withInput()
                ->withErrors('Виникла помилка при пошуку предмету. Зверніться до системного адміністратора.');
        }

        return view('pages.subject_management.update_subject', [
            'subject' => $subject,
            'schoolPrograms' => SchoolProgram::all(),
            'teachers' => User::role('teacher')->get(),
        ]);
    }

    /**
     * Update the specified subject in storage.
     *
     * @param UpdateSubjectRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(UpdateSubjectRequest $request, int $id): RedirectResponse
    {
        try {
            DB::beginTransaction();

            $validatedSubject = $request->safe()->only(
                ['schoolProgram', 'title', 'yearStudyHours', 'weekStudyHours', 'classNum', 'description', 'teachers']
            );

            $subject = AbstractSubject::findOrFail($id);

            $subject->fill([
                'title' => $validatedSubject['title'],
                'year_studyhours' => $validatedSubject['yearStudyHours'],
                'week_studyhours' => $validatedSubject['weekStudyHours'],
                'class_number' => $validatedSubject['classNum'],
                'description' => $validatedSubject['description'],
            ]);

            $schoolProgram = SchoolProgram::firstOrCreate(['name' => $validatedSubject['schoolProgram']]);

            $subject->schoolProgram()->associate($schoolProgram);
            $subject->save();
            $subject->teachers()->sync($validatedSubject['teachers']);

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage());

            return back()->withInput()
                ->withErrors('Виникла помилка при оновлені даних предмету. Зверніться до системного адміністратора.');
        }

        return redirect()->route('subjects.show', ['subject' => $id]);
    }

    /**
     * Remove the specified subject from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        if (!auth()->user()->hasPermissionTo('delete subject')) {
            return redirect()->route('subjects.index');
        }

        try {
            if (!AbstractSubject::destroy($id)) {
                throw new Exception("Model not found.", 404);
            }
        } catch (Throwable $e) {
            Log::error($e->getMessage());

            return back()->withErrors(
                'Виникла помилка при видаленні предмету. Зверніться до системного адміністратора.'
            );
        }

        return redirect()->route('subjects.index');
    }
}
