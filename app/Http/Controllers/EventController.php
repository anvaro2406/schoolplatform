<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEventRequest;
use App\Http\Requests\UpdateEventRequest;
use App\Models\Category;
use App\Models\Event;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Throwable;

class EventController extends Controller
{
    /**
     * Store the image file in Storage and return filepath. If $image is null then return empty string
     *
     * @param $image
     * @return string
     */
    private function storeImage($image): string
    {
        if (isset($image)) {
            return $image->store('images', 'public');
        } else {
            return "";
        }
    }

    /**
     * Display a listing of the events.
     *
     * @return View
     */
    public function index(): View
    {
        $itemsPerPage = session('itemsPerPage', 10);
        $sortColumn = session('sortEventsColumn', 'dateNearest');

        $events = match ($sortColumn) {
            'dateNearest' => Event::orderBy('startDatetime'),
            'dateFarthest' => Event::orderByDesc('startDatetime'),
            'title' => Event::orderBy('title'),
            'category' => Event::orderBy(
                Category::select('name')
                    ->whereColumn('id', 'events.category_id')
                    ->orderByDesc('name')
                    ->limit(1)
            ),
        };

        return view('pages.event_management.events_catalogue', [
            'events' => $events->paginate($itemsPerPage),
            'categories' => Category::all(),
            'sortColumn' => $sortColumn
        ]);
    }

    /**
     * Show the form for creating a new event.
     *
     * @return View|RedirectResponse
     */
    public function create(): View|RedirectResponse
    {
        if (!auth()->user()->hasPermissionTo('create event')) {
            return redirect()->route('events.index');
        }

        return view(
            'pages.event_management.create_event', ['categories' => Category::all()]
        );
    }

    /**
     * Store a newly created event in storage.
     *
     * @param StoreEventRequest $request
     * @return RedirectResponse
     */
    public function store(StoreEventRequest $request): RedirectResponse
    {
        try {
            DB::beginTransaction();

            $validatedEvent = $request->validated();

            $event = new Event([
                'title' => $validatedEvent['title'],
                'image_filepath' => $this->storeImage($request->file('image')),
                'description' => $validatedEvent['description'],
                'startDatetime' => $validatedEvent['startDatetime'],
                'endDatetime' => $validatedEvent['endDatetime'],
            ]);

            $category = Category::firstOrCreate(['name' => $validatedEvent['category']]);

            $event->category()->associate($category);
            $event->save();

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage());

            return back()->withInput()
                ->withErrors('Виникла помилка при створенні події. Зверніться до системного адміністратора.');
        }

        return redirect()->route('events.index');
    }

    /**
     * Show the form for editing the specified event.
     *
     * @param int $id
     * @return View|RedirectResponse
     */
    public function edit(int $id): View|RedirectResponse
    {
        if (!auth()->user()->hasPermissionTo('update event')) {
            return redirect()->route('event.index');
        }

        return view('pages.event_management.update_event', [
            'event' => Event::find($id),
            'categories' => Category::all(),
        ]);
    }

    /**
     * Update the specified event in storage.
     *
     * @param UpdateEventRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(UpdateEventRequest $request, int $id): RedirectResponse
    {
        try {
            DB::beginTransaction();

            $validatedEvent = $request->validated();

            $event = Event::findOrFail($id);

            Storage::disk('public')->delete($event->image_filepath);

            $event->fill([
                'title' => $validatedEvent['title'],
                'image_filepath' => $this->storeImage($request->file('image')),
                'description' => $validatedEvent['description'],
                'startDatetime' => $validatedEvent['startDatetime'],
                'endDatetime' => $validatedEvent['endDatetime'],
            ]);

            $category = Category::firstOrCreate(['name' => $validatedEvent['category']]);

            $event->category()->associate($category);
            $event->save();

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage());

            return back()->withInput()
                ->withErrors('Виникла помилка при оновлені даних події. Зверніться до системного адміністратора.');
        }

        return redirect()->route('events.index');
    }

    /**
     * Soft delete the specified event from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        if (!auth()->user()->hasPermissionTo('delete event')) {
            return redirect()->route('events.index');
        }

        try {
            if (!Event::destroy($id)) {
                throw new Exception("Model not found.", 404);
            }
        } catch (Throwable $e) {
            Log::error($e->getMessage());

            return back()->withErrors(
                'Виникла помилка при видаленні події. Зверніться до системного адміністратора.'
            );
        }

        return redirect()->route('events.index');
    }

    /**
     * Display a listing of the events by specified category.
     *
     * @param string $categoryName
     * @return View
     */
    public function getByCategoryName(string $categoryName): View
    {
        $category = Category::where('name', $categoryName)->first();

        $itemsPerPage = session('itemsPerPage', 10);

        return view('pages.event_management.events_catalogue', [
            'events' => $category->events()->paginate($itemsPerPage),
            'categories' => Category::all(),
        ]);
    }

    /**
     * Set parameter for sorting.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function indexSorted(Request $request): RedirectResponse
    {
        $validatedInput = $request->validate([
            'sortColumn' => ['required', 'regex:/^dateNearest|dateFarthest|title|category$/']
        ]);

        session(['sortEventsColumn' => $validatedInput['sortColumn']]);

        return redirect()->route('events.index');
    }

    /**
     * Display a listing of the events found by specified parameter.
     *
     * @param Request $request
     * @return RedirectResponse|View
     */
    public function search(Request $request): RedirectResponse|View
    {
        $validatedInput = $request->validate([
            'searchParameter' => ['string']
        ]);

        $events = Event::where('title', 'like', '%' . $validatedInput['searchParameter'] . '%')
            ->orWhere('description', 'like', '%' . $validatedInput['searchParameter'] . '%')->get();

        if ($events->isEmpty()) {
            return view('pages.event_management.events_catalogue', [
                'categories' => Category::all(),
                'isSearch' => true,
                'noResult' => true,
            ]);
        }

        return view('pages.event_management.events_catalogue', [
            'events' => $events->toQuery()->paginate(session('itemsPerPage')),
            'categories' => Category::all(),
            'isSearch' => true,
        ]);
    }

    /**
     * Display a listing of the soft deleted events.
     *
     * @return View
     */
    public function indexTrashed(): View
    {
        $itemsPerPage = session('itemsPerPage', 10);

        $events = Event::onlyTrashed()->orderByDesc('endDatetime');

        return view('pages.event_management.events_catalogue', [
            'events' => $events->paginate($itemsPerPage),
            'categories' => Category::all(),
        ]);
    }
}
