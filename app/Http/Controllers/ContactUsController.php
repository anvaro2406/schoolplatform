<?php

namespace App\Http\Controllers;

use App\Mail\ContactUsWritten;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Throwable;

class ContactUsController extends Controller
{
    /**
     * Sending a mail to all users with specified role.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function send(Request $request): RedirectResponse
    {
        $validatedInput = $request->validate([
            'receiver' => ['required', 'regex:/^admin|director|secretary|educational-deputy|organizer$/'],
            'messageSubject' => ['required', 'string'],
            'messageBody' => ['required', 'string'],
        ]);

        try{
            $users = User::role($validatedInput['receiver'])->get();

            foreach ($users as $user) {
                Mail::to($user)->send(
                    new ContactUsWritten($validatedInput['messageSubject'], $validatedInput['messageBody'])
                );
            }
        } catch (Throwable $e) {
            Log::error($e->getMessage());

            return back()->withErrors(
                'Виникла помилка при надсиланні листа. Спробуйте зв\'язатися з системним адміністратором іншим шляхом.'
            );
        }

        $request->session()->flash('status', 'Лист успішно надіслано.');
        return back();
    }
}
