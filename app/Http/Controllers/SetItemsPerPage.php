<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class SetItemsPerPage extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function __invoke(Request $request): RedirectResponse
    {
        $validInput = $request->validate([
            'itemsPerPage' => 'required|numeric|min:1|max:25',
            'path' => 'required|url',
        ]);

        session(['itemsPerPage' => $validInput['itemsPerPage']]);

        return redirect($validInput['path']);
    }
}
