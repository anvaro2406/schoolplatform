<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Throwable;

class JournalSubjectPageController extends Controller
{
    /**
     * Display the journal subject page.
     *
     * @param $id
     * @return RedirectResponse|View
     */
    public function show($id): RedirectResponse|View
    {
        try {
            if (!auth()->user()->hasPermissionTo('view lessons progress')) {
                return redirect()->route('home');
            }
            $subject = Subject::findOrFail($id);
            if ($subject->group != 'no_groups') {
                $students = $subject->schoolClass->students()->where('group', $subject->group)->orderBy('lastname')->get();
            } else {
                $students = $subject->schoolClass->students()->orderBy('lastname')->get();
            }
        } catch (Throwable $e) {
            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return back()->withInput()
                ->withErrors('Виникла помилка при знаходженні даних сторінки журналу з предметом в базі даних. ' .
                    'Зверніться до системного адміністратора.');
        }

        return view('pages.journals.journal_subject_page', [
            'subject' => $subject,
            'abstractSubject' => $subject->abstractSubject,
            'schoolClass' => $subject->schoolClass,
            'lessons' => $subject->lessons->sortBy(function ($lesson) {
                if ($lesson['date'] === null) {
                    return PHP_INT_MAX;
                }

                return strtotime($lesson['date']);
            }),
            'students' => $students,
            'teacher' => $subject->teacher
        ]);
    }
}
