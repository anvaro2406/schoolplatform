<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Throwable;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the employees.
     *
     * @return View
     */
    public function index(): View
    {
        $itemsPerPage = session('itemsPerPage', 10);

        $employees = User::whereHas('roles', function ($query) {
            $query->where('name', '<>', 'parent')->where('name', '<>', 'student');
        });

        return view('pages.employee_management.employees_catalogue', [
            'employees' => $employees->paginate($itemsPerPage)
        ]);
    }

    /**
     * Display an employee profile.
     *
     * @param int $id
     * @return RedirectResponse|View
     */
    public function show(int $id): RedirectResponse|View
    {
        try {
            $employee = User::findOrFail($id);

            if ($employee->hasAnyRole(['parent, student'])) {
                return redirect()->route('employees.index')
                    ->withErrors('Ви не маєте доступу до даних учнів і їх батьків. Якщо ви адміністратор, то ' .
                        'зайдіть в свій обліковий запис і відкрийте список всіх користувачів для перегляду їхніх даних.');
            }
        } catch (Throwable $e) {
            Log::error($e->getMessage());

            return back()->withInput()
                ->withErrors('Виникла помилка при знаходженні даних працівника. Зверніться до системного ' .
                    ' адміністратора.');
        }

        return view('pages.user_management.user_profile', ['user' => $employee]);
    }
}
