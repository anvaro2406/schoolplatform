<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\View\View;

class SendCurrentUserPasswordResetLink extends Controller
{
    /**
     * @param Request $request
     * @return View
     */
    public function __invoke(Request $request): View
    {
        Password::sendResetLink(['email' => auth()->user()->email]);

        return view('pages.auth.current_user_reset_password');
    }
}
