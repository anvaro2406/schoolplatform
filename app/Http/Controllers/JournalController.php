<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreJournalRequest;
use App\Http\Requests\UpdateJournalRequest;
use App\Models\AbstractSubject;
use App\Models\Lesson;
use App\Models\Schedule;
use App\Models\SchoolClass;
use App\Models\SchoolProgram;
use App\Models\StudyYear;
use App\Models\Subject;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Throwable;

class JournalController extends Controller
{
    /**
     * Insert the subjects of the specified school class and their lessons with relations to students in storage.
     *
     * @param $abstractSubjects
     * @param $schoolClassId
     * @param $studyYearId
     * @return void
     */
    private function insertSubjectsAndRelations($abstractSubjects, $schoolClassId, $studyYearId): void
    {
        $subjects = $abstractSubjects->map(function (array $abstractSubject) use ($studyYearId, $schoolClassId) {
            return [
                'study_year_id' => $studyYearId,
                'abstract_subject_id' => $abstractSubject['id'],
                'class_id' => $schoolClassId,
                'group' => $abstractSubject['group'],
                'teacher_id' => $abstractSubject['teacherId'],
            ];
        });
        Subject::insert($subjects->toArray());

        $schoolClass = SchoolClass::findOrFail($schoolClassId);
        $schoolClass->subjects->each(function (Subject $subject) use ($schoolClass) {
            $subjectLessons = [];
            for ($i = 0; $i < $subject->abstractSubject->year_studyhours; $i++) {
                $subjectLessons[] = ['subject_id' => $subject->id];
            }
            Lesson::insert($subjectLessons);

            $subject->lessons->each(function (Lesson $lesson) use ($schoolClass) {
                $lesson->students()->attach($schoolClass->students);
            });
        });
    }

    /**
     * Delete the subjects of the specified school class and their lessons with relations to students from storage.
     *
     * @param $schoolClassId
     * @param $studyYearId
     * @return void
     */
    private function deleteSubjectsAndRelations($schoolClassId, $studyYearId): void
    {
        $schoolClass = SchoolClass::findOrFail($schoolClassId);
        $subjects = $schoolClass->subjects()->where('study_year_id', $studyYearId)->get();
        $subjects->each(function (Subject $subject) use ($schoolClass) {
            $subject->lessons->each(function (Lesson $lesson) use ($schoolClass) {
                $lesson->students()->detach($schoolClass->students);
            });
        });

        $subjectsIds = $subjects->pluck('id');
        Lesson::whereIn('subject_id', $subjectsIds)->delete();
        Schedule::whereIn('subject_id', $subjectsIds)->delete();
        Subject::whereIn('id', $subjectsIds)->delete();
    }

    /**
     * Return json with all school class created in the specified study year.
     *
     * @param $studyYearId
     * @return JsonResponse|RedirectResponse
     */
    public function getStudyYearClasses($studyYearId, $isJournalCreateCall): JsonResponse|RedirectResponse
    {
        if (!auth()->user()->hasPermissionTo('view journal')) {
            return redirect()->route('home');
        }

        if($isJournalCreateCall == 'true'){
            $subjectsClassesIds = Subject::where('study_year_id', $studyYearId)->get()->pluck('class_id')->unique();
            $schoolClasses = SchoolClass::getActualSchoolClasses($studyYearId)
                ->whereNotIn('id', $subjectsClassesIds)
                ->where('deleted_at', null);
        }else{
            $schoolClasses = SchoolClass::getActualSchoolClasses($studyYearId);
        }

        return response()->json(
            $schoolClasses->map(function (SchoolClass $schoolClass) use ($studyYearId) {
                return ['id' => $schoolClass->id, 'name' => $schoolClass->nameInStudyYear($studyYearId)];
            })
        );
    }

    /**
     * Display a listing of the journals or listing of the journals subjects pages which the user is taught.
     *
     * @param $type
     * @return View|RedirectResponse
     */
    public function index($type): View|RedirectResponse
    {
        if (auth()->user()->hasPermissionTo('view journal') && $type === 'allJournals') {
            return view('pages.journals.journals_catalogue', [
                'studyYears' => StudyYear::all()->pluck('name', 'id'),
                'schoolClasses' => SchoolClass::all()->pluck('name', 'id'),
            ]);
        } elseif (auth()->user()->hasPermissionTo('update lessons progress') && $type === 'myJournals') {
            return view('pages.journals.teacher_journal_pages_catalogue', [
                'subjects' => auth()->user()->subjects, // the user is a teacher because he/she has permission
            ]);
        } else {
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new journal.
     *
     * @return RedirectResponse|View
     */
    public function create(): RedirectResponse|View
    {
        if (!auth()->user()->hasPermissionTo('create journal')) {
            return redirect()->route('journals.index');
        }

        $subjectsClassesIds = Subject::all()->pluck('class_id')->unique();

        return view('pages.journals.create_journal', [
            'schoolClasses' => SchoolClass::whereNotIn('id', $subjectsClassesIds)->get(),
            'schoolPrograms' => SchoolProgram::all(),
            'abstractSubjects' => AbstractSubject::all(),
            'studyYears' => StudyYear::all(),
        ]);
    }

    /**
     * Store a newly created journal in storage.
     *
     * @param StoreJournalRequest $request
     * @return RedirectResponse
     */
    public function store(StoreJournalRequest $request): RedirectResponse
    {
        try {
            DB::beginTransaction();

            $schoolClassId = $request->safe()['schoolClass'];
            $studyYearId = $request->safe()['studyYear'];
            $abstractSubjects = collect($request->safe()['subjects']);

            $this->insertSubjectsAndRelations($abstractSubjects, $schoolClassId, $studyYearId);

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return back()->withInput()
                ->withErrors('Виникла помилка під час створення журналу. Зверніться до системного адміністратора.');
        }

        return redirect()->route('journals.show', ['school_class' => $schoolClassId, 'study_year' => $studyYearId]);
    }

    /**
     * Display the journal of the specified school class.
     *
     * @param $schoolClassId
     * @param $studyYearId
     * @return View|RedirectResponse
     */
    public function show($schoolClassId, $studyYearId): View|RedirectResponse
    {
        if (!auth()->user()->hasPermissionTo('view journal')) {
            return redirect()->route('home');
        }

        return view('pages.journals.journal_page', [
            'schoolClasses' => SchoolClass::all()->pluck('name', 'id'),
            'studyYears' => StudyYear::all()->pluck('name', 'id'),
            'selectedStudyYearId' => $studyYearId,
            'selectedSchoolClassId' => $schoolClassId,
            'subjects' => Subject::where('class_id', $schoolClassId)->where('study_year_id', $studyYearId)->get(),
        ]);
    }

    /**
     * Show the form for editing the journal of the specified school class.
     *
     * @param int $schoolClassId
     * @param int $studyYearId
     * @return View|RedirectResponse
     */
    public function edit(int $schoolClassId, int $studyYearId): View|RedirectResponse
    {
        try {
            if (!auth()->user()->hasPermissionTo('update journal')) {
                return redirect()->route('journals.show', ['journal' => $schoolClassId]);
            }

            $schoolClass = SchoolClass::findOrFail($schoolClassId);
            $abstractSubjects = AbstractSubject::where('class_number', $schoolClass->numberInStudyYear($studyYearId))->get();

            $schoolProgramsIds = $abstractSubjects->pluck('school_program_id');
            $schoolPrograms = SchoolProgram::findOrFail($schoolProgramsIds);

            $studyYear = StudyYear::findOrFail($studyYearId);
        } catch (Throwable $e) {
            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return back()->withInput()
                ->withErrors('Виникла помилка під час пошуку класу, журнал якого ви хочете змінити. ' .
                    'Зверніться до системного адміністратора.');
        }

        return view('pages.journals.update_journal', [
            'abstractSubjects' => $abstractSubjects,
            'schoolPrograms' => $schoolPrograms,
            'schoolClass' => $schoolClass,
            'currentSchoolProgram' => $schoolClass->subjects->where('study_year_id', $studyYearId)->first()->abstractSubject->schoolProgram,
            'studyYear' => $studyYear,
        ]);
    }

    /**
     * Update the journal of the specified school class in storage.
     *
     * @param UpdateJournalRequest $request
     * @param int $schoolClassId
     * @param int $studyYearId
     * @return RedirectResponse
     */
    public function update(UpdateJournalRequest $request, int $schoolClassId, int $studyYearId): RedirectResponse
    {
        try {
            DB::beginTransaction();

            $this->deleteSubjectsAndRelations($schoolClassId, $studyYearId);

            $abstractSubjects = collect($request->safe()['subjects']);

            $this->insertSubjectsAndRelations($abstractSubjects, $schoolClassId, $studyYearId);

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return back()->withInput()
                ->withErrors('Виникла помилка під час зміни даних журналу. Зверніться до системного адміністратора.');
        }

        return redirect()->route('journals.show', ['school_class' => $schoolClassId, 'study_year' => $studyYearId]);
    }

    /**
     * Remove the journal of the specified school class from storage.
     *
     * @param int $schoolClassId
     * @return RedirectResponse
     */
    public function destroy(int $schoolClassId, int $studyYearId): RedirectResponse
    {
        if (!auth()->user()->hasPermissionTo('delete journal')) {
            return redirect()->route('home');
        }

        try {
            DB::beginTransaction();

            $this->deleteSubjectsAndRelations($schoolClassId, $studyYearId);

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();

            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return back()->withErrors(
                'Виникла помилка при видаленні журналу. Зверніться до системного адміністратора.'
            );
        }

        return redirect()->route('journals.index', ['type' => 'allJournals']);
    }
}
