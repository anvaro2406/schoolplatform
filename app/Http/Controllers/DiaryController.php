<?php

namespace App\Http\Controllers;

use App\Models\Lesson;
use App\Models\Schedule;
use App\Models\StudyYear;
use App\Models\TeacherFeedback;
use App\Models\User;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Throwable;

class DiaryController extends Controller
{
    /**
     * Display the diary of specified student.
     *
     * @param int $studentId
     * @param int|null $weekNumber
     * @return View|RedirectResponse
     */
    public function show(int $studentId, int $weekNumber = null): View|RedirectResponse
    {
        try {
            if (!auth()->user()->hasAnyPermission(['view own diary', 'view child diary'])) {
                return redirect()->route('home');
            }

            $student = User::findOrFail($studentId);
            if (!$student->hasRole('student')) {
                throw new Exception(
                    'Provided student id: ' . $studentId . ' belongs to user which is not assigned student role.'
                );
            }

            $schedule = Schedule::getPersonalSchedule($studentId);
            if ($schedule->isEmpty()) {
                throw new Exception(
                    'Provided student id: ' . $studentId . ' belongs to user which does not have schedule.'
                );
            }

            $currentStudyYear = StudyYear::getCurrentStudyYear();
            $weeks = $currentStudyYear->getOrderedWeeks();

            if ($weekNumber == null) {
                $selectedWeek = $currentStudyYear->getCurrentWeek();
            } else {
                $selectedWeek = $weeks->where('orderNumber', $weekNumber)->first();
            }

            $weekLessons = $student->lessons->filter(function (Lesson $lesson) use ($selectedWeek) {
                return $lesson->date != null && $lesson->date->between($selectedWeek['startDate'], $selectedWeek['endDate'], true);
            });
            if (!$weekLessons->isEmpty()) {
                $weekLessons = $weekLessons->groupBy([
                    function (Lesson $lesson) {
                        return strtolower($lesson->date->englishDayOfWeek);
                    },
                    function (Lesson $lesson) {
                        return $lesson->subject_id;
                    }
                ])->map(function (Collection $subjects) {
                    return $subjects->map(function (Collection $lessons) {
                        return $lessons->sortBy(function (Lesson $lesson) {
                            return $lesson->created_at;
                        })->values();
                    });
                });
            }

            $weekFeedbacks = $student->receivedFeedbacks->filter(
                function (TeacherFeedback $feedback) use ($selectedWeek) {
                    return $feedback->date != null
                        && $feedback->date->between($selectedWeek['startDate'], $selectedWeek['endDate'], true);
                }
            );
            if (!$weekFeedbacks->isEmpty()) {
                $weekFeedbacks = $weekFeedbacks->groupBy(function (TeacherFeedback $feedback) {
                    return strtolower($feedback->date->englishDayOfWeek);
                });
            }
        } catch (Throwable $e) {
            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return view('pages.diary', [
                'canFormDiary' => false,
                'student' => null,
                'schedule' => [],
                'weeks' => [],
                'selectedWeek' => [],
                'weekLessons' => [],
                'weekFeedbacks' => [],
            ])->withErrors(['Виникла помилка під час формування щоденника. Зверніться до системного адміністратора.']);
        }

        return view('pages.diary', [
            'canFormDiary' => true,
            'student' => $student,
            'schedule' => $schedule,
            'weeks' => $weeks->groupBy(function (array $range) {
                return $range['startDate']->year;
            }),
            'selectedWeek' => $selectedWeek,
            'weekLessons' => $weekLessons,
            'weekFeedbacks' => $weekFeedbacks,
        ]);
    }
}
