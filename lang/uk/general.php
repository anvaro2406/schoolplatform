<?php

return [
    'monday' => 'Понеділок',
    'tuesday' => 'Вівторок',
    'wednesday' => 'Середа',
    'thursday' => 'Четвер',
    'friday' => 'П\'ятниця',
    'saturday' => 'Субота',
];
