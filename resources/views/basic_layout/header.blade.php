<header id="header" class="custom-header header d-flex flex-column align-items-center fixed-top">
    <div class="logo-container container-fluid d-flex align-items-center justify-content-center">
        <a href="{{ url('/') }}" class="logo d-flex justify-content-center align-items-center w-full flex-row">
            <img src="{{ asset('/assets/img/custom_logo.png') }}" alt="" style="max-width:none;">
            <h1>Красилівська загальноосвітня школа І-ІІІ ступенів №1</h1>
        </a>
    </div>
    <div class="navbar-container container-fluid d-flex align-items-center justify-content-center">
        @include('basic_layout.navbar')
    </div>
</header>
