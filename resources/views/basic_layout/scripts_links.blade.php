<!-- Vendor JS Files -->
<script src="{{ asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/aos/aos.js') }}"></script>
<script src="{{ asset('js/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>
<!-- Template Main JS File -->
<script src="{{ asset('/assets/js/main.js') }}"></script>
<!-- Google maps API -->
<script async
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-9nCimB7jPp-ZPZNMiLfK_0KABYtRSQk&callback=initMap">
</script>
