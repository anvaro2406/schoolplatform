<div class="row text-center py-2">
    @if ($paginator->hasPages())
        <div class="col-xxl-2 p-0 m-0"></div>
        <div class="custom-pagination col-xxl-7 col-xl-8 p-0 m-0">
            <a href="{{ $paginator->previousPageUrl() }}"
               class="prev {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
                Попередня
            </a>
            <br class="new-line"/>
            @if(($paginator->onEachSide * 2 + 5) < $paginator->lastPage())
                {{-- onEachSide * 2 + 1(current) + 2(first, last) + 2(min skipped pages amount)--}}
                <a href="{{ $paginator->url(1) }}" class="{{ ($paginator->currentPage() == 1) ? ' active' : '' }}">1</a>

                @if(($paginator->currentPage() - 1 - $paginator->onEachSide) <= 1)
                    @for ($i = 2; $i <= $paginator->currentPage() + $paginator->onEachSide; $i++)
                        <a href="{{ $paginator->url($i) }}"
                           class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                            {{ $i }}
                        </a>
                    @endfor
                    ...
                @elseif(($paginator->currentPage() + 1 + $paginator->onEachSide) >= $paginator->lastPage())
                    ...
                    @for ($i = $paginator->currentPage() - $paginator->onEachSide; $i < $paginator->lastPage(); $i++)
                        <a href="{{ $paginator->url($i) }}"
                           class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                            {{ $i }}
                        </a>
                    @endfor
                @else
                    ...
                    @for ($i = $paginator->currentPage() - $paginator->onEachSide; $i <= $paginator->currentPage() + $paginator->onEachSide; $i++)
                        <a href="{{ $paginator->url($i) }}"
                           class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                            {{ $i }}
                        </a>
                    @endfor
                    ...
                @endif

                <a href="{{ $paginator->url($paginator->lastPage()) }}"
                   class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' active' : '' }}">
                    {{ $paginator->lastPage() }}
                </a>
            @else
                @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                    <a href="{{ $paginator->url($i) }}"
                       class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                        {{ $i }}
                    </a>
                @endfor
            @endif
            <br class="new-line"/>
            <a href="{{ $paginator->nextPageUrl() }}"
               class="next {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                Наступна
            </a>
        </div>
    @else
        <div class="col-xxl-9 col-xl-8 p-0 m-0"></div>
    @endif
    @if($paginator->total() != 0)
        <div class="col-xxl-3 col-xl-4 d-flex align-items-center justify-content-end">
            <form id="items-per-page-form" method="POST" action="{{ url('items_per_age') }}">
                @csrf
                <div style="font-size: 20px;">
                    <label for="items-per-page">
                        Елементів на сторінці:
                        <x-select id="items-per-page" name="itemsPerPage" onchange="changeItemsPerPage()"
                                  style="font-size: 18px;">
                            <option value="1" @selected(1 == $paginator->perPage())>1</option>
                            <option value="2" @selected(2 == $paginator->perPage())>2</option>
                            @for( $itemsCount = 10; $itemsCount <= 25; $itemsCount += 5)
                                <option value="{{ $itemsCount }}" @selected($itemsCount == $paginator->perPage())>
                                    {{ $itemsCount }}
                                </option>
                            @endfor
                        </x-select>
                    </label>
                    <input hidden name="path" value="{{ $paginator->path() }}">
                </div>
            </form>
        </div>
    @endif
</div>

