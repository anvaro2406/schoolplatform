<footer id="footer" class="custom-footer footer mt-auto">
    <div class="footer-legal">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <div class="col-md-3 text-start mb-3 mb-md-0 d-flex flex-column justify-content-center">
                    <img src="{{ asset('/assets/img/custom_logo.png') }}" alt="">
                </div>
                <div class="col-md-5 text-start mb-3 mb-md-0 d-flex flex-column justify-content-center">
                    Адреса: 31000, Хмельницька обл, місто Красилів, вул.Центральна, 38<br/>
                    Електронна пошта: schl1@edu.kr.km.ua<br/>
                    Телефон: 0385542704<br/>
                </div>
                <div class="col-md-2 text-start mb-3 mb-md-0 d-flex flex-column justify-content-center">
                    <div class="copyright">
                        © <strong><span>Красилівська ЗОШ №1</span></strong>.
                    </div>
                    <div>
                        Всі права збережено.
                    </div>
                    <div class="credits">
                        Розробив Андрій Романов
                    </div>
                </div>
                <div class="col-md-2 d-flex flex-column justify-content-center">
                    <div class="social-links mb-3 mb-lg-0 text-center text-md-center">
                        <a href="https://www.facebook.com/groups/krschl1/" class="facebook">
                            <i class="bi bi-facebook"></i>
                        </a>
                        <a href="https://www.instagram.com/school_1_the_best/" class="instagram">
                            <i class="bi bi-instagram"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
