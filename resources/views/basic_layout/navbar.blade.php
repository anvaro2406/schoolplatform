<nav id="navbar" class="custom-navbar navbar col-12 d-flex justify-content-center">
    <i id="open-navbar-drawer-btn" class="bi bi-list col-1"></i>
    <ul id="navbar-content" class="col-10">
        @can('create news')
            <li class="dropdown">
                <span>
                    <span><a href="{{ route('news.index') }}">Новини</a></span>&nbsp;
                    <i class="bi bi-chevron-down dropdown-indicator"></i>
                </span>
                <ul>
                    <li><a href="{{ route('news.create') }}">Створити новину</a></li>
                </ul>
            </li>
        @else
            <li><a href="{{ route('news.index') }}">Новини</a></li>
        @endif
        @can('create event')
            <li class="dropdown">
                <span>
                    <span><a href="{{ route('events.index') }}">Шкільні події</a></span>&nbsp;
                    <i class="bi bi-chevron-down dropdown-indicator"></i>
                </span>
                <ul>
                    <li><a href="{{ route('events.create') }}">Створити подію</a></li>
                </ul>
            </li>
        @else
            <li><a href="{{ route('events.index') }}">Шкільні події</a></li>
        @endif
        <li><a href="{{ route('employees.index') }}">Працівники школи</a></li>
        <li class="dropdown">
            <span>
                <span>Освіта</span>&nbsp;<i class="bi bi-chevron-down dropdown-indicator"></i>
            </span>
            <ul>
                @if(Auth::check() && Auth::user()->can('create subject'))
                    <li class="dropdown">
                        <span>
                            <span><a href="{{ route('subjects.index') }}">Шкільні предмети</a></span>&nbsp;
                            <i class="bi bi-chevron-down dropdown-indicator"></i>
                        </span>
                        <ul>
                            <li><a href="{{ route('subjects.create') }}">Створити предмет</a></li>
                        </ul>
                    </li>
                @else
                    <li><a href="{{ route('subjects.index') }}">Шкільні предмети</a></li>
                @endif
                <li class="dropdown">
                    <span>
                        <span>Розклад занять</span>&nbsp;<i class="bi bi-chevron-down dropdown-indicator"></i>
                    </span>
                    <ul>
                        @if (Auth::check())
                            @can('create schedule')
                                <li><a href="{{ route('schedules.create') }}">Створити розклад занять</a></li>
                            @endcan
                            @can('view own schedule')
                                <li>
                                    <a href="{{ route('schedules.show', ['type' => 'usersOwn', 'schedule' => Auth::user()->id]) }}">
                                        Мій розклад занять
                                    </a>
                                </li>
                            @endcan
                        @endif
                        <li>
                            <a href="{{ route('schedules.index', ['type' => 'classes']) }}">Розклад занять для учнів</a>
                        </li>
                        <li>
                            <a href="{{ route('schedules.index', ['type' => 'teachers']) }}">
                                Розклад занять для вчителів
                            </a>
                        </li>
                    </ul>
                </li>
                    @if (Auth::check())
                        @can('view own diary')
                            <li><a href="{{ route('diary.show', ['studentId' => Auth::user()->id]) }}">Щоденник</a></li>
                        @endcan
                        @can('view child diary') {{-- can do only parents--}}
                            @foreach(Auth::user()->children as $child)
                                <li>
                                    <a href="{{ route('diary.show', ['studentId' => $child->id]) }}">
                                        Щоденник {{ $child->firstname }}
                                    </a>
                                </li>
                            @endforeach
                        @endcan
                        @canany(['create journal', 'view journal', 'update lessons progress'])
                            <li class="dropdown">
                            <span>
                                <span>Журнали</span>&nbsp;<i class="bi bi-chevron-down dropdown-indicator"></i>
                            </span>
                                <ul>
                                    @can('create journal')
                                        <li><a href="{{ route('journals.create') }}">Створити класний журнал</a></li>
                                    @endcan
                                    @can('view journal')
                                        <li>
                                            <a href="{{ route('journals.index', ['type' => 'allJournals']) }}">
                                                Список класних журналів
                                            </a>
                                        </li>
                                    @endcan
                                    @can('update lessons progress')
                                        <li>
                                            <a href="{{ route('journals.index', ['type' => 'myJournals']) }}">
                                                Мої журнали
                                            </a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>
                        @endcanany
                @endif
            </ul>
        </li>
        @can('create class')
            <li class="dropdown">
                <span>
                    <span><a href="{{ route('school-classes.index') }}">Класи</a></span>&nbsp;<i
                        class="bi bi-chevron-down dropdown-indicator"></i>
                </span>
                <ul>
                    <li><a href="{{ route('school-classes.create') }}">Створити клас</a></li>
                </ul>
            </li>
        @else
            <li><a href="{{ route('school-classes.index') }}">Класи</a></li>
        @endcan
        <li><a href="{{ url('/contact_us') }}">Зворотній зв'язок</a></li>
    </ul>
    @if (Auth::check())
        <div class="username dropdown col-xl-2 col-11 row align-items-center">
            <span>
                <span id="open-navbar-drawer-user-actions-btn" class="username-btn">
                    {{Auth::user()->firstname}} {{Auth::user()->lastname}}
                    <i class="bi bi-person"></i>
                </span>
            </span>
            <ul id="user-actions">
                @can('view user list')
                    <li class="d-flex flex-row justify-content-start w-full">
                        <a href="{{ route('users.index') }}">
                            Список користувачів
                        </a>
                    </li>
                @endcan
                @can('create user')
                    <li class="d-flex flex-row justify-content-start w-full">
                        <a href="{{ route('users.create', ['type' => 'employee']) }}">
                            Додати працівника
                        </a>
                    </li>
                    <li class="d-flex flex-row justify-content-start border-bottom w-full">
                        <a href="{{ route('users.create', ['type' => 'student']) }}">
                            Додати учня
                        </a>
                    </li>
                @endcan
                @can('create study year')
                    <li class="d-flex flex-row justify-content-start border-bottom w-full">
                        <a href="{{ route('study-years.create') }}">
                            Додати навчальний рік
                        </a>
                    </li>
                @endcan

                <li class="d-flex flex-row justify-content-start w-full">
                    <a href="{{ route('users.show', ['user' => Auth::user()->id]) }}">
                        Обліковий запис
                    </a>
                </li>
                <li class="d-flex flex-row justify-content-start w-full">
                    @if(Auth::user()->getRoleNames()->contains('parent'))
                        <a href="{{ route('users.edit', ['user' => Auth::user()->id, 'type' => 'parent']) }}">
                            Змінити дані облікового запису
                        </a>
                    @elseif(Auth::user()->getRoleNames()->contains('student'))
                        <a href="{{ route('users.edit', ['user' => Auth::user()->id, 'type' => 'student']) }}">
                            Змінити дані облікового запису
                        </a>
                    @else
                        <a href="{{ route('users.edit', ['user' => Auth::user()->id, 'type' => 'employee']) }}">
                            Змінити дані облікового запису
                        </a>
                    @endif
                </li>
                <li class="d-flex flex-row justify-content-start w-full">
                    <form class="delete-user-form" method="POST"
                          action="{{ route('users.destroy', ['user' => Auth::user()->id]) }}">
                        @csrf
                        @method('DELETE')
                        <span onclick="deletionSubmit('Ви впевнені, що хочете видалити свій обліковий запис?',
                                this.closest('form'))">
                            Видалити обліковий запис
                        </span>
                    </form>
                </li>
                <li class="d-flex flex-row justify-content-start w-full border-bottom">
                    <a href="{{ route('password.reset.link') }}">
                        Змінити пароль
                    </a>
                </li>
                <li class="d-flex justify-content-end">
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <span class="logout-button mx-3"
                              onclick="event.preventDefault(); this.closest('form').submit();">
                            Вийти
                        </span>
                    </form>
                </li>
            </ul>
        </div>
    @else
        <span class="col-xl-1 col-11 login-button">
            <a href="{{ url('/login') }}">Ввійти&nbsp;<i class="bi bi-person"></i></a>
        </span>
    @endif
</nav>

