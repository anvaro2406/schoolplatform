<div id="navbar-drawer" class="d-none">
    <i id="close-navbar-drawer-btn" class="bi bi-x"></i>
    <div id="navbar-drawer-content"></div>
</div>
<div id="sidebar-drawer" class="d-none">
    <i id="close-sidebar-drawer-btn" class="bi bi-x"></i>
    <div id="sidebar-drawer-content"></div>
</div>
