<form class="d-flex flex-row justify-center align-items-center row m-2 school-class-selection-form">
    <x-label id="schoolClassLabel" for="schoolClass" class="col-xxl-4 col-xl-5 col-lg-6 col-md-8 text-center"
             :value="__('Виберіть клас, дані якого хочете переглянути: ')"></x-label>
    <x-select id="schoolClass" onchange="changeSchoolClassSelect(this, '{{ url('/school-classes') }}')"
              class="col-xl-2 col-md-3 col-sm-5 col-8">
        <option hidden selected disabled>Назва класу</option>
        @foreach($schoolClassesList as $id => $name)
            @if($id == $selectedId)
                <option selected value="{{ $id }}">
                    {{ $name }}
                </option>
            @else
                <option value="{{ $id }}">
                    {{ $name }}
                </option>
            @endif
        @endforeach
    </x-select>
</form>
