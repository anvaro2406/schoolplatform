<form class="d-flex flex-row justify-center align-items-center row m-2 fs-4">
    @if($type == 'classes')
        <x-label for="scheduleOwner" class="col-xxl-5 col-xl-6 col-lg-7 col-md-9 text-center"
                 :value="__('Виберіть клас, розклад якого хочете переглянути: ')"/>
        <x-select id="scheduleOwner" class="col-xl-1 col-sm-2 col-4 text-wrap fs-5"
                  onchange="changeScheduleOwnerSelect('{{ $type }}', this.value)">
            <option class="text-wrap" value="" selected disabled hidden></option>
            @foreach($scheduleOwners as $id => $name)
                @if($id == $selectedScheduleOwnerId)
                    <option selected value="{{ $id }}">
                        {{ $name }}
                    </option>
                @else
                    <option value="{{ $id }}">
                        {{ $name }}
                    </option>
                @endif
            @endforeach
        </x-select>
    @else
        <x-label for="scheduleOwner" class="col-xxl-5 col-xl-6 col-lg-7 text-center"
                 :value="__('Виберіть вчителя, чий розклад хочете переглянути: ')"/>
        <x-select id="scheduleOwner" class="col-xl-3 col-lg-4 col-md-5 col-sm-7 col-10 text-wrap fs-5"
                  onchange="changeScheduleOwnerSelect('{{ $type }}', this.value)">
            <option class="text-wrap" value="" selected disabled hidden></option>
            @foreach($scheduleOwners as $id => $name)
                @if($id == $selectedScheduleOwnerId)
                    <option selected value="{{ $id }}">
                        {{ $name }}
                    </option>
                @else
                    <option value="{{ $id }}">
                        {{ $name }}
                    </option>
                @endif
            @endforeach
        </x-select>
    @endif
</form>
