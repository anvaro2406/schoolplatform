<form class="d-flex flex-row justify-center align-items-center row m-2 journal-selection-form">
    <x-label id="journalLabel" for="studyYear" class="col-xxl-6 text-center mb-xxl-0 mb-2"
             :value="__('Виберіть навчальний рік і клас, журнал якого хочете переглянути: ')"></x-label>
    <x-select id="studyYear" class="col-xl-2 col-md-3 col-sm-4 col-8 mb-sm-0 mb-2 text-wrap"
              onchange="changeJournalStudyYearSelect({{ $selectedSchoolClassId }}, false)">
        @foreach($studyYears as $id => $name)
            @if($id == $selectedStudyYearId)
                <option selected value="{{ $id }}">
                    {{ $name }}
                </option>
            @else
                <option value="{{ $id }}">
                    {{ $name }}
                </option>
            @endif
        @endforeach
    </x-select>
    <x-select id="schoolClass" class="col-xl-1 col-md-3 col-sm-3 col-8 ms-sm-3 mb-sm-0 mb-2">
        @foreach($schoolClasses as $id => $name)
            @if($id == $selectedSchoolClassId)
                <option selected value="{{ $id }}">
                    {{ $name }}
                </option>
            @else
                <option value="{{ $id }}">
                    {{ $name }}
                </option>
            @endif
        @endforeach
    </x-select>
    <div class="flex items-center justify-center col-xl-2 col-md-3 col-sm-4 col-8">
        <x-button class="sb-btn" onclick="submitJournalSelect()">Підтвердити</x-button>
    </div>
</form>
