<form class="d-flex flex-col align-items-center m-2 d-none overflow-auto"
      method="POST" action="{{ route('feedbacks.store') }}" id="feedback-form-modal-window">
    @csrf
    <x-session-status class="mb-2 w-full px-4" :status="session('status')"></x-session-status>
    <x-errors class="mb-2 w-full px-4" :errors="$errors"></x-errors>
    <i id="close-feedback-form-btn" class="bi bi-x" onclick="feedbackFormModalWindowClose()"></i>
    <x-label id="studentName" for="student" class="w-full text-center mb-2 fs-4 color-white"></x-label>
    <x-input id="student" name="studentId" hidden value="{{ old('studentId') }}"/>
    <x-label for="subject" class="w-full text-center mb-2 fs-4 color-white" :value="__('Тема')"></x-label>
    <x-input id="subject" class="col-9 mb-2 w-75 fs-5" name="subject" type="text" required maxlength="64" minlength="8"
             value="{{ old('subject') }}"/>
    <x-label for="description" class="w-full text-center mb-2 fs-4 color-white" :value="__('Опис')"></x-label>
    <textarea id="description" name="description" required maxlength="400" minlength="32" class="w-75 fs-5 mb-2 rounded"
              rows="10">{{ old('description') }}</textarea>
    <div class="flex items-center justify-center col-xl-2 col-md-3 col-sm-4 col-8">
        <x-button class="sb-btn" type="submit">Підтвердити</x-button>
    </div>
</form>
