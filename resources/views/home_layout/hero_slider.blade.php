<section id="hero-slider" class="hero-slider">
    <div class="container" data-aos="fade-in">
        <div class="row">
            <div class="col-12">
                <div class="swiper sliderFeaturedPosts">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <a href="{{ url('/') }}" class="custom-img-bg img-bg d-flex align-items-end"
                               style="background-image: url({{ asset('/assets/img/creativity.jpg') }});">
                                <div class="img-bg-inner">
                                    <h2>Ми - творчі</h2>
                                </div>
                            </a>
                        </div>
                        <div class="swiper-slide">
                            <a href="{{ url('/') }}" class="custom-img-bg img-bg d-flex align-items-end"
                               style="background-image: url({{ asset('/assets/img/education.jpg') }});">
                                <div class="img-bg-inner">
                                    <h2>Ми - розумні</h2>
                                </div>
                            </a>
                        </div>
                        <div class="swiper-slide">
                            <a href="{{ url('/') }}" class="custom-img-bg img-bg d-flex align-items-end"
                               style="background-image: url({{ asset('/assets/img/sport.jpg') }});">
                                <div class="img-bg-inner">
                                    <h2>Ми - спортивні</h2>
                                </div>
                            </a>
                        </div>
                        <div id="we-patriotic" class="swiper-slide">
                            <a href="{{ url('/') }}" class="custom-img-bg img-bg d-flex align-items-end"
                               style="background-image: url({{ asset('/assets/img/patriotism.jpg') }});">
                                <div class="img-bg-inner">
                                    <h2>Ми - патріотичні</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="custom-swiper-button-next">
                        <span class="bi-chevron-right"></span>
                    </div>
                    <div class="custom-swiper-button-prev">
                        <span class="bi-chevron-left"></span>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </div>
</section>
