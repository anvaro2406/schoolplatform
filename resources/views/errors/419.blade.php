@extends('basic_layout')

@section('title', 'Помилка 419')

@section('content')
    <div class="container-fluid d-flex flex-row justify-content-center row m-0 p-0">
        <p class="col-md-6 d-flex flex-column justify-content-center text-center p-0 fs-2 text-danger">
            Сталася помилка 419.
        </p>
    </div>
    <div class="container-fluid d-flex flex-row justify-content-center row m-0 p-0">
        <p class="col-md-6 d-flex flex-column justify-content-center text-center p-0 fs-3">
            Щоб виправити помилку, оновіть сторінку та зачекайте 60 секунд. Якщо після цих дій робота системи не
            відновилася, то зверніться до системного адміністратора і опишіть, коли та за яких умов сталася помилка.
        </p>
    </div>
@endsection
