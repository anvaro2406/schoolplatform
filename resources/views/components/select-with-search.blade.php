@props(['disabled' => false])
<div
    class="w-full p-0 d-flex flex-col rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
    <input type="text" placeholder="Пошук.." onkeyup="selectSearchFunction(this)"
           class="w-full input-field rounded-top shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
    <select {{ $disabled ? 'disabled' : '' }}{!! $attributes->merge(['class' => 'input-field rounded-bottom shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"'])!!}>
        {{ $slot }}
    </select>
</div>
