@extends('basic_layout')

@section('title', 'Створення новини')

@section('content')
    <div id="create-news-content" class="d-flex flex-row justify-content-center align-items-center">
        <div class="form w-full px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg m-4">
            <x-session-status class="mb-4" :status="session('status')"></x-session-status>
            <x-errors class="mb-4" :errors="$errors"></x-errors>
            <form method="POST" action="{{ route('news.store') }}">
                @csrf
                <div class="row">
                    <div class="col-xxl-3 col-xl-3 pb-2">
                        <x-label for="category" :value="__('Категорія')"></x-label>
                        <x-input list="categories" name="category" id="category" type="text" required max="255"
                                 class="input-field mt-1 w-full" :value="old('category')"></x-input>
                        <datalist id="categories">
                            @foreach($categories as $category)
                                <option value="{{ $category->name }}">
                            @endforeach
                        </datalist>
                    </div>
                    <div class="col-xxl-9 col-xl-9 pb-2">
                        <x-label for="title" :value="__('Назва')"></x-label>
                        <x-input id="title" class="input-field mt-1 w-full" type="text" name="title"
                                 :value="old('title')" required max="255"></x-input>
                    </div>
                </div>
                <div class="row">
                    <x-label for="description" class="pb-2">Опис новини:</x-label>
                    <textarea id="description" name="description" class="w-full input-field" rows="5">
                        {{ old('description') }}
                    </textarea>
                </div>
                <div class="row">
                    <x-label for="wyziwyg-editor" class="pb-2">Вміст новини:</x-label>
                    <textarea id="wyziwyg-editor" name="content">{{ old('content') }}</textarea>
                    <input id="wyziwyg-image" type="file" name="wyziwyg-image" class="d-none" onchange=""/>
                </div>
                <div class="flex items-center justify-center mt-3">
                    <x-submit-button class="sb-btn ml-3">Створити</x-submit-button>
                </div>
            </form>
        </div>
    </div>
@endsection
