@extends('basic_layout')

@section('title', $news->title)

@include('news_management_layout.categories_sidebar', ['categories' => $categories])

@section('content')
    <x-errors class="mb-2 fs-5" :errors="$errors"></x-errors>
    <b class="row w-full fs-2 d-flex justify-content-center text-center">{{ $news->title }}</b>
    <div class="post-meta w-full text-center">
        <span>
            <a href="{{ route('users.show', ['user' => $news->author->id]) }}">
                {{ $news->author->firstname . ' ' . $news->author->lastname }}
            </a>
        </span>
        <span class="mx-1">&bullet;</span>
        <span>
            <a href="{{ route('news.category-name', ['category' => $news->category->name]) }}">
                {{ $news->category->name }}
            </a>
        </span>
        <span class="mx-1">&bullet;</span>
        <span>{{ $news->created_at->translatedFormat('j F Y, G:i') }}</span>
        @if($news->updated_at != $news->created_at)
            <span class="mx-1">&bullet;</span>
            <span><i>Змінено: {{ $news->updated_at->translatedFormat('j F Y, G:i') }}</i></span>
        @endif
    </div>
    <div class="mb-2">
        {!! $news->content !!}
    </div>
@endsection
