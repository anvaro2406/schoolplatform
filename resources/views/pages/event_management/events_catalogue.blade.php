@extends('basic_layout')

@section('title', 'Шкільні події')

@include('event_management_layout.categories_sidebar', ['categories' => $categories])

@section('content')
    <x-errors class="mb-2 fs-5" :errors="$errors"></x-errors>
    @isset($isSearch)
        @isset($noResult)
            <div class="row my-2">
                <p class="text-center fs-3 col-12">Результати пошуку відсутні.</p>
            </div>
        @else
            <div class="row my-2">
                <p class="text-center fs-3 col-12">Результати пошуку:</p>
            </div>
        @endisset
    @endisset
    @isset($sortColumn)
        <div class="row my-2 justify-content-end">
            <form class="form col-xxl-10 col-lg-9 col-md-12 col-12 my-1"
                  method="GET" action="{{ route('events.search') }}" id="search">
                <div class="input-group">
                    <x-input name="searchParameter" type="search" placeholder="Пошук"
                             class="form-control input-field w-full border-end-0"></x-input>
                    <x-button class="btn input-group-append" type="submit">
                        <i class="bi bi-search fs-5"></i>
                    </x-button>
                </div>
            </form>
            <form class="col-xxl-2 col-lg-3 col-md-4 col-sm-6 col-8 my-1" method="GET"
                  action="{{ route('events.sorted') }}" id="sort-by-form">
                <x-select name="sortColumn" onchange="changeSortBy()" style="font-size: 18px;" class="w-full">
                    <option value="title" @selected($sortColumn == 'title')>Назва</option>
                    <option value="dateFarthest" @selected($sortColumn == 'dateFarthest')>Дата (спочатку віддаленіші)
                    </option>
                    <option value="dateNearest" @selected($sortColumn == 'dateNearest')>Дата (спочатку ближчі)</option>
                    <option value="category" @selected($sortColumn == 'category')>Категорія</option>
                </x-select>
            </form>
        </div>
    @endisset
    @if(isset($events) && !$events->isEmpty())
        <div class="col">
            @foreach ($events as $event)
                @include('event_management_layout.event_card', ['event' => $event])
            @endforeach
            {{ $events->onEachSide(2)->links() }}
        </div>
    @else
        <div class="row my-2">
            <p class="text-center fs-3 col-12">Опубліковані події відсутні.</p>
        </div>
    @endif
@endsection
