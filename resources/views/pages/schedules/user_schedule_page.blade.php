@extends('basic_layout')

@section('title', 'Розклад занять для ' . $user->full_name)

@section('content')
    <x-errors class="mb-2 fs-5" :errors="$errors"></x-errors>
    @if(isset($type) && !$scheduleOwners->isEmpty() && !$schedule->isEmpty())
        @if($type != 'usersOwn')
            @include('schedule_layout.selection_form', [
                    'type' => $type, 'selectedScheduleOwnerId' => $user->id, 'scheduleOwners' => $scheduleOwners
            ])
        @endif
        <div class="d-flex flex-col align-items-center">
            <h2 class="fs-2 text-center my-2">
                {{ 'Розклад занять для '}}
                <br>
                {{ $user->full_name }}
            </h2>
            <div class="w-full bg-white shadow-md overflow-auto sm:rounded-lg my-2 border p-2 row d-flex justify-content-center">
                @foreach($schedule as $day => $timeSlots)
                    <div class="col-xxl-3 col-xl-4 col-lg-5 col-md-6 col-sm-9 px-1 d-flex flex-column">
                        <h2 class="fs-3 text-center my-2">{{ __('general.' . $day) }}</h2>
                        <table class="table table-bordered border-dark text-center m-0 fs-5 flex-grow-1 schedule-table">
                            <thead>
                            <tr class="align-middle">
                                <th scope="col" class="time-slot-column">Часове вікно</th>
                                <th scope="col">Предмет</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($timeSlots as $timeSlot)
                                @php($subject = $timeSlot['subjects']->first())
                                <tr class="align-middle">
                                    <td>{{ $timeSlot['timeSlot']->rangeName }}</td>
                                    <td>
                                        <a href="{{ route('subjects.show', ['subject' => $subject->abstractSubject->id]) }}">
                                            {{ $subject->abstractSubject->title }}
                                        </a>
                                        <br>
                                        @if($user->hasRole('teacher'))
                                            <a href="{{ route('school-classes.show', ['school_class' => $subject->schoolClass->id]) }}"
                                               class="fs-6">
                                                {{ $subject->schoolClass->name . ' клас' }}
                                            </a>
                                            <br>
                                            @if(Auth::check() && Auth::user()->can('view lessons progress'))
                                            <a href="{{ route('journal-subject-pages.show', ['journalSubjectPage' => $subject->id]) }}"
                                               class="fs-6">
                                                {{ 'Сторінка журналу' }}
                                            </a>
                                            <br>
                                                @endif
                                        @else
                                            <a href="{{ route('employees.show', ['employee' => $subject->teacher->id]) }}"
                                               class="fs-6">
                                                {{ $subject->teacher->full_name }}
                                            </a>
                                            <br>
                                        @endif
                                        @if($subject->video_conference_link)
                                            <a href="{{ $subject->video_conference_link }}" class="fs-6 color-blue">
                                                {{ substr($subject->video_conference_link, 0, 12) }}
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endforeach
            </div>
        </div>
    @elseif(isset($type) && !$scheduleOwners->isEmpty())
        @if($type != 'usersOwn')
            @include('schedule_layout.selection_form', [
                'type' => $type, 'selectedScheduleOwnerId' => $user->id, 'scheduleOwners' => $scheduleOwners
            ])
            <div class="row my-2">
                <p class="text-center fs-3 col-12">Дані розкладу вибраного вчителя ще не оцифровано.</p>
            </div>
        @else
            <div class="row my-2">
                <p class="text-center fs-3 col-12">Ваш розклад ще не оцифровано.</p>
            </div>
        @endif
    @else
        <div class="row my-2">
            <p class="text-center fs-3 col-12">Потрібна інформація ще не оцифрована.</p>
        </div>
    @endif
@endsection
