@extends('basic_layout')

@if($type === 'classes')
    @section('title', 'Розклад занять для учнів')
@else
    @section('title', 'Розклад занять для вчителів')
@endif

@section('content')
    <x-errors class="mb-4" :errors="$errors"></x-errors>
    @if(isset($type) && !$scheduleOwners->isEmpty())
        @include('schedule_layout.selection_form', [
                'type' => $type, 'selectedScheduleOwnerId' => -1, 'scheduleOwners' => $scheduleOwners
        ])
    @else
        <div class="row my-2">
            <p class="text-center fs-3 col-12">Потрібна інформація ще не оцифрована.</p>
        </div>
    @endif
@endsection
