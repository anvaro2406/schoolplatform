@extends('basic_layout')

@section('title', 'Створення розкладу')

@section('content')
    <div class="d-flex flex-row justify-content-center align-items-center">
        <div class="form w-full px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg m-4">
            <x-session-status class="mb-4" :status="session('status')"></x-session-status>
            <x-errors class="mb-4" :errors="$errors"></x-errors>
            <form method="POST" action="{{ route('schedules.store') }}">
                @csrf
                <div class="d-flex flex-row justify-center align-items-center row mb-2">
                    <x-label for="schoolClass" class="col-xxl-4 col-xl-5 col-lg-6 col-md-8 mr-2 text-center"
                             :value="__('Виберіть клас, для якого створюється розклад: ')"/>
                    <x-select name="schoolClass" id="schoolClass"
                              class="col-xl-2 col-md-3 col-sm-4 col-8 input-field overflow-auto" required
                              onchange="changeScheduleClassSelect(this.value, {{ $currentStudyYearId }})">
                        <option class="text-wrap" value="" selected disabled hidden>Клас</option>
                        @foreach($schoolClasses as $schoolClass)
                            @if($schoolClass->id == old('schoolClass'))
                                <option class="text-wrap" value="{{ $schoolClass->id }}" selected>
                                    {{ $schoolClass->name}}
                                </option>
                            @else
                                <option class="text-wrap" value="{{ $schoolClass->id }}">
                                    {{ $schoolClass->name}}
                                </option>
                            @endif
                        @endforeach
                    </x-select>
                </div>
                <div class="d-flex justify-content-center row overflow-auto">
                    @foreach($days as $dayIndex => $day)
                        <div class="col-xxl-8 col-xl-10 col-12">
                            <x-label class="text-center fs-4 mb-1 mt-2" :value="__('general.' . $day)"></x-label>
                            <table class="table table-bordered text-center m-0 schedule-creation-table">
                                <thead>
                                <tr class="align-middle">
                                    <th scope="col" rowspan="2" class="time-slot-column">Часове вікно</th>
                                    <th scope="col" rowspan="2" class="is-group-column">Розділення на групи</th>
                                    <th scope="col" colspan="2">Предмет</th>
                                </tr>
                                <tr>
                                    <th scope="col">1 група</th>
                                    <th scope="col">2 група</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($timeSlots as $timeSlotIndex => $timeSlot)
                                    @php
                                        $standardInputIndex = $timeSlots->count() * $dayIndex + $timeSlotIndex;
                                        $secondGroupInputIndex = $days->count() * $timeSlots->count() + $timeSlots->count() * $dayIndex + $timeSlotIndex
                                    @endphp
                                    <tr class="align-middle time-slots-row">
                                        <td>{{ $timeSlot->rangeName}}</td>
                                        <td>
                                            @if(old('checkboxes.' . $standardInputIndex) == null)
                                                <x-input type="checkbox" name="checkboxes[{{ $standardInputIndex }}]"
                                                         onchange="changeJournalGroupCheckbox(this)"/>
                                            @else
                                                <x-input type="checkbox" name="checkboxes[{{ $standardInputIndex }}]"
                                                         onchange="changeJournalGroupCheckbox(this)" checked/>
                                            @endif
                                        </td>
                                        <td colspan="2">
                                            <x-input name="timeSlots[{{ $standardInputIndex }}][timeSlotId]"
                                                     value="{{ $timeSlot->id }}" hidden/>
                                            <x-input name="timeSlots[{{ $standardInputIndex }}][day]"
                                                     value="{{ $day }}" hidden/>
                                            <x-select name="timeSlots[{{ $standardInputIndex }}][subjectId]"
                                                      class="input-field overflow-auto w-full subject-select">
                                                <option class="text-wrap" value=""></option>
                                                @foreach($wholeClassSubjects as $subject)
                                                    <option class="text-wrap"
                                                            @selected($subject->id == old('timeSlots.' . $standardInputIndex . '.subjectId'))
                                                            data-school-class-id="{{ $subject->class_id }}"
                                                            data-study-year-id="{{ $subject->study_year_id }}"
                                                            value="{{ $subject->id }}">
                                                        {{ $subject->abstractSubject->title }}
                                                    </option>
                                                @endforeach
                                            </x-select>
                                        </td>
                                        <td class="d-none">
                                            <x-input name="timeSlots[{{ $standardInputIndex }}][timeSlotId]"
                                                     value="{{ $timeSlot->id }}" hidden disabled/>
                                            <x-input name="timeSlots[{{ $standardInputIndex }}][day]"
                                                     value="{{ $day }}" hidden disabled/>
                                            <x-select name="timeSlots[{{ $standardInputIndex }}][subjectId]"
                                                      class="input-field overflow-auto w-full subject-select" disabled>
                                                <option class="text-wrap" value=""></option>
                                                @foreach($firstGroupSubjects as $subject)
                                                    <option class="text-wrap"
                                                            @selected($subject->id == old('timeSlots.' . $standardInputIndex . '.subjectId'))
                                                            data-school-class-id="{{ $subject->class_id }}"
                                                            data-study-year-id="{{ $subject->study_year_id }}"
                                                            value="{{ $subject->id }}">
                                                        {{ $subject->abstractSubject->title }}
                                                    </option>
                                                @endforeach
                                            </x-select>
                                        </td>
                                        <td class="d-none">
                                            <x-input
                                                name="timeSlots[{{ $secondGroupInputIndex }}][timeSlotId]"
                                                value="{{ $timeSlot->id }}" hidden disabled/>
                                            <x-input name="timeSlots[{{ $secondGroupInputIndex }}][day]"
                                                     value="{{ $day }}" hidden disabled/>
                                            <x-select name="timeSlots[{{ $secondGroupInputIndex }}][subjectId]"
                                                      class="input-field overflow-auto w-full subject-select" disabled>
                                                <option class="text-wrap" value=""></option>
                                                @foreach($secondGroupSubjects as $subject)
                                                    <option class="text-wrap"
                                                            @selected($subject->id == old('timeSlots.' . $secondGroupInputIndex . '.subjectId'))
                                                            data-school-class-id="{{ $subject->class_id }}"
                                                            data-study-year-id="{{ $subject->study_year_id }}"
                                                            value="{{ $subject->id }}">
                                                        {{ $subject->abstractSubject->title }}
                                                    </option>
                                                @endforeach
                                            </x-select>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endforeach
                    <x-label class="mb-2 text-center">
                        <i>*Для відображення доступного списку предметів спочатку потрібно вибрати клас</i>
                    </x-label>
                </div>
                <div class="flex items-center justify-center mt-3">
                    <x-submit-button class="sb-btn ml-3">Створити</x-submit-button>
                </div>
            </form>
        </div>
    </div>
@endsection
