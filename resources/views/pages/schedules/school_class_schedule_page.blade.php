@extends('basic_layout')

@section('title', 'Розклад занять ' . $schoolClass->name . ' класу')

@section('content')
    <x-errors class="mb-2 fs-5" :errors="$errors"></x-errors>
    @if(isset($type) && !$scheduleOwners->isEmpty() && !$schedule->isEmpty())
        @include('schedule_layout.selection_form', [
                'type' => $type, 'selectedScheduleOwnerId' => $schoolClass->id, 'scheduleOwners' => $scheduleOwners
        ])
        <div class="d-flex flex-col align-items-center">
            <h2 class="fs-2 text-center my-2">{{ 'Розклад занять ' . $schoolClass->name . ' класу' }}</h2>
            <div
                class="w-full bg-white shadow-md overflow-auto sm:rounded-lg my-2 border p-2 row d-flex justify-content-center">
                @foreach($schedule as $day => $timeSlots)
                    <div class="col-xxl-4 col-lg-6 col-md-9 px-1 d-flex flex-column">
                        <h2 class="fs-3 text-center my-2">{{ __('general.' . $day) }}</h2>
                        <table class="table table-bordered border-dark text-center m-0 fs-5 flex-grow-1">
                            <thead>
                            <tr class="align-middle">
                                <th scope="col" rowspan="2">Часове вікно</th>
                                <th scope="col" colspan="2">Предмет</th>
                            </tr>
                            <tr>
                                <th scope="col">1 група</th>
                                <th scope="col">2 група</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($timeSlots as $timeSlot)
                                <tr class="align-middle">
                                    <td>{{ $timeSlot['timeSlot']->rangeName }}</td>
                                    @if($timeSlot['subjects']->count() == 2)
                                        @php
                                            $firstGroupSubject = $timeSlot['subjects']->where('group', 'first_group')->first();
                                            $secondGroupSubject = $timeSlot['subjects']->where('group', 'second_group')->first();
                                        @endphp
                                        <td>
                                            <a href="{{ route('subjects.show', ['subject' => $firstGroupSubject->abstractSubject->id]) }}">
                                                {{ $firstGroupSubject->abstractSubject->title }}
                                            </a>
                                            <br>
                                            <a href="{{ route('employees.show', ['employee' => $firstGroupSubject->teacher->id]) }}"
                                               class="fs-6">
                                                {{ $firstGroupSubject->teacher->full_name }}
                                            </a>
                                            <br>
                                            @if($firstGroupSubject->video_conference_link)
                                                <a href="{{ $firstGroupSubject->video_conference_link }}"
                                                   class="fs-6 color-blue">
                                                    {{ substr($firstGroupSubject->video_conference_link, 0, 12) }}
                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('subjects.show', ['subject' => $secondGroupSubject->abstractSubject->id]) }}">
                                                {{ $secondGroupSubject->abstractSubject->title }}
                                            </a>
                                            <br>
                                            <a href="{{ route('employees.show', ['employee' => $secondGroupSubject->teacher->id]) }}"
                                               class="fs-6">
                                                {{ $secondGroupSubject->teacher->full_name }}
                                            </a>
                                            <br>
                                            @if($secondGroupSubject->video_conference_link)
                                                <a href="{{ $secondGroupSubject->video_conference_link }}"
                                                   class="fs-6 color-blue">
                                                    {{ substr($secondGroupSubject->video_conference_link, 0, 12) }}
                                                </a>
                                            @endif
                                        </td>
                                    @else
                                        <td colspan="2">
                                            <a href="{{ route('subjects.show', ['subject' => $timeSlot['subjects']->first()->abstractSubject->id]) }}">
                                                {{ $timeSlot['subjects']->first()->abstractSubject->title }}
                                            </a>
                                            <br>
                                            <a href="{{ route('employees.show', ['employee' => $timeSlot['subjects']->first()->teacher->id]) }}"
                                               class="fs-6">
                                                {{ $timeSlot['subjects']->first()->teacher->full_name }}
                                            </a>
                                            <br>
                                            @if($timeSlot['subjects']->first()->video_conference_link)
                                                <a href="{{ $timeSlot['subjects']->first()->video_conference_link }}"
                                                   class="fs-6 color-blue">
                                                    {{ substr($timeSlot['subjects']->first()->video_conference_link, 0, 12) }}
                                                </a>
                                            @endif
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row d-flex justify-center my-2">
            @can('update schedule')
                <a href="{{ route('schedules.edit', ['schedule' => $schoolClass->id]) }}"
                   class="col-xxl-2 col-lg-3 col-md-4 col-6 p-0">
                    <x-button class="w-full justify-content-center">Змінити розклад</x-button>
                </a>
            @endcan
            @can('delete schedule')
                <form class="col-xxl-2 col-lg-3 col-md-4 col-6 p-0" method="POST"
                      action="{{ route('schedules.destroy', ['schedule' => $schoolClass->id]) }}">
                    @csrf
                    @method('DELETE')
                    <x-button class="w-full justify-content-center"
                              onclick="deletionSubmit('Ви впевнені, що хочете видалити даний розклад занять?', this.closest('form'))">
                        Видалити розклад
                    </x-button>
                </form>
            @endcan
        </div>
    @elseif(isset($type) && !$scheduleOwners->isEmpty())
        @include('schedule_layout.selection_form', [
                'type' => $type, 'selectedScheduleOwnerId' => $schoolClass->id, 'scheduleOwners' => $scheduleOwners
        ])
        <div class="row my-2">
            <p class="text-center fs-3 col-12">Дані розкладу вибраного класу ще не оцифровано.</p>
        </div>
    @else
        <div class="row my-2">
            <p class="text-center fs-3 col-12">Потрібна інформація ще не оцифрована.</p>
        </div>
    @endif
@endsection
