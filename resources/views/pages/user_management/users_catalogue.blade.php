@extends('basic_layout')

@section('title', 'Список користувачів')

@section('content')
    <x-errors class="mb-2 fs-5" :errors="$errors"></x-errors>
    <div class="overflow-auto my-1">
        <table class="user-list table table-striped table-bordered text-center m-0">
            <thead>
            <tr>
                <th scope="col">№</th>
                <th scope="col">Ім'я</th>
                <th scope="col">Прізвище</th>
                <th scope="col">По-батькові</th>
                <th scope="col">Стать</th>
                <th scope="col">Дата народження</th>
                <th scope="col">Адреса</th>
                <th scope="col">Телефон</th>
                <th scope="col">Пошта</th>
                <th scope="col">Ролі</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $index => $user)
                <tr class="align-middle">
                    <th scope="row">{{ $index + 1  }}</th>
                    <td>{{ $user->firstname }}</td>
                    <td>{{ $user->lastname }}</td>
                    <td>{{ $user->patronymic }}</td>
                    @if($user->gender == 'male')
                        <td>Чоловік</td>
                    @else
                        <td>Жінка</td>
                    @endif
                    <td>{{ $user->birth_date }}</td>
                    <td>{{ $user->address }}</td>
                    <td>{{ $user->phone }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @foreach ($user->getRoleNames() as $role)
                            {{ $role . " " }}
                        @endforeach
                    </td>
                    <td>
                        @if($user->getRoleNames()->contains('parent'))
                            <a href="{{ route('users.edit', ['user' => $user->id, 'type' => 'parent']) }}"
                               class="btn btn-warning"><i class="bi bi-pencil-fill"></i></a>
                        @elseif($user->getRoleNames()->contains('student'))
                            <a href="{{ route('users.edit', ['user' => $user->id, 'type' => 'student']) }}"
                               class="btn btn-warning"><i class="bi bi-pencil-fill"></i></a>
                        @else
                            <a href="{{ route('users.edit', ['user' => $user->id, 'type' => 'employee']) }}"
                               class="btn btn-warning"><i class="bi bi-pencil-fill"></i></a>
                        @endif
                    </td>
                    <td>
                        <form method="POST" action="{{ route('users.destroy', ['user' => $user->id]) }}">
                            @csrf
                            @method('DELETE')
                            <span class="btn btn-danger"
                                  onclick="deletionSubmit('Ви впевнені, що хочете видалити даного користувача?',
                                    this.closest('form'))">
                                <i class="bi bi-trash"></i>
                            </span>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {{ $users->onEachSide(2)->links() }}
@endsection
