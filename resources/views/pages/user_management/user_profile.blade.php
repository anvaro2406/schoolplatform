@extends('basic_layout')

@section('title', $user->firstname . ' ' . $user->lastname)

@section('content')
    <div class="user-profile row">
        <div class="col-lg-3 col-md-4 d-flex justify-content-center">
            <div class="user-photo-container">
                @if($user->photo_filepath != "")
                    <img src="@storageAsset($user->photo_filepath)" alt=""/>
                @else
                    <i class="bi bi-person fs-1"></i>
                @endif
            </div>
        </div>
        <div class="col-lg-9 col-md-8 text-break">
            <div class="section-header d-flex justify-content-between align-items-center mb-3">
                <h2>Особиста інформація</h2>
            </div>
            <div class="row">
                <div class="col-xxl-4 col-lg-6 border">
                    <p>Ім'я: {{ $user->firstname }}</p>
                    <p>Прізвище: {{ $user->lastname }}</p>
                    <p>По-батькові: {{ $user->patronymic }}</p>
                </div>
                <div class="col-xxl-4 col-lg-6 border">
                    @if($user->gender == 'male')
                        <p>Стать: чоловік</p>
                    @else
                        <p>Стать: жінка</p>
                    @endif
                    <p>Дата народження: {{ $user->birth_date }}</p>
                </div>
                <div class="col-xxl-4 border">
                    <p>Адреса: {{ $user->address }}</p>
                    <p>Телефон: {{ $user->phone ?? '-' }}</p>
                    <p>Пошта: {{ $user->email }}</p>
                </div>
            </div>
            @if($user->hasRole('student') && !$user->parents->isEmpty())
                <div class="section-header d-flex justify-content-between align-items-center mb-3">
                    <h2>Батьки</h2>
                </div>
                <div class="row">
                    @foreach($user->parents as $parent)
                        <div class="col-xxl-4 col-lg-6 border">
                            @if($parent->gender == 'male')
                                <p class="text-center fs-4"><strong>Батько</strong></p>
                            @else
                                <p class="text-center fs-4"><b>Матір</b></p>
                            @endif
                            <p>Ім'я: {{ $parent->firstname }}</p>
                            <p>Прізвище: {{ $parent->lastname }}</p>
                            <p>По-батькові: {{ $parent->patronymic }}</p>
                        </div>
                    @endforeach
                </div>
            @elseif($user->hasRole('parent') && !$user->children->isEmpty())
                <div class="section-header d-flex justify-content-between align-items-center mb-3">
                    <h2>Діти</h2>
                </div>
                <div class="row mb-2">
                    @foreach($user->children as $child)
                        <div class="col-xxl-4 col-lg-6 border">
                            <p>Ім'я: {{ $child->firstname }}</p>
                            <p>Прізвище: {{ $child->lastname }}</p>
                            <p>По-батькові: {{ $child->patronymic }}</p>
                        </div>
                    @endforeach
                </div>
            @elseif(!$user->hasAnyRole('parent', 'student') && $user->employeeProperty != null)
                <div class="section-header d-flex justify-content-between align-items-center mb-3">
                    <h2>Професійна інформація</h2>
                </div>
                <div class="row mb-2">
                    <div class="col-xl-4 border">
                        <p>Позиція: {{ $user->employeeProperty->position }}</p>
                        <p>Науковий ступінь: {{ $user->employeeProperty->science_degree }}</p>
                        <p>Досвід: {{ $user->employeeProperty->experience }}</p>
                    </div>
                    <div class="col-xl-8 border">
                        <p>Біографія:</p>
                        <p>{{ $user->employeeProperty->biography }}</p>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
