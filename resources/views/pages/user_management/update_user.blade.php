@extends('basic_layout')

@section('title', 'Зміна даних користувача')

@section('content')
    <div id="create-user-content" class="d-flex flex-row justify-content-center align-items-center">
        <div class="form w-full px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg m-4">
            <x-session-status class="mb-4" :status="session('status')"></x-session-status>
            <x-errors class="mb-4" :errors="$errors"></x-errors>
            <form method="POST" action="{{ route('users.update', ['user' => $user->id]) }}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    @if($type == 'employee')
                        <div class="col-xxl-2 col-xl-3 col-lg-4 pb-2 text-center">
                            <x-label for="roles" :value="__('Роль')"></x-label>
                            <x-select size="7" multiple name="roles[]" id="role" class="input-field overflow-auto"
                                      required>
                                @foreach($roles as $role)
                                    @if(in_array($role->name, old('roles', $user->getRoleNames()->toArray())))
                                        <option value="{{ $role->name }}" selected>{{ $role->name }}</option>
                                    @else
                                        <option value="{{ $role->name }}">{{ $role->name}}</option>
                                    @endif
                                @endforeach
                            </x-select>
                        </div>
                        <div class="col-xxl-10 col-xl-9 col-lg-8 py-2 border text-center">
                            <div class="row">
                                @include('user_management_layout.common_update_form', ['user' => $user])
                            </div>
                        </div>
                    @else
                        <div class="py-2 border text-center">
                            <div class="row">
                                @include('user_management_layout.common_update_form', ['user' => $user])
                            </div>
                        </div>
                    @endif

                </div>
                @if($type == 'employee')
                    <div class="row py-2 border mt-1">
                        @include('user_management_layout.update_employee_properties',
                            ['employeeProperties' => $user->employeeProperty])
                    </div>
                @endif
                <div class="flex items-center justify-center mt-3">
                    <x-submit-button class="sb-btn ml-3">Змінити дані</x-submit-button>
                    @if($type == 'student')
                        <a href="{{ route('users.create', ['type' => 'parent', 'user' => $user->id]) }}">
                            <x-button class="sb-btn ml-3">Додати батьків</x-button>
                        </a>
                    @endif
                </div>
            </form>
        </div>
    </div>
@endsection
