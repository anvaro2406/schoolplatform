@extends('basic_layout')

@section('title', 'Красилівська школа №1')

@section('content')
    @include('home_layout.hero_slider')
    <div class="section-header d-flex justify-content-between align-items-center mb-3">
        <h2>Наші досягнення</h2>
    </div>
    @include('home_layout.achievements')
    <div class="section-header d-flex justify-content-between align-items-center mb-3">
        <h2>Наша історія</h2>
    </div>
    @include('home_layout.history')
@endsection
