@extends('basic_layout')

@section('title', $schoolClass->name . ' Клас')

@section('content')
    @include('school_class_layout.selection_form', ['schoolClassesList' => $schoolClassesList, 'selectedId' => $schoolClass->id])
    <x-errors class="mb-2 fs-5" :errors="$errors"></x-errors>
    <div class="school-class-container row d-flex justify-content-lg-between justify-content-center">
        <div class="col-xl-3 col-lg-4 col-sm-6 col-8 head-teacher-card d-flex align-items-center flex-column
                    my-1 py-2 sm:max-w-md bg-white shadow-md overflow-hidden sm:rounded-lg border row">
            <h2 class="title text-center mb-2">Класний керівник</h2>
            @include('employee_management_layout.employee_card', ['employee' => $schoolClass->headTeacher])
            <h2 class="title text-center my-2">Додаткові дані</h2>
            <a href="{{ route('schedules.show', ['type' => 'classes', 'schedule' => $schoolClass->id]) }}"
               class="color-blue text-center">
                Посилання на сторінку розкладу
            </a>
        </div>
        <div class="students-container col-xl-9 col-lg-8 my-1 py-2 bg-white shadow-md overflow-auto sm:rounded-lg
                    border">
            <h2 class="title text-center mb-2">Список учнів</h2>
            <table class="table table-striped table-bordered text-center m-0 overflow-auto">
                <thead>
                <tr>
                    <th scope="col">№</th>
                    <th scope="col">Прізвище</th>
                    <th scope="col">Ім'я</th>
                    <th scope="col">По-батькові</th>
                </tr>
                </thead>
                <tbody>
                @foreach($schoolClass->students as $index => $student)
                    <tr class="align-middle">
                        <th scope="row">{{ $index + 1 }}</th>
                        <td>{{ $student->lastname }}</td>
                        <td>{{ $student->firstname }}</td>
                        <td>{{ $student->patronymic }}</td>
                        @can('view student')
                            <td>
                                <a href="{{ route('users.show', ['user' => $student->id]) }}"
                                   class="btn btn-success"><i class="bi bi-file-earmark-text"></i></a>
                            </td>
                        @endcan
                        @can('update student')
                            <td>
                                <a href="{{ route('users.edit', ['user' => $student->id, 'type' => 'student']) }}"
                                   class="btn btn-warning"><i class="bi bi-pencil-fill"></i></a>
                            </td>
                        @endcan
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row d-flex justify-center my-2">
        @can('update class')
            <a href="{{ route('school-classes.edit', ['school_class' => $schoolClass->id]) }}"
               class="col-xxl-2 col-lg-3 col-md-4 col-6 p-0">
                <x-button class="w-full justify-content-center">Змінити дані класу</x-button>
            </a>
        @endcan
        @can('delete class')
            <form class="col-xxl-2 col-lg-3 col-md-4 col-6 p-0"
                  method="POST" action="{{ route('school-classes.destroy', ['school_class' => $schoolClass->id]) }}">
                @csrf
                @method('DELETE')
                <x-button class="w-full justify-content-center"
                          onclick="deletionSubmit('Ви впевнені, що хочете видалити даний клас?', this.closest('form'))">
                    Видалити клас
                </x-button>
            </form>
        @endcan
    </div>
@endsection
