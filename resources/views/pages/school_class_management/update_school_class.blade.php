@extends('basic_layout')

@section('title', 'Зміна даних ' . $schoolClass->name . ' класу')

@section('content')
    <div class="d-flex flex-row justify-content-center align-items-center">
        <div class="form w-full px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg m-4">
            <x-session-status class="mb-4" :status="session('status')"></x-session-status>
            <x-errors class="mb-4" :errors="$errors"></x-errors>
            <form method="POST" action="{{ route('school-classes.update', ['school_class' => $schoolClass->id]) }}">
                @csrf
                @method('PUT')
                <div class="row text-center d-flex justify-content-center">
                    <div class="col-md-6 pb-2 d-flex flex-col justify-content-between align-items-center">
                        <div class="col-xxl-6 col-xl-8 col-lg-10">
                            <x-label for="headTeacher" :value="__('Класний керівник')"></x-label>
                            <x-select-with-search size="5" name="headTeacher" id="headTeacher" class="overflow-auto"
                                                  required>
                                @foreach($teachers as $teacher)
                                    <option class="text-wrap" value="{{ $teacher->id }}"
                                        @selected($teacher->id == old('headTeacher', $schoolClass->headTeacher->id))>
                                        {{ $teacher->full_name }}
                                    </option>
                                @endforeach
                            </x-select-with-search>
                        </div>
                        <div class="row d-flex justify-content-center">
                            <div class="col-xxl-5 col-xl-8 col-lg-10">
                                <x-label for="letter" :value="__('Код класу (А,Б,В,Г)')"></x-label>
                                <x-input id="letter" class="input-field mt-1 w-full" type="text"
                                         name="letter" :value="old('letter', $schoolClass->letter)" required maxLength="1"></x-input>
                            </div>
                            <div class="col-xxl-6 col-xl-8 col-lg-10">
                                <x-label for="studyYear" :value="__('Навчальний рік реєстрації')"></x-label>
                                <x-select name="studyYear" id="studyYear" class="overflow-auto mt-1 w-full fs-5"
                                          required>
                                    @foreach($studyYears as $studyYear)
                                        <option class="text-wrap" value="{{ $studyYear->id }}"
                                            @selected($studyYear->id == old('studyYear', $schoolClass->study_year_id))>
                                            {{ $studyYear->name }}
                                        </option>
                                    @endforeach
                                </x-select>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-10 mt-1 d-flex flex-row align-items-center justify-content-center">
                            @if(old('isAutoGrouping'))
                                <x-input id="isAutoGrouping" class="me-2" type="checkbox"
                                         name="isAutoGrouping" checked
                                         onchange="toggleClassAutoGrouping(this)"></x-input>
                            @else
                                <x-input id="isAutoGrouping" class="me-2" type="checkbox"
                                         name="isAutoGrouping"
                                         onchange="toggleClassAutoGrouping(this)"></x-input>
                            @endif
                            <x-label for="isAutoGrouping" :value="__('Зробити розподіл груп автоматично')"/>
                        </div>
                    </div>
                    <div class="col-md-6 pb-2 d-flex justify-content-center flex-col">
                        <x-label for="students" :value="__('Учні')"></x-label>
                        <input type="text" placeholder="Пошук.." onkeyup="classStudentSearch(this)"
                               class="w-full fs-5 rounded-top shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                        <div style="height: 50vh;"
                             class="overflow-auto table-responsive border fs-5 rounded-bottom shadow-sm border-gray-300">
                            <table class="table table-bordered text-center m-0">
                                <tbody>
                                @foreach($students as $student)
                                    @php($currentSchoolClassStudent = $schoolClass->students->where('id', $student->id)->first())
                                    <tr>
                                        <td class="align-middle p-1">
                                            <input type="number" name="students[{{ $student->id }}][id]" value="{{ $student->id }}"
                                                   class="student-id-input" hidden disabled/>
                                            <input type="checkbox" name="checkboxes[{{ $student->id }}]" onchange="toggleStudentCheckbox(this)"
                                                   @checked(old('students.' . $student->id . '.id') ||
                                                            (!old('students') && $currentSchoolClassStudent)
                                                   )
                                                   class="student-checkbox shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"/>
                                        </td>
                                        <td class="align-middle p-0">{{ $student->full_name }}</td>
                                        <td class="align-middle p-0">
                                            <x-select name="students[{{ $student->id }}][group]" class="select-group w-full fs-5">
                                                <option class="text-wrap" value="" selected hidden disabled>
                                                    Група
                                                </option>
                                                <option class="text-wrap" value="first_group"
                                                    @selected(old(
                                                                    'students.' . $student->id . '.group',
                                                                    $currentSchoolClassStudent ? $currentSchoolClassStudent->group : null
                                                                 ) == 'first_group')>
                                                    Перша група
                                                </option>
                                                <option class="text-wrap" value="second_group"
                                                    @selected(old(
                                                                    'students.' . $student->id . '.group',
                                                                    $currentSchoolClassStudent ? $currentSchoolClassStudent->group : null
                                                                 ) == 'second_group')>
                                                    Друга група
                                                </option>
                                            </x-select>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="flex items-center justify-center mt-3">
                    <x-submit-button class="sb-btn ml-3">Змінити</x-submit-button>
                </div>
            </form>
        </div>
    </div>
@endsection
