@extends('basic_layout')

@section('title', 'Класи')

@section('content')
    @if(isset($schoolClassesList) && !$schoolClassesList->isEmpty())
        @include('school_class_layout.selection_form', ['schoolClassesList' => $schoolClassesList, 'selectedId' => -1])
    @else
        <div class="row my-2">
            <p class="text-center fs-3 col-12">Дані класів ще не опубліковано.</p>
        </div>
    @endif
@endsection
