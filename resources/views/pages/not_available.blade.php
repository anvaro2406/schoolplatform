@extends('basic_layout')

@section('title', 'Функціонал недоступний')

@section('content')
    <div class="sorry-content container-fluid d-flex flex-row justify-content-center row m-0 p-0">
        <i class="col-lg-2 col-md-3 offset-lg-5 offset-md-4 sorry-emoji bi bi-emoji-frown text-center m-0"></i>
        <p class="col-md-5 d-flex flex-column justify-content-center text-center p-0">
            На жаль, даний функціонал поки що недоступний.
        </p>
    </div>
@endsection
