@extends('basic_layout')

@section('title', 'Класні журнали')

@section('content')
    @if(isset($schoolClasses) && !$schoolClasses->isEmpty() && isset($studyYears) && !$studyYears->isEmpty())
        @include('journal_layout.selection_form', [
                'schoolClasses' => $schoolClasses, 'selectedSchoolClassId' => -1,
                'studyYears' => $studyYears, 'selectedStudyYearId' => -1,
        ])
    @else
        <div class="row my-2">
            <p class="text-center fs-3 col-12">Потрібна інформація ще не оцифрована.</p>
        </div>
    @endif
@endsection
