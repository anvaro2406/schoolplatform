@extends('basic_layout')

@if(isset($subjects) && !$subjects->isEmpty())
    @section('title', 'Журнал ' . $subjects[0]->schoolClass->nameInStudyYear($selectedStudyYearId) . ' класу')
@else
    @section('title', 'Неоцифрований журнал')
@endif

@section('content')
    <x-errors class="mb-2 fs-5" :errors="$errors"></x-errors>
    @if(isset($schoolClasses) && !$schoolClasses->isEmpty()
        && isset($studyYears) && !$studyYears->isEmpty()
        && isset($subjects) && !$subjects->isEmpty())
        @include('journal_layout.selection_form', [
                'schoolClassesList' => $schoolClasses, 'selectedSchoolClassId' => $selectedSchoolClassId,
                'studyYearsList' => $studyYears, 'selectedStudyYearId' => $selectedStudyYearId,
        ])
        <div class="d-flex justify-content-center">
            <div class="journals-subjects-pages-list col-xxl-6 col-lg-8 px-6 py-4 bg-white shadow-md overflow-auto
                        sm:rounded-lg mt-2">
                <h2 class="text-center mb-3 fs-4">Список сторінок журналу з предметами, які викладаються</h2>
                <ul>
                    @foreach ($subjects as $subject)
                        <li>
                            <a href="{{ route('journal-subject-pages.show', ['journalSubjectPage' => $subject->id]) }}">
                                {{ $subject->abstractSubject->title }}
                                &nbsp;
                                @if($subject->group === 'first_group')
                                    (Перша група)
                                @elseif($subject->group === 'second_group')
                                    (Друга група)
                                @endif
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="row d-flex justify-center my-2">
            @can('update journal')
                <a href="{{ route('journals.edit', ['school_class' => $selectedSchoolClassId, 'study_year' => $selectedStudyYearId]) }}"
                   class="col-xxl-2 col-lg-3 col-md-4 col-6 p-0">
                    <x-button class="w-full justify-content-center">Змінити дані журналу</x-button>
                </a>
            @endcan
            @can('delete journal')
                <form class="col-xxl-2 col-lg-3 col-md-4 col-6 p-0" method="POST"
                      action="{{ route('journals.destroy', ['school_class' => $selectedSchoolClassId, 'study_year' => $selectedStudyYearId]) }}">
                    @csrf
                    @method('DELETE')
                    <x-button class="w-full justify-content-center"
                              onclick="deletionSubmit('Якщо для даного класу вже існує розклад занять, то він буде також видалений. Ви впевнені, що хочете видалити даний класний журнал?', this.closest('form'))">
                        Видалити журнал
                    </x-button>
                </form>
            @endcan
        </div>
    @elseif(isset($schoolClasses) && !$schoolClasses->isEmpty()
            && isset($studyYears) && !$studyYears->isEmpty())
        @include('journal_layout.selection_form', [
                'schoolClassesList' => $schoolClasses, 'selectedSchoolClassId' => $selectedSchoolClassId,
                'studyYearsList' => $studyYears, 'selectedStudyYearId' => $selectedStudyYearId,
        ])
        <div class="row my-2">
            <p class="text-center fs-3 col-12">Дані журналу вибраного класу ще не оцифровано.</p>
        </div>
    @else
        <div class="row my-2">
            <p class="text-center fs-3 col-12">Потрібна інформація ще не оцифрована.</p>
        </div>
    @endif
@endsection

