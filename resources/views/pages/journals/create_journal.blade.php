@extends('basic_layout')

@section('title', 'Створення журналу')

@section('content')
    <div class="d-flex flex-row justify-content-center align-items-center">
        <div class="form w-full px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg m-4">
            <x-session-status class="mb-4" :status="session('status')"></x-session-status>
            <x-errors class="mb-4" :errors="$errors"></x-errors>
            <form method="POST" action="{{ route('journals.store') }}">
                @csrf
                <div class="row text-center d-flex justify-content-center mb-2">
                    <div class="col-xxl-3 col-xl-4 col-lg-5 col-md-6 col-sm-8 mb-2">
                        <x-select name="studyYear" id="studyYear" class="overflow-auto w-full fs-5" required
                                  onchange="changeJournalStudyYearSelect(null, true)">
                            <option class="text-wrap" value="" selected disabled hidden>Навчальний рік реєстрації</option>
                            @foreach($studyYears as $studyYear)
                                <option class="text-wrap" value="{{ $studyYear->id }}"
                                    @selected($studyYear->id == old('studyYear'))>
                                    {{ $studyYear->name }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-4 mb-2">
                        <x-select name="schoolClass" id="schoolClass" class="input-field w-full overflow-auto" required
                                  onchange="changeJournalMainSelectors()">
                            <option class="text-wrap" value="" selected disabled hidden>Клас</option>
                            @foreach($schoolClasses as $schoolClass)
                                @if($schoolClass->id == old('schoolClass'))
                                    <option class="text-wrap" value="{{ $schoolClass->id }}" selected>
                                        {{ $schoolClass->name}}
                                    </option>
                                @else
                                    <option class="text-wrap" value="{{ $schoolClass->id }}">
                                        {{ $schoolClass->name}}
                                    </option>
                                @endif
                            @endforeach
                        </x-select>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-5 col-sm-8 mb-2">
                        <x-select name="schoolProgram" id="schoolProgram" class="input-field overflow-auto w-full"
                                  required onchange="changeJournalMainSelectors()">
                            <option class="text-wrap" value="" selected disabled hidden>Програма викладання</option>
                            @foreach($schoolPrograms as $schoolProgram)
                                @if($schoolProgram->id == old('schoolProgram'))
                                    <option class="text-wrap" value="{{ $schoolProgram->id }}" selected>
                                        {{ $schoolProgram->name}}
                                    </option>
                                @else
                                    <option class="text-wrap" value="{{ $schoolProgram->id }}">
                                        {{ $schoolProgram->name}}
                                    </option>
                                @endif
                            @endforeach
                        </x-select>
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <div class="col-xxl-8 col-xl-10 col-12 overflow-auto">
                        <x-label class="text-center mb-2">Список предметів</x-label>
                        <table class="table table-bordered text-center m-0 journal-subjects-table">
                            <thead>
                            <tr class="align-middle">
                                <th scope="col" rowspan="2" class="subject-column">Предмет</th>
                                <th scope="col" rowspan="2" class="is-group-column">Розділення на групи</th>
                                <th scope="col" colspan="2">Вчитель</th>
                            </tr>
                            <tr>
                                <th scope="col">1 група</th>
                                <th scope="col">2 група</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($abstractSubjects as $abstractSubject)
                                <tr class="align-middle abstract-subject-row">
                                    <td class="d-none school-program-cell">{{ $abstractSubject->school_program_id}}</td>
                                    <td class="d-none class-number-cell">{{ $abstractSubject->class_number}}</td>
                                    <td>{{ $abstractSubject->title}}</td>
                                    <td>
                                        @if(old('checkboxes.' . $loop->index) == null)
                                            <x-input type="checkbox" name="checkboxes[{{ $loop->index }}]"
                                                     onchange="changeJournalGroupCheckbox(this)"/>
                                        @else
                                            <x-input type="checkbox" name="checkboxes[{{ $loop->index }}]"
                                                     onchange="changeJournalGroupCheckbox(this)" checked/>
                                        @endif
                                    </td>
                                    <td colspan="2">
                                        <x-input name="subjects[{{ $loop->index }}][id]"
                                                 value="{{ $abstractSubject->id }}" hidden/>
                                        <x-input name="subjects[{{ $loop->index }}][group]" value="no_groups" hidden/>
                                        <x-select name="subjects[{{ $loop->index }}][teacherId]"
                                                  class="input-field overflow-auto w-full">
                                            @foreach($abstractSubject->teachers as $teacher)
                                                @if($teacher->id == old('subjects.' . $loop->parent->index . '.teacherId'))
                                                    <option class="text-wrap" value="{{ $teacher->id }}" selected>
                                                        {{ $teacher->full_name }}
                                                    </option>
                                                @else
                                                    <option class="text-wrap" value="{{ $teacher->id }}">
                                                        {{ $teacher->full_name }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </x-select>
                                    </td>
                                    <td class="d-none">
                                        <x-input name="subjects[{{ $loop->index }}][id]"
                                                 value="{{ $abstractSubject->id }}" hidden disabled/>
                                        <x-input name="subjects[{{ $loop->index }}][group]" value="first_group" hidden
                                                 disabled/>
                                        <x-select name="subjects[{{ $loop->index }}][teacherId]"
                                                  class="input-field overflow-auto w-full" disabled>
                                            @foreach($abstractSubject->teachers as $teacher)
                                                @if($teacher->id == old('subjects.' . $loop->parent->index . '.teacherId'))
                                                    <option class="text-wrap" value="{{ $teacher->id }}" selected>
                                                        {{ $teacher->full_name }}
                                                    </option>
                                                @else
                                                    <option class="text-wrap" value="{{ $teacher->id }}">
                                                        {{ $teacher->full_name }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </x-select>
                                    </td>
                                    <td class="d-none">
                                        <x-input name="subjects[{{ $loop->count + $loop->index }}][id]"
                                                 value="{{ $abstractSubject->id }}" hidden disabled/>
                                        <x-input name="subjects[{{ $loop->count + $loop->index }}][group]"
                                                 value="second_group" hidden disabled/>
                                        <x-select name="subjects[{{ $loop->count + $loop->index }}][teacherId]"
                                                  class="input-field overflow-auto w-full" disabled>
                                            @foreach($abstractSubject->teachers as $teacher)
                                                @if($teacher->id == old('subjects.' . $loop->parent->count + $loop->parent->index . '.teacherId'))
                                                    <option class="text-wrap" value="{{ $teacher->id }}" selected>
                                                        {{ $teacher->full_name }}
                                                    </option>
                                                @else
                                                    <option class="text-wrap" value="{{ $teacher->id }}">
                                                        {{ $teacher->full_name }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </x-select>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <x-label class="mb-2">
                            <i>
                                *Для відображення доступного списку предметів спочатку потрібно вибрати клас і програму
                            </i>
                        </x-label>
                    </div>
                </div>
                <div class="flex items-center justify-center mt-3">
                    <x-submit-button class="sb-btn ml-3">Створити</x-submit-button>
                </div>
            </form>
        </div>
    </div>
@endsection
