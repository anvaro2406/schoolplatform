@extends('basic_layout')

@if($subject->group === 'first_group')
    @section('title', 'Журнал ' . $schoolClass->nameInStudyYear($subject->study_year_id) . ' класу. ' . $abstractSubject->title . ' (Перша група)')
@elseif($subject->group === 'second_group')
    @section('title', 'Журнал ' . $schoolClass->nameInStudyYear($subject->study_year_id) . ' класу. ' . $abstractSubject->title . ' (Друга група)')
@else
    @section('title', 'Журнал ' . $schoolClass->nameInStudyYear($subject->study_year_id) . ' класу. ' . $abstractSubject->title)
@endif

@section('content')
    @csrf
    @include('journal_layout.feedback_form')
    <div class="d-flex flex-col align-items-center">
        @if($subject->group === 'first_group')
            <h2 class="fs-1 text-center my-2">{{ $abstractSubject->title . ' (Перша група)' }}</h2>
        @elseif($subject->group === 'second_group')
            <h2 class="fs-1 text-center my-2">{{ $abstractSubject->title . ' (Друга група)' }}</h2>
        @else
            <h2 class="fs-1 text-center my-2">{{ $abstractSubject->title }}</h2>
        @endif
        <div class="w-full fs-2 text-start mt-2 journal-subject-tab-title"
             onclick="clickJournalSubjectTabTitle(this, 'journal-subject-marks-attendance-tab')">
            <hr class="color-black opacity-100">
            <i class="bi bi-chevron-down"></i>&nbsp;Оцінки та відвідування
        </div>
        <div class="d-none w-full bg-white shadow-md overflow-auto sm:rounded-lg mt-2 border p-2"
             id="journal-subject-marks-attendance-tab">
            <table class="table table-bordered border-dark text-center m-0 fs-5 fixed-layout-table">
                <thead>
                <tr class="align-middle">
                    <th scope="col" class="journal-sequence-number-cell">№<br>з/п</th>
                    <th scope="col" class="diagonal-cell">
                        <span class="text-end">Дата уроку</span>
                        <span class="text-start">Прізвище та ім'я здобувачів освіти</span>
                    </th>
                    @foreach($lessons as $lesson)
                        <td class="journal-lesson-cell td-with-input">
                            <input type="date" class="dateInput{{ $lesson->id }}" name="date"
                                   value="{{ $lesson->date != null ? $lesson->date->format('Y-m-d') : null }}"
                                   onchange="changeLessonData('{{ $lesson->id }}', this)"/>
                        </td>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($students as $student)
                    <tr class="align-middle journal-subject-marks-attendance-tr">
                        <th>
                            {{ $loop->iteration }}
                        </th>
                        <td class="text-truncate open-feedback-form-btn" data-student-id="{{ $student->id }}"
                            onclick="feedbackFormModalWindowOpen('{{ $student->id }}', '{{ htmlspecialchars($student->lastname . ' ' .  $student->firstname) }}')">
                            {{ $student->lastname . ' ' .  $student->firstname}}
                        </td>
                        @foreach($student->lessons->where('subject_id', $subject->id)->pluck('journalCell') as $journalCell)
                            <td class="journal-subject-mark-attendance-cell td-with-input">
                                @if(!$journalCell->is_absent)
                                    <input type="text" value="н" name="journalCell" onclick="this.select()"
                                           onchange="changeJournalCellData(
                                               '{{ $journalCell->lesson_id }}',
                                               '{{ $journalCell->student_id }}',
                                               this
                                           )"/>
                                @else
                                    <input type="text" value="{{ $journalCell->mark }}" name="journalCell"
                                           onchange="changeJournalCellData(
                                               '{{ $journalCell->lesson_id }}',
                                               '{{ $journalCell->student_id }}',
                                               this
                                           )" onclick="this.select()"/>
                                @endif
                            </td>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="w-full fs-2 text-start mt-2 journal-subject-tab-title"
             onclick="clickJournalSubjectTabTitle(this, 'journal-subject-themes-homework-tab')">
            <hr class="color-black opacity-100">
            <i class="bi bi-chevron-down"></i>&nbsp;Теми уроків та завдання додому
        </div>
        <div class="d-none w-full bg-white shadow-md overflow-auto sm:rounded-lg mt-2 border p-2"
             id="journal-subject-themes-homework-tab">
            <table class="table table-bordered border-dark text-center m-0 fs-5">
                <thead>
                <tr class="align-middle">
                    <th scope="col" class="journal-sequence-number-cell">№<br>з/п</th>
                    <th scope="col">Дата</th>
                    <th scope="col">Тема</th>
                    <th scope="col">Завдання додому</th>
                </tr>
                </thead>
                <tbody>
                @foreach($lessons as $lesson)
                    <tr class="align-middle journal-subject-theme-homework-tr">
                        <th>{{ $loop->iteration }}</th>
                        <td class="td-with-input">
                            <input type="date"
                                   value="{{ $lesson->date != null ? $lesson->date->format('Y-m-d') : null }}"
                                   name="date" class="fs-4 dateInput{{ $lesson->id }}"
                                   onchange="changeLessonData('{{ $lesson->id }}', this)"/>
                        </td>
                        <td class="td-with-input lesson-theme-td">
                            <textarea name="theme"
                                      onchange="changeLessonData('{{ $lesson->id }}', this)">{{ $lesson->theme }}</textarea>
                        </td>
                        <td class="td-with-input lesson-homework-td">
                            <textarea name="homework"
                                      onchange="changeLessonData('{{ $lesson->id }}', this)">{{ $lesson->homework }}</textarea>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="w-full fs-2 text-start mt-2 journal-subject-tab-title"
             onclick="clickJournalSubjectTabTitle(this, 'journal-subject-additional-tab')">
            <hr class="color-black opacity-100">
            <i class="bi bi-chevron-down"></i>&nbsp;Додаткова інформація
        </div>
        <div class="d-none w-full my-2 d-flex justify-content-center row" id="journal-subject-additional-tab">
            <div class="col-xl-3 col-lg-4 col-sm-6 col-10 head-teacher-card d-flex align-items-center flex-column mb-2">
                <h2 class="title text-center mb-2">Вчитель</h2>
                @include('employee_management_layout.employee_card', ['employee' => $teacher])
            </div>
            <div class="col-xl-4 col-md-6 col-sm-8">
                <h2 class="title text-center mb-2">Додаткові дані</h2>
                <div class="py-2 sm:max-w-md bg-white shadow-md overflow-hidden sm:rounded-lg border mb-2 text-center fs-4
                            d-flex flex-column">
                    <a href="{{ route('subjects.show', ['subject' => $abstractSubject->id]) }}" class="color-blue">
                        Посилання на сторінку предмета
                    </a>
                    <a href="{{ route('school-classes.show', ['school_class' => $schoolClass->id]) }}"
                       class="color-blue">
                        Посилання на сторінку класу
                    </a>
                    @if(isset($subject->video_conference_link))
                        <a href="{{ $subject->video_conference_link }}" class="color-blue">
                            Посилання на відеоконференцію
                        </a>
                    @else
                        <p>Посилання на відеоконференцію відсутнє</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
