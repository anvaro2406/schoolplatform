@extends('basic_layout')

@section('title', 'Мої журнали')

@section('content')
    @if(isset($subjects) && !$subjects->isEmpty())
        <div class="d-flex justify-content-center">
            <div class="col-xxl-6 col-lg-8 px-6 py-4 bg-white shadow-md overflow-auto sm:rounded-lg mt-2">
                <h2 class="text-center mb-3 fs-4">Список сторінок журналів з предметами, які я викладаю</h2>
                <table class="table table-striped table-bordered text-center m-0 fs-5">
                    <thead>
                    <tr class="align-middle">
                        <th scope="col">Клас</th>
                        <th scope="col">Предмет</th>
                        <th scope="col">Група</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($subjects as $subject)
                        <tr class="align-middle">
                            <td>
                                <a href="{{ route('journal-subject-pages.show', ['journalSubjectPage' => $subject->id]) }}">
                                    {{ $subject->schoolClass->name }}
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('journal-subject-pages.show', ['journalSubjectPage' => $subject->id]) }}">
                                    {{ $subject->abstractSubject->title }}
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('journal-subject-pages.show', ['journalSubjectPage' => $subject->id]) }}">
                                    @if($subject->group === 'first_group')
                                        Перша група
                                    @elseif($subject->group === 'second_group')
                                        Друга група
                                    @else
                                        Весь клас
                                    @endif
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @else
        <div class="row my-2">
            <p class="text-center fs-3 col-12">Дані журналів ще не оцифровано.</p>
        </div>
    @endif
@endsection
