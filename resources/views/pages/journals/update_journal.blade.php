@extends('basic_layout')

@section('title', 'Зміна даних журналу')

@section('content')
    <div class="d-flex flex-row justify-content-center align-items-center">
        <div class="form w-full px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg m-4">
            <x-session-status class="mb-4" :status="session('status')"></x-session-status>
            <x-errors class="mb-4" :errors="$errors"></x-errors>
            <form method="POST" action="{{ route('journals.update', ['school_class' => $schoolClass->id, 'study_year' => $studyYear->id]) }}">
                @csrf
                @method('PUT')
                <div class="row text-center d-flex justify-content-center mb-2">
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-5 mb-2">
                        <x-select class="overflow-auto w-full fs-5">
                            <option class="text-wrap" selected disabled>{{ $studyYear->name }}</option>
                        </x-select>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-4 mb-2">
                        <x-select class="input-field overflow-auto w-full" id="schoolClass">
                            <option class="text-wrap" selected disabled>
                                {{ $schoolClass->nameInStudyYear($studyYear->id) }}
                            </option>
                        </x-select>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-5 col-sm-6 mb-2">
                        <x-select name="schoolProgram" id="schoolProgram" class="input-field overflow-auto w-full"
                                  required onchange="changeJournalMainSelectors()">
                            @foreach($schoolPrograms as $schoolProgram)
                                @if($schoolProgram->id == old('schoolProgram', $currentSchoolProgram->id))
                                    <option class="text-wrap" value="{{ $schoolProgram->id }}" selected>
                                        {{ $schoolProgram->name}}
                                    </option>
                                @else
                                    <option class="text-wrap" value="{{ $schoolProgram->id }}">
                                        {{ $schoolProgram->name}}
                                    </option>
                                @endif
                            @endforeach
                        </x-select>
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <div class="col-xxl-8 col-xl-10 col-12 overflow-auto">
                        <x-label class="text-center mb-2">Список предметів</x-label>
                        <table class="table table-bordered text-center m-0 journal-subjects-table">
                            <thead>
                            <tr class="align-middle">
                                <th scope="col" rowspan="2" class="subject-column">Предмет</th>
                                <th scope="col" rowspan="2" class="is-group-column">Розділення на групи</th>
                                <th scope="col" colspan="2">Вчитель</th>
                            </tr>
                            <tr>
                                <th scope="col">1 група</th>
                                <th scope="col">2 група</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($abstractSubjects as $index => $abstractSubject)
                                @php(
                                    $subjects = $abstractSubject->subjects->where('class_id', $schoolClass->id)
                                    ->where('study_year_id', $studyYear->id)
                                )
                                <tr class="align-middle abstract-subject-row">
                                    <td class="d-none school-program-cell">{{ $abstractSubject->school_program_id}}</td>
                                    <td class="d-none class-number-cell">{{ $abstractSubject->class_number}}</td>
                                    <td>{{ $abstractSubject->title}}</td>
                                    <td>
                                        @if($subjects->isEmpty()
                                            || ($subjects->first()->group == 'no_groups'
                                            && old("checkboxes.$index") == null))
                                            <x-input type="checkbox" name="checkboxes[{{ $index }}]"
                                                     onchange="changeJournalGroupCheckbox(this)"/>
                                        @else
                                            <x-input type="checkbox" name="checkboxes[{{ $index }}]"
                                                     onchange="changeJournalGroupCheckbox(this)" checked/>
                                        @endif
                                    </td>
                                    <td colspan="2">
                                        <x-input name="subjects[{{ $index }}][id]"
                                                 value="{{ $abstractSubject->id }}" hidden/>
                                        <x-input name="subjects[{{ $index }}][group]" value="no_groups" hidden/>
                                        <x-select name="subjects[{{ $index }}][teacherId]"
                                                  class="input-field overflow-auto w-full">
                                            @foreach($abstractSubject->teachers as $teacher)
                                                @if($teacher->id == old('subjects.' . $index . '.teacherId')
                                                    || (!$subjects->isEmpty()
                                                    && $teacher->id == $subjects->first()->teacher->id))
                                                    <option class="text-wrap" value="{{ $teacher->id }}" selected>
                                                        {{ "$teacher->lastname $teacher->firstname $teacher->patronymic" }}
                                                    </option>
                                                @else
                                                    <option class="text-wrap" value="{{ $teacher->id }}">
                                                        {{ "$teacher->lastname $teacher->firstname $teacher->patronymic" }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </x-select>
                                    </td>
                                    <td class="d-none">
                                        <x-input name="subjects[{{ $index }}][id]"
                                                 value="{{ $abstractSubject->id }}" hidden disabled/>
                                        <x-input name="subjects[{{ $index }}][group]" value="first_group" hidden
                                                 disabled/>
                                        <x-select name="subjects[{{ $index }}][teacherId]"
                                                  class="input-field overflow-auto w-full" disabled>
                                            @foreach($abstractSubject->teachers as $teacher)
                                                @if($teacher->id == old('subjects.' . $index . '.teacherId')
                                                    || (!$subjects->isEmpty()
                                                    && $teacher->id == $subjects->first()->teacher->id))
                                                    <option class="text-wrap" value="{{ $teacher->id }}" selected>
                                                        {{ "$teacher->lastname $teacher->firstname $teacher->patronymic" }}
                                                    </option>
                                                @else
                                                    <option class="text-wrap" value="{{ $teacher->id }}">
                                                        {{ "$teacher->lastname $teacher->firstname $teacher->patronymic" }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </x-select>
                                    </td>
                                    <td class="d-none">
                                        <x-input name="subjects[{{ $loop->count + $index }}][id]"
                                                 value="{{ $abstractSubject->id }}" hidden disabled/>
                                        <x-input name="subjects[{{ $loop->count + $index }}][group]"
                                                 value="second_group" hidden disabled/>
                                        <x-select name="subjects[{{ $loop->count + $index }}][teacherId]"
                                                  class="input-field overflow-auto w-full" disabled>
                                            @foreach($abstractSubject->teachers as $teacher)
                                                @if($teacher->id == old('subjects.' . $loop->parent->count + $index . '.teacherId')
                                                    || ($subjects->count() == 2
                                                    && $teacher->id == $subjects->last()->teacher->id))
                                                    <option class="text-wrap" value="{{ $teacher->id }}" selected>
                                                        {{ "$teacher->lastname $teacher->firstname $teacher->patronymic" }}
                                                    </option>
                                                @else
                                                    <option class="text-wrap" value="{{ $teacher->id }}">
                                                        {{ "$teacher->lastname $teacher->firstname $teacher->patronymic" }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </x-select>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="flex items-center justify-center mt-3">
                    <x-button class="sb-btn ml-3"
                                     onclick="formSubmitWithConfirmation('Якщо для даного класу вже існує розклад занять, то він буде видалений. Ви впевнені, що хочете змінити даний класний журнал?', this.closest('form'))">
                        Змінити
                    </x-button>
                </div>
            </form>
        </div>
    </div>
@endsection
