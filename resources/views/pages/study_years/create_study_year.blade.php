@extends('basic_layout')

@section('title', 'Створення навчального року')

@section('content')
    <div class="d-flex flex-row justify-content-center align-items-center">
        <div class="form w-full px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg m-4">
            <x-session-status class="mb-4" :status="session('status')"></x-session-status>
            <x-errors class="mb-4" :errors="$errors"></x-errors>
            <form method="POST" action="{{ route('study-years.store') }}"
                  class="d-flex flex-row justify-center align-items-center row m-2">
                @csrf
                <x-label for="end" :value="__('Вкажіть дату завершення навчального року:')"
                         class="col-xxl-4 col-xl-5 col-lg-6 col-md-8 text-center mb-2"></x-label>
                <x-input name="end" id="end" class="overflow-auto col-xl-2 col-md-3 col-sm-5 col-8 fs-5 mb-2" type="date"
                         required :value="old('end')"></x-input>
                <div class="flex items-center justify-center align-items-center col-xl-2 col-md-3 col-sm-5 col-8 mb-2">
                    <x-submit-button class="sb-btn ml-3">Створити</x-submit-button>
                </div>
            </form>
        </div>
    </div>
@endsection
