@extends('basic_layout')

@section('title', 'Працівники школи')

@section('content')
    <x-errors class="mb-4" :errors="$errors"></x-errors>
    @if(isset($employees) && !$employees->isEmpty())
        <div class="row d-flex justify-content-center">
            @foreach ($employees as $employee)
                @include('employee_management_layout.employee_card', ['employee' => $employee])
            @endforeach
            {{ $employees->onEachSide(2)->links() }}
        </div>
    @else
        <div class="row my-2">
            <p class="text-center fs-3 col-12">Дані шкільних працівників поки що не опубліковані.</p>
        </div>
    @endif
@endsection
