@extends('basic_layout')

@section('title', 'Вхід в систему')

@section('content')
    <div id="login-content" class="d-flex flex-row justify-content-center align-items-center row">
        <div class="form w-full sm:max-w-md px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg m-4
                    col-xxl-7 col-xl-7 col-lg-7">
            <x-session-status class="mb-4" :status="session('status')"></x-session-status>
            <x-errors class="mb-4" :errors="$errors"></x-errors>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div>
                    <x-label for="email" :value="__('Електронна пошта')"></x-label>
                    <x-input id="email" class="input-field block mt-1 w-full" type="email" name="email" :value="old('email')"
                             required autofocus></x-input>
                </div>
                <div class="mt-4">
                    <x-label for="password" :value="__('Пароль')"></x-label>
                    <x-input id="password" class="input-field block mt-1 w-full"
                             type="password"
                             name="password"
                             required autocomplete="current-password"></x-input>
                </div>
                <div class="block mt-4">
                    <label for="remember_me" class="inline-flex items-center">
                        <input id="remember_me" type="checkbox"
                               class="input-field rounded border-gray-300 text-indigo-600 shadow-sm
                                    focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                               name="remember">
                        <span class="ml-2 text-md text-gray-600">Запам'ятати мене</span>
                    </label>
                </div>
                <div class="flex items-center justify-end mt-4">
                    @if (Route::has('password.request'))
                        <a class="underline text-md text-gray-600 hover:text-gray-900"
                           href="{{ route('password.request') }}">
                            Забули пароль?
                        </a>
                    @endif
                    <x-submit-button class="sb-btn ml-3">Ввійти</x-submit-button>
                </div>
            </form>
        </div>
        <div class="user-instruction col-xxl-5 col-xl-5 col-lg-5 ml-4 mb-4">
            <h2>Ви вперше на нашому сайті?</h2>
            <div class="border-1 p-1 mt-2">
                <p>
                    Для входу в систему вам потрібно використати електронну поштову скриню, видану при реєстрації в школі.
                    Пароль до системи буде знаходитися в першому повідомленні, збереженому у цій поштовій скрині.
                </p>
                <p>
                    Якщо ви не маєте, виданої в школі, електронної поштової скрині, то зверніться до адміністратора.
                </p>
            </div>
        </div>
    </div>
@endsection
