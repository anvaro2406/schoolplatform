@extends('basic_layout')

@section('title', 'Підтвердження паролю')

@section('content')
    <div id="confirm-password-content" class="d-flex flex-row justify-content-center align-items-center">
        <div class="form w-full sm:max-w-md px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg m-4">
            <div class="mb-4 text-md text-gray-600">
                Будь ласка підтвердіть свій пароль, перед тим як продовжити.
            </div>
            <x-errors class="mb-4" :errors="$errors"></x-errors>
            <form method="POST" action="{{ route('password.confirm') }}">
                @csrf
                <div>
                    <x-label for="password" :value="__('Пароль')"></x-label>
                    <x-input id="password" class="input-field block mt-1 w-full"
                             type="password"
                             name="password"
                             required autocomplete="current-password"></x-input>
                </div>
                <div class="flex justify-center mt-4">
                    <x-submit-button class="sb-btn">
                        Підтвердити
                    </x-submit-button>
                </div>
            </form>
        </div>
    </div>
@endsection
