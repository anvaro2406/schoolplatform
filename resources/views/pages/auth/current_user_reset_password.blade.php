@extends('basic_layout')

@section('title', 'Зміна пароля')

@section('content')
    <div id="current-user-reset-password-content" class="d-flex flex-row justify-content-center align-items-center">
        <p class="fs-2 text-success text-center">
            На вашу електронну пошту надіслано повідомлення з посиланням на сторінку зміни пароля.
        </p>
    </div>
@endsection
