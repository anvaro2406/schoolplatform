@extends('basic_layout')

@section('title', 'Зміна пароля')

@section('content')
    <div id="forgot-password-content" class="d-flex flex-row justify-content-center align-items-center">
        <div class="form w-full sm:max-w-md px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg m-4">
            <div class="mb-4 text-md text-gray-600">
                Забули пароль? Немає проблем. Просто введіть свою електронну пошту і ви отримаєте повідомлення з
                посиланням на сторінку зміни пароля. Там ви зможете вказати новий пароль.
            </div>
            <x-session-status class="mb-4" :status="session('status')"></x-session-status>
            <x-errors class="mb-4" :errors="$errors"></x-errors>
            <form method="POST" action="{{ route('password.email') }}">
                @csrf
                <div>
                    <x-label for="email" :value="__('Електронна пошта')"></x-label>
                    <x-input id="email" class="input-field block mt-1 w-full" type="email" name="email" :value="old('email')"
                             required autofocus></x-input>
                </div>
                <div class="flex items-center justify-end mt-4">
                    <x-submit-button class="sb-btn">
                        Надіслати повідомлення
                    </x-submit-button>
                </div>
            </form>
        </div>
    </div>
@endsection
