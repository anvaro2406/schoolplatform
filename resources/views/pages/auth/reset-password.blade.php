@extends('basic_layout')

@section('title', 'Зміна пароля')

@section('content')
    <div id="reset-password-content" class="d-flex flex-row justify-content-center align-items-center">
        <div class="form w-full sm:max-w-md px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg m-4">
            <x-errors class="mb-4" :errors="$errors"></x-errors>
            <form method="POST" action="{{ route('password.update') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $request->route('token') }}">
                <div>
                    <x-label for="email" :value="__('Електронна пошта')"></x-label>
                    <x-input id="email" class="input-field block mt-1 w-full" type="email" name="email"
                             :value="old('email', $request->email)" required autofocus></x-input>
                </div>
                <div class="mt-4">
                    <x-label for="password" :value="__('Пароль')"></x-label>
                    <x-input id="password" class="input-field block mt-1 w-full" type="password"
                             name="password" required></x-input>
                </div>
                <div class="mt-4">
                    <x-label for="password_confirmation" :value="__('Підтвердіть пароль')"></x-label>
                    <x-input id="password_confirmation" class="input-field block mt-1 w-full"
                             type="password"
                             name="password_confirmation" required></x-input>
                </div>
                <div class="flex items-center justify-center mt-4">
                    <x-submit-button class="sb-btn">
                        Змінити пароль
                    </x-submit-button>
                </div>
            </form>
        </div>
    </div>
@endsection
