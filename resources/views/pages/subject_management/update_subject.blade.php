@extends('basic_layout')

@section('title', 'Зміна шкільного предмету. ' . $subject->title)

@section('content')
    <div id="update-news-content" class="d-flex flex-row justify-content-center align-items-center">
        <div class="form w-full px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg m-4">
            <x-session-status class="mb-4" :status="session('status')"></x-session-status>
            <x-errors class="mb-4" :errors="$errors"></x-errors>
            <form method="POST" action="{{ route('subjects.update', ['subject' => $subject->id]) }}">
                @csrf
                @method('PUT')
                <div class="row text-center d-flex justify-content-center">
                    <div class="col-xl-3 col-lg-4 col-md-6 pb-2">
                        <x-label for="teacher" :value="__('Вчителі')"></x-label>
                        <x-select size="4" multiple name="teachers[]" id="teacher" class="input-field overflow-auto"
                                  required>
                            @foreach($teachers as $teacher)
                                @if(in_array($teacher->id, old('teachers', $subject->teachers()->pluck('teacher_id')->toArray())))
                                    <option class="text-wrap" value="{{ $teacher->id }}" selected>
                                        {{ $teacher->lastname . ' ' . $teacher->firstname . ' ' . $teacher->patronymic}}
                                    </option>
                                @else
                                    <option class="text-wrap" value="{{ $teacher->id }}">
                                        {{ $teacher->lastname . ' ' . $teacher->firstname . ' ' . $teacher->patronymic}}
                                    </option>
                                @endif
                            @endforeach
                        </x-select>
                    </div>
                    <div class="col-xl-9 col-lg-8 row d-flex justify-content-center">
                        <div class="col-md-6 pb-2">
                            <x-label for="schoolPrograms" :value="__('Шкільна програма')"></x-label>
                            <x-input list="schoolPrograms" name="schoolProgram" id="schoolProgram" type="text" required
                                     max="255" class="input-field mt-1 w-full"
                                     :value="old('schoolProgram', $subject->schoolProgram->name)"></x-input>
                            <datalist id="schoolPrograms">
                                @foreach($schoolPrograms as $schoolProgram)
                                    <option value="{{ $schoolProgram->name }}">
                                @endforeach
                            </datalist>
                        </div>
                        <div class="col-md-6 pb-2">
                            <x-label for="title" :value="__('Назва')"></x-label>
                            <x-input id="title" class="input-field mt-1 w-full" type="text" name="title"
                                     :value="old('title', $subject->title)" required max="255"></x-input>
                        </div>
                        <div class="col-xl-5 col-md-6 pb-2">
                            <x-label for="yearStudyHours" :value="__('Кількість годин в рік')"></x-label>
                            <x-input id="yearStudyHours" class="input-field mt-1 w-full" type="number"
                                     name="yearStudyHours" required min="8.75" max="525" step="0.25"
                                     :value="old('yearStudyHours', $subject->year_studyhours)"></x-input>
                        </div>
                        <div class="col-xl-5 col-md-6 pb-2">
                            <x-label for="weekStudyHours" :value="__('Кількість годин в тиждень')"></x-label>
                            <x-input id="weekStudyHours" class="input-field mt-1 w-full" type="number"
                                     name="weekStudyHours" required min="0.25" max="15" step="0.25"
                                     :value="old('weekStudyHours', $subject->week_studyhours)"></x-input>
                        </div>
                        <div class="col-xl-2 col-md-4 pb-2">
                            <x-label for="class" :value="__('Клас')"></x-label>
                            <x-input id="class" class="input-field mt-1 w-full" type="number" name="classNum"
                                     :value="old('classNum', $subject->class_number)" required min="1" max="11"></x-input>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <x-label for="wyziwyg-editor" class="pb-2">Опис предмету:</x-label>
                    <textarea id="wyziwyg-editor" name="description">
                        {{ old('description', $subject->description) }}
                    </textarea>
                    <input id="wyziwyg-image" type="file" name="wyziwyg-image" class="d-none" onchange=""/>
                </div>
                <div class="flex items-center justify-center mt-3">
                    <x-submit-button class="sb-btn ml-3">Змінити</x-submit-button>
                </div>
            </form>
        </div>
    </div>
@endsection
