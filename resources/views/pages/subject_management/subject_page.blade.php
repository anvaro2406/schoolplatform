@extends('basic_layout')

@section('title', $subject->title)

@section('content')
    <x-errors class="mb-2 fs-5" :errors="$errors"></x-errors>
    <b class="row w-full fs-2 d-flex justify-content-center text-center">{{ $subject->title }}</b>
    <div class="row d-flex justify-content-center" id="subject-hours">
        <span class="col-lg-4 col-md-6 text-center">
            <b>Кількість годин в рік:</b>&nbsp;{{+$subject->year_studyhours}}
        </span>
        <span class="col-lg-4 col-md-6 text-center">
            <b>Кількість годин в тиждень:</b>&nbsp;{{+$subject->week_studyhours}}
        </span>
        <span class="col-lg-4 col-md-6 text-center">
            <b>Шкільна програма:</b>&nbsp;{{$subject->schoolProgram->name}}
        </span>
    </div>
    <div>
        {!! $subject->description !!}
    </div>
    <b class="row w-full fs-3 d-flex justify-content-center text-center">Вчителі, які викладають предмет</b>
    <div class="d-flex justify-content-center row mb-2">
        @foreach($subject->teachers as $teacher)
            @include('employee_management_layout.employee_card', ['employee' => $teacher])
        @endforeach
    </div>
    <div class="row d-flex justify-content-center my-2">
        @can('update subject')
            <a href="{{ route('subjects.edit', ['subject' => $subject->id]) }}"
               class="col-xxl-3 col-lg-4 col-md-5 col-6 p-0">
                <x-button class="w-full justify-content-center">Змінити дані предмету</x-button>
            </a>
        @endcan
        @can('delete subject')
            <form class="col-xxl-2 col-lg-3 col-md-4 col-6 text-end p-0"
                  method="POST" action="{{ route('subjects.destroy', ['subject' => $subject->id]) }}">
                @csrf
                @method('DELETE')
                <x-button
                    onclick="deletionSubmit('Ви впевнені, що хочете видалити даний предмет?', this.closest('form'))"
                    class="w-full justify-content-center">Видалити предмет</x-button>
            </form>
        @endcan
    </div>
@endsection
