@extends('basic_layout')

@section('title', 'Шкільні предмети')

@section('content')
    @if(isset($groupedSubjects) && !$groupedSubjects->isEmpty() && $сlassNum)
        @include('subject_management_layout.selection_form', ['selectedClassNum' => $сlassNum])
        <div class="row d-flex justify-content-center">
            @foreach ($groupedSubjects as $subjects)
                <div class="subjects-list col-xxl-3 col-lg-4 col-sm-5 sm:max-w-md px-6 py-4 bg-white shadow-md
                            overflow-hidden sm:rounded-lg">
                    <h2>{{$subjects[0]->schoolProgram->name}}</h2>
                    <ul>
                        @foreach ($subjects as $subject)
                            <li>
                                <a href="{{route('subjects.show', ['subject' => $subject->id])}}">{{$subject->title}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endforeach
        </div>
    @elseif($сlassNum)
        @include('subject_management_layout.selection_form', ['selectedClassNum' => $сlassNum])
        <div class="row my-2">
            <p class="text-center fs-3 col-12">Список шкільних предметів даного класу ще не опубліковано.</p>
        </div>
    @else
        @include('subject_management_layout.selection_form', ['selectedClassNum' => 0])
    @endif
@endsection
