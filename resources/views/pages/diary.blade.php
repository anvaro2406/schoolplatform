@extends('basic_layout')

@if(isset($student))
    @section('title', 'Щоденник учня: ' . $student->full_name)
@else
    @section('title', 'Щоденник')
@endif

@section('content')
    <x-errors class="mb-2 fs-5 text-center" :errors="$errors"></x-errors>
    @if($canFormDiary)
        @include('diary_layout.week_selection', [
            'selectedWeekNumber' => $selectedWeek['orderNumber'], 'weeks' => $weeks, 'studentId' => $student->id
        ])
        <div class="d-flex flex-col align-items-center">
            <div class="w-full bg-white shadow-md sm:rounded-lg my-2 border p-2 row d-flex justify-content-center">
                <h2 class="fs-2 text-center my-2">Заняття</h2>
                @foreach($schedule as $day => $timeSlots)
                    @if(array_key_exists($day, $selectedWeek))
                        <div class="px-1 d-flex flex-column overflow-auto">
                            <h2 class="fs-3 text-center my-2">
                                {{ __('general.' . $day) }} {{ $selectedWeek[$day]->format('d.m.y') }}
                            </h2>
                            <table class="table table-bordered border-dark text-center m-0 fs-5 flex-grow-1">
                                <thead>
                                <tr class="align-middle">
                                    <th scope="col" class="time-slot-column">Часове вікно</th>
                                    <th scope="col" class="diary-subject-column">Предмет</th>
                                    <th scope="col" class="diary-theme-column">Тема уроку</th>
                                    <th scope="col" class="diary-homework-column">Завдання додому</th>
                                    <th scope="col" class="mark-attendance-column">Оцінка або присутність</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($multipleSimilarSubjectsCounter = [])
                                @foreach($timeSlots as $timeSlot)
                                    @php($subject = $timeSlot['subjects']->first())
                                    <tr class="align-middle">
                                        <td>{{ $timeSlot['timeSlot']->rangeName }}</td>
                                        <td>
                                            <a href="{{ route('subjects.show', ['subject' => $subject->abstractSubject->id]) }}">
                                                {{ $subject->abstractSubject->title }}
                                            </a>
                                        </td>
                                        @php($lessonCounter = array_count_values($multipleSimilarSubjectsCounter)[$day . $subject->id] ?? 0)
                                        @if($weekLessons->has($day) && $weekLessons[$day]->has($subject->id)
                                            && $weekLessons[$day][$subject->id]->count() > $lessonCounter)
                                            @php($currentLesson = $weekLessons[$day][$subject->id][$lessonCounter])
                                            @php($multipleSimilarSubjectsCounter[] = $day . $subject->id)
                                            <td>{{ $currentLesson->theme }}</td>
                                            <td>{{ $currentLesson->homework }}</td>
                                            @if($currentLesson->journalCell->mark != null)
                                                <td>{{ $currentLesson->journalCell->mark }}</td>
                                            @elseif(!$currentLesson->journalCell->is_absent)
                                                <td>н</td>
                                            @else
                                                <td></td>
                                            @endif
                                        @else
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                @endforeach
                <h2 class="fs-2 text-center my-2">Зауваження від вчителів</h2>
                @foreach($weekFeedbacks as $day => $feedbacks)
                    <div class="px-1 my-2 d-flex flex-column overflow-auto">
                        @if(array_key_exists($day, $selectedWeek))
                            <h2 class="fs-3 text-center my-2">
                                {{ __('general.' . $day) }} {{ $selectedWeek[$day]->format('d.m.y') }}
                            </h2>
                            <div class="row w-full d-flex justify-content-center">
                                @foreach($feedbacks as $feedback)
                                    <div
                                        class="col-xxl-3 col-xl-4 col-lg-6 col-md-9 d-flex justify-content-center flex-column p-1">
                                        <div class="border border-dark fs-5 w-full p-1">
                                            <p class="text-center fs-4 fw-bold">Від вчителя</p>
                                            <p class="text-center">{{ $feedback->teacher->full_name }}</p>
                                        </div>
                                        <div class="border border-dark fs-5 w-full p-1">
                                            <p class="text-center fs-4 fw-bold">Тема</p>
                                            <p class="text-center">{{ $feedback->subject }}</p>
                                        </div>
                                        <div class="border border-dark fs-5 w-full p-1">
                                            <p class="text-center fs-4 fw-bold">Вміст</p>
                                            <p class="text-center">{{ $feedback->description }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                @endforeach
                @if($weekFeedbacks->isEmpty())
                    <p class="text-center fs-3 w-full">Зауваження відсутні.</p>
                @endif
            </div>
        </div>
    @endif
@endsection
