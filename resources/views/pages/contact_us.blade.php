@extends('basic_layout')

@section('title', 'Зворотній зв\'язок')

@section('content')
    <div id="contact-us-content" class="d-flex flex-row justify-content-center align-items-center row">
        <div class="contact-info col-lg-5 my-3 p-1">
            <h2 class="text-center">Наші контактні дані</h2>
            <div>
                <p>Адреса: 31000, Хмельницька обл, місто Красилів, вул.Центральна, 38</p>
                <p>Електронна пошта: schl1@edu.kr.km.ua</p>
                <p>Телефон: 0385542704</p>
            </div>
            <div id="map"></div>
        </div>
        <div class="form px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg m-3 col-lg-6">
            <x-session-status class="mb-4" :status="session('status')"></x-session-status>
            <x-errors class="mb-4" :errors="$errors"></x-errors>
            <form method="POST" action="{{ route('contact-us') }}">
                @csrf
                <div>
                    <x-select name="receiver" style="font-size: 18px;" class="w-full">
                        <option disabled selected>Отримувач</option>
                        <option value="admin">Системний адміністратор</option>
                        <option value="director">Директор</option>
                        <option value="secretary">Секретар</option>
                        <option value="educational-deputy">Заступник директора з навчальної роботи</option>
                        <option value="organizer">Педагог-організатор</option>
                    </x-select>
                </div>
                <div class="mt-3">
                    <x-label for="message-subject" :value="__('Тема листа:')"></x-label>
                    <x-input id="message-subject" class="input-field block mt-1 w-full" name="messageSubject"
                             required type="text" :value="old('messageSubject')"></x-input>
                </div>
                <div class="mt-3">
                    <x-label for="message-body" :value="__('Вміст листа:')"></x-label>
                    <textarea id="message-body" class="input-field block mt-1 w-full" name="messageBody"
                             required rows="8">{{ old('messageBody') }}</textarea>
                </div>
                <div>
                    <p class="fs-6">*Даний лист повністю анонімний.</p>
                </div>
                <div class="flex items-center justify-end mt-2">
                    <x-submit-button class="sb-btn ml-3">Надіслати</x-submit-button>
                </div>
            </form>
        </div>
    </div>
@endsection
