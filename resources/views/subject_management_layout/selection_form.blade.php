<form class="d-flex flex-row justify-center align-items-center row m-2 school-class-selection-form">
    <x-label id="schoolClassLabel" for="schoolClass" class="col-xxl-5 col-xl-6 col-lg-8 col-md-9 text-center mb-1"
             :value="__('Виберіть номер класу, предмети якого хочете переглянути: ')"></x-label>
    <x-select id="schoolClass" onchange="window.location.href = '{{ url('subjects/class/') }}' + '/' + this.value;"
              class="col-xl-2 col-md-3 col-sm-5 col-8">
        <option hidden selected disabled></option>
        @for ($i = 1; $i <= 11; $i++)
            @if($i == $selectedClassNum)
                <option selected value="{{ $i }}">
                    {{ $i . ' Клас' }}
                </option>
            @else
                <option value="{{ $i }}">
                    {{ $i . ' Клас' }}
                </option>
            @endif
        @endfor
    </x-select>
</form>
