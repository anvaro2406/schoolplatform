<form class="d-flex flex-row justify-center align-items-center row m-2 fs-4">
        <x-label for="week" class="col-xxl-6 col-xl-7 col-lg-8 text-center"
                 :value="__('Виберіть тиждень в щоденнику, який хочете переглянути: ')"/>
        <x-select id="week" class="col-xl-3 col-lg-4 col-md-5 col-sm-7 text-wrap fs-5"
                  onchange="changeDiaryWeekSelect('{{ $studentId }}', this.value)">
            <option class="text-wrap" value="" selected disabled hidden></option>
            @foreach($weeks as $year => $yearWeeks)
                <optgroup label="{{ $year }}">
                    @foreach($yearWeeks as $week)
                    @if($week['orderNumber'] == $selectedWeekNumber)
                        <option selected value="{{ $week['orderNumber'] }}">
                            {{ $week['orderNumber'] }} Тиждень:
                            {{ $week['startDate']->format('d.m') . '-' . $week['endDate']->format('d.m') }}
                        </option>
                    @else
                        <option value="{{ $week['orderNumber'] }}">
                            {{ $week['orderNumber'] }} Тиждень:
                            {{ $week['startDate']->format('d.m') . '-' . $week['endDate']->format('d.m') }}
                        </option>
                    @endif
                @endforeach
                </optgroup>
            @endforeach
        </x-select>
</form>
