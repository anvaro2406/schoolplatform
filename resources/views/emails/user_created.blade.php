<table width="100%" cellpadding="0" cellspacing="0" role="presentation" style="box-sizing: border-box;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,
    'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; background-color: #edf2f7; margin: 0; padding: 0;
    width: 100%">
    <tbody>
    <tr>
        <td style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI',
            Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';
            padding: 25px 0; text-align: center">
            <a href="{{ url('/') }}" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont,
                'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji',
                'Segoe UI Symbol'; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none;
                display:inline-block" target="_blank">
                SchoolPlatform
            </a>
        </td>
    </tr>
    <tr>
        <td width="100%" cellpadding="0" cellspacing="0" style="box-sizing: border-box; font-family: -apple-system,
            BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji',
            'Segoe UI Symbol'; background-color: #edf2f7; border-bottom: 1px solid #edf2f7;
            border-top: 1px solid #edf2f7; margin: 0; padding: 0; width: 100%">
            <table class="m_-4661995270733944748inner-body" align="center" width="570" cellpadding="0" cellspacing="0"
                   role="presentation" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont,
                   'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji',
                   'Segoe UI Symbol'; background-color: #ffffff; border-color: #e8e5ef; border-radius: 2px;
                   border-width: 1px; margin: 0 auto; padding: 0; width: 570px">
                <tbody>
                <tr>
                    <td style="box-sizing: border-box; font-family: -apple-system,
                        BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji',
                        'Segoe UI Emoji', 'Segoe UI Symbol'; max-width: 100vw; padding: 32px">
                        <h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI',
                            Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji',
                            'Segoe UI Symbol'; color: #3d4852; font-size: 18px; font-weight: bold; margin-top: 0;
                            text-align: left">
                            Доброго дня,
                        </h1>
                        <p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI',
                            Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji',
                            'Segoe UI Symbol'; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left">
                            Ви отримали цей лист, тому що вас зареєстрували у системі SchoolPlatform. Ви можете відкрити
                            головну сторінку системи, натиснувши на посилання вище.
                        </p>
                        <p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI',
                            Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji',
                            'Segoe UI Symbol'; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left">
                            Ваш пароль: <b>{{ $password }}</b>.
                        </p>
                        <p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI',
                            Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji',
                            'Segoe UI Symbol'; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left">
                            Ви можете змінити пароль будь-якої миті, відкривши сторінку вашого облікового запису та
                            виконавши всі необхідні дії.
                        </p>
                        <p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI',
                            Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji',
                            'Segoe UI Symbol'; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left">
                            З найкращими побажаннями,<br/>
                            {{ $sender->firstname . " " . $sender->lastname }}
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
            Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';">
            <table class="m_8909233378387545379footer" align="center" width="570" cellpadding="0" cellspacing="0"
                   role="presentation" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont,
                   'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji',
                   'Segoe UI Symbol'; margin: 0 auto; padding: 0; text-align: center; width: 570px">
                <tbody>
                <tr>
                    <td align="center" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont,
                        'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji',
                        'Segoe UI Symbol'; max-width: 100vw; padding: 32px">
                        <p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI',
                            Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji',
                            'Segoe UI Symbol'; line-height: 1.5em; margin-top: 0; color: #b0adc5; font-size: 12px;
                            text-align: center">
                            © 2022 SchoolPlatform. Всі права збережено.
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
