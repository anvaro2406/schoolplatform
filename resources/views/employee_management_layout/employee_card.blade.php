<div class="col-xxl-2 col-lg-3 col-md-4 col-sm-6 col-7 p-1 m-0.5 employee-card sm:max-w-md bg-white shadow-md
            overflow-hidden sm:rounded-lg border">
    <a href="{{ route('employees.show', ['employee' => $employee->id]) }}">
        <div class="d-flex align-items-center justify-content-center employee-card-image-container">
            @if($employee->photo_filepath != "")
                <img src="@storageAsset($employee->photo_filepath)" alt=""/>
            @else
                <i class="bi bi-person fs-1"></i>
            @endif
        </div>
        <h2>{{ $employee->lastname }}</h2>
        <h2>{{ $employee->firstname }}</h2>
        <h2>{{ $employee->patronymic }}</h2>
        <h3 class="mt-1">{{ $employee->employeeProperty->position }}</h3>
    </a>
</div>
