@section('sidebar')
    <div id="sidebar" class="col-xxl-2 col-xl-2">
        <h2 class="text-center">Категорії</h2>
        <ul>
            <li>
                <a href="{{ route('events.index') }}">Всі події</a>
            </li>
            <li>
                <a href="{{ route('events.archive') }}">Архів подій</a>
            </li>
            @foreach ($categories as $category)
                <li>
                    <a href="{{ url('events/category/' . $category->name) }}">{{ $category->name }}</a>
                </li>
            @endforeach
        </ul>
    </div>
@endsection
