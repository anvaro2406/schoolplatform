<div class="container post p-1">
    <div class="border p-1">
        <div class="col" id="{{ 'event' . $event->id }}">
            <h1 class="d-block text-center">{{ $event->title }}</h1>
            <div class="row dates">
                <span class="col-md-6 text-center">
                    <b>Початок:</b>&nbsp;
                    {{ $event->startDatetime->translatedFormat('j F Y, G:i') }}
                </span>
                <span class="col-md-6 text-center">
                    <b>Завершення:</b>&nbsp;
                    {{ $event->endDatetime->translatedFormat('j F Y, G:i') }}
                </span>
            </div>
            <div class="row">
                <div class="col-lg-6 d-flex align-items-center justify-content-center">
                    @if($event->image_filepath != "")
                        <img src="@storageAsset($event->image_filepath)" alt=""/>
                    @else
                        <i class="bi bi-images fs-1"></i>
                    @endif
                </div>
                <p class="mb-4 d-block col-lg-6">{{ $event->description }}</p>
            </div>
        </div>
        <div id="{{ 'eventMenu' . $event->id }}" class="d-none event-menu">
            <div class="d-flex flex-row justify-content-end">
                <i class="bi bi-x fs-2 mr-1 d-none" id="{{ 'eventMenuCloseBtn' . $event->id }}"
                   onclick="eventMenuBtnClick({{ $event->id }})">
                </i>
            </div>
            <div class="d-flex flex-col justify-content-center align-items-center">
                <a href="{{ route('events.edit', ['event' => $event->id]) }}" class="justify-content-center w-50 p-0">
                    <x-button class="w-full justify-content-center">Змінити подію</x-button>
                </a>
                <form class="p-0 text-center w-50" method="POST"
                      action="{{ route('events.destroy', ['event' => $event->id]) }}">
                    @csrf
                    @method('DELETE')
                    <x-button
                        onclick="deletionSubmit('Ви впевнені, що хочете видалити дану подію?', this.closest('form'))"
                        class="w-full justify-content-center">
                        Видалити подію
                    </x-button>
                </form>
            </div>
        </div>
        <div class="d-flex flex-row justify-content-end mt-2">
            <div class="post-meta d-flex align-items-center mb-0">
                <span>
                    <a href="{{ route('events.category-name', ['category' => $event->category->name]) }}">
                        {{ $event->category->name }}
                    </a>
                </span>
                <span class="mx-1">&bullet;</span>
                <span>{{ $event->updated_at->translatedFormat('j F Y, G:i') }}</span>
            </div>
            @if(Auth::check() && Auth::user()->can('update event') && Auth::user()->can('delete event')
                    && $event->deleted_at == null)
                <i class="bi bi-three-dots-vertical fs-5 mx-2" id="{{ 'eventMenuOpenBtn' . $event->id }}"
                   onclick="eventMenuBtnClick({{ $event->id }})">
                </i>
            @endif
        </div>
    </div>
</div>
