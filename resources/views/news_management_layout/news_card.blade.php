<div class="container post p-1">
    <div class="border p-1">
        <div id="{{ 'news' . $singleNews->id }}">
            <h1 class="text-center mb-2">
                <a href="{{ route('news.show', ['news' => $singleNews->id]) }}">{{ $singleNews->title }}</a>
            </h1>
            <div class="row">
                <a href="{{ route('news.show', ['news' => $singleNews->id]) }}" class="news-card-image col-lg-6 block">
                    <script>
                        getFirstNewsImage(
                            '{{ route('news.show', ['news' => $singleNews->id]) }}',
                            '{!! trim(preg_replace('/\s\s+/', ' ', $singleNews->content)) !!}'
                        );
                    </script>
                </a>
                <p class="mb-4 d-block col-lg-6">{{ $singleNews->description }}</p>
            </div>
        </div>
        <div id="{{ 'newsMenu' . $singleNews->id }}" class="d-none news-menu">
            <div class="d-flex flex-row justify-content-end">
                <i class="bi bi-x fs-2 mr-1 d-none" id="{{ 'newsMenuCloseBtn' . $singleNews->id }}"
                   onclick="newsMenuBtnClick({{ $singleNews->id }})">
                </i>
            </div>
            <div class="d-flex flex-col justify-content-center align-items-center">
                <a href="{{ route('news.edit', ['news' => $singleNews->id]) }}" class="justify-content-center w-50 p-0">
                    <x-button class="w-full justify-content-center">Змінити новину</x-button>
                </a>
                <form class="p-0 text-center w-50" method="POST"
                      action="{{ route('news.destroy', ['news' => $singleNews->id]) }}">
                    @csrf
                    @method('DELETE')
                    <x-button
                        onclick="deletionSubmit('Ви впевнені, що хочете видалити дану новину?', this.closest('form'))"
                        class="w-full justify-content-center">
                        Видалити новину
                    </x-button>
                </form>
            </div>
        </div>
        <div class="row mt-2">
            <div class="d-flex align-items-center author col-md-6">
                @if($singleNews->author->photo_filepath != '')
                    <div class="photo">
                        <img src="@storageAsset($singleNews->author->photo_filepath)" alt="" class="img-fluid">
                    </div>
                @else
                    <div class="empty-photo border">
                        <i class="bi bi-person"></i>
                    </div>
                @endif
                <div class="name">
                    <h3 class="m-0 p-0">
                        <a href="{{ route('users.show', ['user' => $singleNews->author->id]) }}">
                            {{ $singleNews->author->fullName }}
                        </a>
                    </h3>
                </div>
            </div>
            <div class="d-flex flex-row justify-content-end col-md-6">
                <div class="post-meta d-flex align-items-center mb-0">
                <span>
                    <a href="{{ route('news.category-name', ['category' => $singleNews->category->name]) }}">
                        {{ $singleNews->category->name }}
                    </a>
                </span>
                    <span class="mx-1">&bullet;</span>
                    <span>{{ $singleNews->updated_at->translatedFormat('j F Y, G:i') }}</span>
                </div>
                @if(Auth::check() && Auth::user()->can('update news') && Auth::user()->can('delete news'))
                    <i class="bi bi-three-dots-vertical fs-5 mx-2 d-flex align-items-center" id="{{ 'newsMenuOpenBtn' . $singleNews->id }}"
                       onclick="newsMenuBtnClick({{ $singleNews->id }})">
                    </i>
                @endif
            </div>
        </div>
    </div>
</div>
