@section('sidebar')
    <div id="sidebar" class="col-xxl-2 col-xl-2">
        <h2 class="text-center">Категорії</h2>
        <ul>
            <li>
                <a href="{{ route('news.index') }}">Всі новини</a>
            </li>
            @foreach ($categories as $category)
                <li>
                    <a href="{{ url('news/category/' . $category->name) }}">{{ $category->name }}</a>
                </li>
            @endforeach
        </ul>
    </div>
@endsection
