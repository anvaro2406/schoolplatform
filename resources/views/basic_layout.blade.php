<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>

    @include('basic_layout.head_links')
</head>
<body class="d-flex flex-column min-vh-100">

@include('basic_layout.mobile_drawers')

@include('basic_layout.header')

<div class="row p-0 m-0 flex-grow-1">
    @yield('sidebar')

    <div id="main" class="col-10">
        @yield('content')
    </div>
</div>

@include('basic_layout.footer')
@include('basic_layout.up_button')
@include('basic_layout.sidebar_drawer_button')
@include('basic_layout.scripts_links')
</body>
</html>
