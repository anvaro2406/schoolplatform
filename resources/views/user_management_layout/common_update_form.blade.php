<div class="row m-0 p-0 text-center">
    <div class="col-xxl-3 col-md-6">
        <x-label for="firstname" :value="__('Ім\'я')"></x-label>
        <x-input id="firstname" class="input-field mt-1 w-full" type="text" name="firstname"
                 :value="old('firstname', $user->firstname)" required></x-input>
    </div>
    <div class="col-xxl-3 col-md-6">
        <x-label for="lastname" :value="__('Прізвище')"></x-label>
        <x-input id="lastname" class="input-field block mt-1 w-full" type="text" name="lastname"
                 :value="old('lastname', $user->lastname)" required></x-input>
    </div>
    <div class="col-xxl-3 col-md-6">
        <x-label for="patronymic" :value="__('По-батькові')"></x-label>
        <x-input id="patronymic" class="input-field block mt-1 w-full" type="text" name="patronymic"
                 :value="old('patronymic', $user->patronymic)" required></x-input>
    </div>
    <div class="col-xxl-3 col-md-6">
        <x-label for="gender" :value="__('Стать')"></x-label>
        <x-select id="gender" class="input-field block mt-1 w-full" name="gender" required>
            @if ((old('gender', $user->gender) ?? '') == '')
                <option value="male">Чоловік</option>
                <option value="female">Жінка</option>
            @elseif(old('gender', $user->gender) == 'male')
                <option value="male" selected>Чоловік</option>
                <option value="female">Жінка</option>
            @else
                <option value="male">Чоловік</option>
                <option value="female" selected>Жінка</option>
            @endif
        </x-select>
    </div>
</div>
<div class="row m-0 mt-4 p-0 text-center">
    <div class="col-xxl-3 col-md-6">
        <x-label for="address" :value="__('Адреса')"></x-label>
        <x-input id="address" class="input-field block mt-1 w-full" type="text" name="address"
                 :value="old('address', $user->address)" required></x-input>
    </div>
    <div class="col-xxl-3 col-md-6">
        <x-label for="phone" :value="__('Телефон')"></x-label>
        <x-input id="phone" class="input-field block mt-1 w-full" type="text" name="phone"
                 :value="old('phone', $user->phone)"></x-input>
    </div>
    <div class="col-xxl-3 col-md-6">
        <x-label for="email" :value="__('Електронна пошта')"></x-label>
        <x-input id="email" class="input-field block mt-1 w-full" type="email" name="email"
                 :value="old('email', $user->email)" required></x-input>
    </div>
    <div class="col-xxl-3 col-md-6">
        <x-label for="birth_date" :value="__('Дата народження')"></x-label>
        <x-input id="birth_date" class="input-field block mt-1 w-full" type="date" name="birth_date"
                 :value="old('birth_date', $user->birth_date)" required></x-input>
    </div>
</div>
<div class="row m-0 mt-4 p-0 text-center d-flex flex-row justify-content-center">
    <div class="col-xxl-3 col-md-6">
        <x-label for="photo" :value="__('Фото')"></x-label>
        <input id="photo" class="input-field block mt-1 w-full" type="file" name="photo"
               accept="image/png, image/jpeg, image/gif, image/bmp"/>
    </div>
</div>
