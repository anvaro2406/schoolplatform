<div class="col-xl-3 col-md-12 row p-0 m-0">
    <div class="col-xl-12 col-md-5 text-center">
        <x-label for="position" :value="__('Позиція')"></x-label>
        <x-input id="position" class="input-field block mt-1 w-full" type="text" name="position"
                 :value="old('position')" required></x-input>
    </div>
    <div class="col-xl-12 col-md-5 text-center">
        <x-label for="science_degree" :value="__('Науковий ступінь')"></x-label>
        <x-input id="science_degree" class="input-field block mt-1 w-full" type="text"
                 name="science_degree" :value="old('science_degree')"></x-input>
    </div>
    <div class="col-xl-12 col-md-2 text-center d-flex flex-column align-items-center">
        <x-label for="experience" :value="__('Досвід')"></x-label>
        <x-input id="experience" class="input-field block mt-1 w-full" type="number" name="experience"
                 :value="old('experience')" required min="0" max="100"></x-input>
    </div>
</div>
<div class="col-xl-9 col-md-12">
    <div class="d-flex flex-column align-items-center">
        <x-label for="biography" :value="__('Коротка біографія')"></x-label>
        <textarea rows="7" id="biography" class="input-field block mt-1 w-full" name="biography" required>
            {{ old('biography') }}
        </textarea>
    </div>
</div>

