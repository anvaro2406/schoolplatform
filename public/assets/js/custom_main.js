function setContentSectionHeight() {
    let mainHeight = document.getElementById("main").offsetHeight;
    let loginContent = document.getElementById("login-content");
    let forgotPasswordContent = document.getElementById("forgot-password-content");
    let resetPasswordContent = document.getElementById("reset-password-content");
    let confirmPasswordContent = document.getElementById("confirm-password-content");
    let currentUserResetPasswordContent = document.getElementById("current-user-reset-password-content");
    let contactUsContent = document.getElementById("contact-us-content");

    if (loginContent !== null) {
        loginContent.style.minHeight = mainHeight + "px";
    } else if (forgotPasswordContent !== null) {
        forgotPasswordContent.style.minHeight = mainHeight + "px";
    } else if (resetPasswordContent !== null) {
        resetPasswordContent.style.minHeight = mainHeight + "px";
    } else if (confirmPasswordContent !== null) {
        confirmPasswordContent.style.minHeight = mainHeight + "px";
    } else if (currentUserResetPasswordContent !== null) {
        currentUserResetPasswordContent.style.minHeight = mainHeight + "px";
    } else if (contactUsContent !== null) {
        contactUsContent.style.minHeight = mainHeight + "px";
    }
}

function wyziwygInitialization() {
    tinymce.init({
        selector: 'textarea#wyziwyg-editor',
        resize: true,
        language: 'uk',
        plugins: 'code table lists image',
        toolbar: 'undo redo | blocks | bold italic | alignleft aligncenter alignright | indent outdent | ' +
            'bullist numlist | code | table | image',
        file_picker_callback: function (callback, value, meta) {
            if (meta.filetype === 'image') {
                let input = document.getElementById('wyziwyg-image');
                input.click();
                input.onchange = function () {
                    let file = input.files[0];
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        callback(e.target.result, {
                            alt: file.name
                        });
                    };
                    reader.readAsDataURL(file);
                };
            }
        }
    });
}

function setJournalSelectionFormOnLoad() {
    let journalStudyYearSelect = document.getElementById('studyYear');
    let journalSchoolClassSelect = document.getElementById('schoolClass');

    if (journalStudyYearSelect && journalSchoolClassSelect) {
        if (journalStudyYearSelect.value !== journalStudyYearSelect.options[0].value) {
            let fullUrl = '/journals/study-year/' + journalStudyYearSelect.value + '/false';
            let newSchoolClassSelectText = '';

            fetch(fullUrl)
                .then(response => response.json())
                .then(data => {
                    for (const index in data) {
                        if (data[index]['id'] === parseInt(journalSchoolClassSelect.value)) {
                            newSchoolClassSelectText += '<option selected value="' + data[index]['id'] + '">'
                                + data[index]['name'] + '</option>';
                        } else {
                            newSchoolClassSelectText += '<option value="' + data[index]['id'] + '">'
                                + data[index]['name'] + '</option>';
                        }
                    }
                    journalSchoolClassSelect.innerHTML = newSchoolClassSelectText;
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }
}

function setCreateJournalSelectionsOnLoad() {
    let schoolProgram = document.getElementById('schoolProgram');
    let schoolClass = document.getElementById('schoolClass');
    let abstractSubjectsList = document.getElementsByClassName('abstract-subject-row')

    if (schoolProgram && schoolClass && abstractSubjectsList.count !== 0) {
        let selectedSchoolProgramId = schoolProgram.options[schoolProgram.selectedIndex].value;
        let selectedClassNumber = schoolClass.options[schoolClass.selectedIndex].text.slice(0, -2);

        Array.from(abstractSubjectsList).forEach(function (abstractSubject) {
            let classNumber = abstractSubject.getElementsByClassName('class-number-cell')[0].textContent;
            let schoolProgramId = abstractSubject.getElementsByClassName('school-program-cell')[0].textContent;

            if (classNumber !== selectedClassNumber || schoolProgramId !== selectedSchoolProgramId) {
                abstractSubject.classList.add('d-none');

                let selects = abstractSubject.getElementsByTagName('select');
                Array.from(selects).forEach((select) => select.disabled = true);
                let inputs = abstractSubject.getElementsByTagName('input');
                Array.from(inputs).forEach((select) => select.disabled = true);
            } else {
                let selects = abstractSubject.getElementsByTagName('select');
                let inputs = abstractSubject.getElementsByTagName('input');

                if (inputs[0].checked) {
                    // input of whole class subject teacher data
                    selects[0].parentElement.classList.add('d-none'); // td tag
                    selects[0].disabled = true; // teacher selection
                    inputs[1].disabled = true; // hidden abstract subject id input
                    inputs[2].disabled = true; // hidden group input

                    // input of first group subject teacher data
                    selects[1].parentElement.classList.remove('d-none'); // td tag
                    selects[1].disabled = false; // teacher selection
                    inputs[3].disabled = false; // hidden abstract subject id input
                    inputs[4].disabled = false; // hidden group input

                    // input of second group subject teacher data
                    selects[2].parentElement.classList.remove('d-none'); // td tag
                    selects[2].disabled = false; // teacher selection
                    inputs[5].disabled = false; // hidden abstract subject id input
                    inputs[6].disabled = false; // hidden group input
                }
            }
        });
    }
}

function setUpdateJournalSelectionsOnLoad() {
    let schoolProgram = document.getElementById('schoolProgram');
    let abstractSubjectsList = document.getElementsByClassName('abstract-subject-row')

    if (schoolProgram && abstractSubjectsList.count !== 0) {
        let selectedSchoolProgramId = schoolProgram.options[schoolProgram.selectedIndex].value;

        Array.from(abstractSubjectsList).forEach(function (abstractSubject) {
            let schoolProgramId = abstractSubject.getElementsByClassName('school-program-cell')[0].textContent;

            if (schoolProgramId !== selectedSchoolProgramId) {
                abstractSubject.classList.add('d-none');

                let selects = abstractSubject.getElementsByTagName('select');
                Array.from(selects).forEach((select) => select.disabled = true);
                let inputs = abstractSubject.getElementsByTagName('input');
                Array.from(inputs).forEach((select) => select.disabled = true);
            } else {
                let selects = abstractSubject.getElementsByTagName('select');
                let inputs = abstractSubject.getElementsByTagName('input');

                if (inputs[0].checked) {
                    // input of whole class subject teacher data
                    selects[0].parentElement.classList.add('d-none'); // td tag
                    selects[0].disabled = true; // teacher selection
                    inputs[1].disabled = true; // hidden abstract subject id input
                    inputs[2].disabled = true; // hidden group input

                    // input of first group subject teacher data
                    selects[1].parentElement.classList.remove('d-none'); // td tag
                    selects[1].disabled = false; // teacher selection
                    inputs[3].disabled = false; // hidden abstract subject id input
                    inputs[4].disabled = false; // hidden group input

                    // input of second group subject teacher data
                    selects[2].parentElement.classList.remove('d-none'); // td tag
                    selects[2].disabled = false; // teacher selection
                    inputs[5].disabled = false; // hidden abstract subject id input
                    inputs[6].disabled = false; // hidden group input
                }
            }
        });
    }
}

function setScheduleSelectionsOnLoad() {
    let schoolClass = document.getElementById('schoolClass');
    let timeSlots = document.getElementsByClassName('time-slots-row');

    if (schoolClass && schoolClass.value !== '') {
        if(schoolClass.value !== '0') {
            let selectedSchoolClassId = schoolClass.options[schoolClass.selectedIndex].value;
            let subjectsSelects = Array.from(document.getElementsByClassName('subject-select'));
            subjectsSelects.forEach(function (subjectSelect) {
                Array.from(subjectSelect.options).forEach(function (option) {
                    let optionSchoolClassId = option.getAttribute('data-school-class-id');

                    if (optionSchoolClassId && optionSchoolClassId !== selectedSchoolClassId) {
                        option.disabled = true;
                        option.classList.add('d-none');
                    } else {
                        option.disabled = false;
                        option.classList.remove('d-none');
                    }
                })
            });
        }

        Array.from(timeSlots).forEach(function (timeSlot) {
            let inputs = timeSlot.getElementsByTagName('input');
            let selects = timeSlot.getElementsByTagName('select');

            if (inputs[0].checked) {
                // input of whole class subject data
                selects[0].parentElement.classList.add('d-none'); // td tag
                selects[0].disabled = true; // subject selection
                inputs[1].disabled = true; // hidden time slot id input
                inputs[2].disabled = true; // hidden day input

                // input of first group subject data
                selects[1].parentElement.classList.remove('d-none'); // td tag
                selects[1].disabled = false; // subject selection
                inputs[3].disabled = false; // hidden time slot id input
                inputs[4].disabled = false; // hidden day input

                // input of second group subject data
                selects[2].parentElement.classList.remove('d-none'); // td tag
                selects[2].disabled = false; // subject selection
                inputs[5].disabled = false; // hidden time slot id input
                inputs[6].disabled = false; // hidden day input
            }
        });
    }
}

function setTeacherFeedbackFormModalWindow(){
    let modalWindow = document.getElementById('feedback-form-modal-window');

    if (modalWindow) {
        let studentName = modalWindow.querySelector('#studentName');
        let studentIdInput = modalWindow.querySelector('#student');

        if(studentName && studentIdInput && studentIdInput.value !== ''){
            let allOpenBtns = document.getElementsByClassName('open-feedback-form-btn');
            let chosenBtn = Array.from(allOpenBtns).filter(function(btn) {
                return btn.getAttribute('data-student-id') === studentIdInput.value;
            })[0];
            studentName.innerHTML = 'Зауваження для учня: <br>' + chosenBtn.innerText;
            modalWindow.classList.remove('d-none');
        }
    }
}

function setSchoolClassCreationFormOnLoad(){
    let studentCheckboxes = Array.from(document.getElementsByClassName('student-checkbox'));
    let isAutoGroupingCheckbox = document.getElementById('isAutoGrouping');

    toggleClassAutoGrouping(isAutoGroupingCheckbox);
    if (studentCheckboxes) {
        studentCheckboxes.forEach(function (checkbox) {
            toggleStudentCheckbox(checkbox);
        });
    }
}

function windowLoad() {
    setContentSectionHeight();
    wyziwygInitialization();
    setJournalSelectionFormOnLoad();
    setSchoolClassCreationFormOnLoad();
    setCreateJournalSelectionsOnLoad();
    setUpdateJournalSelectionsOnLoad();
    setScheduleSelectionsOnLoad();
    setTeacherFeedbackFormModalWindow();
}

function feedbackFormModalWindowOpen(studentId, studentFullName) {
    let modalWindow = document.getElementById('feedback-form-modal-window');
    let studentName = modalWindow.querySelector('#studentName');
    let studentIdInput = modalWindow.querySelector('#student');

    if (modalWindow && studentName && studentIdInput) {
        modalWindow.classList.remove('d-none');

        studentIdInput.setAttribute('value', studentId);
        studentName.innerHTML = 'Зауваження для учня: <br>' + studentFullName;
    }
}

function feedbackFormModalWindowClose(){
    let modalWindow = document.getElementById('feedback-form-modal-window');

    if (modalWindow) {
        modalWindow.classList.add('d-none');
    }
}

function changeScheduleClassSelect(selectedSchoolClassId, selectedStudyYearId) {
    let subjectsSelects = Array.from(document.getElementsByClassName('subject-select'));

    subjectsSelects.forEach(function (subjectSelect) {
        subjectSelect.selectedIndex = 0;

        Array.from(subjectSelect.options).forEach(function (option) {
            let optionSchoolClassId = option.getAttribute('data-school-class-id');
            let optionStudyYearId = option.getAttribute('data-study-year-id');

            if (optionSchoolClassId
                && selectedStudyYearId
                && (optionSchoolClassId !== selectedSchoolClassId
                || parseInt(optionStudyYearId) !== selectedStudyYearId)
            ) {
                option.disabled = true;
                option.classList.add('d-none');
            } else {
                option.disabled = false;
                option.classList.remove('d-none');
            }
        });
    });
}

function changeDiaryWeekSelect(studentId, weekNumber) {
    window.location.href = '/diary/' + studentId + '/' + weekNumber;
}

function changeScheduleOwnerSelect(type, selectedId) {
    let showType = (type === 'classes') ? 'classes' : 'users';

    window.location.href = '/schedules/' + showType + '/' + selectedId;
}

function toggleClassAutoGrouping(checkbox){
    let groupSelects = Array.from(document.getElementsByClassName('select-group'));

    if(groupSelects){
        groupSelects.forEach(function (select) {
            select.disabled = checkbox.checked;
            if(checkbox.checked){
                select.selectedIndex = 0;
            }
        });
    }
}

function toggleStudentCheckbox(checkbox){
    let studentIdInput = checkbox.previousElementSibling;
    let groupSelect = checkbox.closest('td').nextElementSibling.nextElementSibling.getElementsByTagName('select')[0];

    studentIdInput.disabled = !checkbox.checked;
    groupSelect.disabled = !checkbox.checked;
    if(!checkbox.checked){
        groupSelect.selectedIndex = 0;
    }
}

function classStudentSearch(input){
    let filter = input.value.toUpperCase();
    let tableDiv = input.nextElementSibling;
    let studentsRows = tableDiv.getElementsByTagName("tr");
    let txtValue;

    for (let i = 0; i < studentsRows.length; i++) {
        let tds = studentsRows[i].getElementsByTagName("td");
        let nameTd = tds[1];

        txtValue = nameTd.textContent || nameTd.innerText;
        if(txtValue.toUpperCase().indexOf(filter) <= -1){
            studentsRows[i].classList.add('d-none');
        }else{
            studentsRows[i].classList.remove('d-none');
        }
    }
}

function changeSchoolClassSelect(select, url) {
    window.location.href = url + '/' + select.value;
}

function changeJournalGroupCheckbox(checkbox) {
    let parentTd = checkbox.parentElement;
    let noGroupsTd = parentTd.nextElementSibling;
    let firstGroupTd = noGroupsTd.nextElementSibling;
    let secondGroupTd = firstGroupTd.nextElementSibling;

    if (checkbox.checked) {
        noGroupsTd.classList.add('d-none');
        firstGroupTd.classList.remove('d-none');
        secondGroupTd.classList.remove('d-none');
    } else {
        noGroupsTd.classList.remove('d-none');
        firstGroupTd.classList.add('d-none');
        secondGroupTd.classList.add('d-none');
    }

    noGroupsTd.querySelector("select").disabled = checkbox.checked;
    noGroupsTd.querySelector("input").disabled = checkbox.checked;
    noGroupsTd.querySelector("input").nextElementSibling.disabled = checkbox.checked;
    firstGroupTd.querySelector("select").disabled = !checkbox.checked;
    firstGroupTd.querySelector("input").disabled = !checkbox.checked;
    firstGroupTd.querySelector("input").nextElementSibling.disabled = !checkbox.checked;
    secondGroupTd.querySelector("select").disabled = !checkbox.checked;
    secondGroupTd.querySelector("input").disabled = !checkbox.checked;
    secondGroupTd.querySelector("input").nextElementSibling.disabled = !checkbox.checked;
}

function changeJournalMainSelectors() {
    let schoolProgram = document.getElementById('schoolProgram');
    let schoolClass = document.getElementById('schoolClass');
    let abstractSubjectsList = document.getElementsByClassName('abstract-subject-row')

    let selectedSchoolProgramId = schoolProgram.options[schoolProgram.selectedIndex].value;
    let selectedClassNumber = schoolClass.options[schoolClass.selectedIndex].text.slice(0, -2);

    Array.from(abstractSubjectsList).forEach(function (abstractSubject) {
        let classNumber = abstractSubject.getElementsByClassName('class-number-cell')[0].textContent;
        let schoolProgramId = abstractSubject.getElementsByClassName('school-program-cell')[0].textContent;

        if (classNumber !== selectedClassNumber || schoolProgramId !== selectedSchoolProgramId) {
            abstractSubject.classList.add('d-none');

            let selects = abstractSubject.getElementsByTagName('select');
            Array.from(selects).forEach((select) => select.disabled = true);
            let inputs = abstractSubject.getElementsByTagName('input');
            Array.from(inputs).forEach((select) => select.disabled = true);
        } else {
            abstractSubject.classList.remove('d-none');

            let selects = abstractSubject.getElementsByTagName('select');
            let inputs = abstractSubject.getElementsByTagName('input');

            // checkbox changes
            inputs[0].disabled = false;
            inputs[0].checked = false;

            // input of whole class subject teacher data
            selects[0].parentElement.classList.remove('d-none'); // td tag
            selects[0].disabled = false; // teacher selection
            inputs[1].disabled = false; // hidden abstract subject id input
            inputs[2].disabled = false; // hidden group input

            // input of first group subject teacher data
            selects[1].parentElement.classList.add('d-none'); // td tag
            selects[1].disabled = true; // teacher selection
            inputs[3].disabled = true; // hidden abstract subject id input
            inputs[4].disabled = true; // hidden group input

            // input of second group subject teacher data
            selects[2].parentElement.classList.add('d-none'); // td tag
            selects[2].disabled = true; // teacher selection
            inputs[5].disabled = true; // hidden abstract subject id input
            inputs[6].disabled = true; // hidden group input
        }
    });
}

function changeLessonData(id, input) {
    const headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'X-CSRF-TOKEN': document.querySelector('input[name="_token"]').value
    };
    const body = {
        fieldName: input.name,
        value: input.value
    };

    fetch('/lessons/' + id, {
        method: 'PATCH',
        headers: headers,
        body: JSON.stringify(body)
    })
        .then(response => response.json())
        .then(data => {
            if (data.errors) {
                input.value = '';
                alert(data.message);
                return;
            }
            if (input.name === 'date') {
                Array.from(document.getElementsByClassName('dateInput' + id))
                    .forEach(date => date.value = input.value);
            }
        })
        .catch(error => {
            console.log(error)
        });
}

function changeJournalCellData(lessonId, studentId, input) {
    const headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'X-CSRF-TOKEN': document.querySelector('input[name="_token"]').value
    };
    const body = {
        lessonId: lessonId,
        studentId: studentId,
        value: input.value,
    };

    fetch('/journal-cells', {
        method: 'PATCH',
        headers: headers,
        body: JSON.stringify(body)
    })
        .then(response => response.json())
        .then(data => {
            if (data.errors) {
                input.value = '';
                alert(data.message)
            }
        })
        .catch(error => {
            console.log(error)
        });
}

function clickJournalSubjectTabTitle(thisDiv, tabId) {
    document.getElementById(tabId).classList.toggle('d-none');

    let iTag = thisDiv.getElementsByTagName('i')[0];
    iTag.classList.toggle('bi-chevron-down');
    iTag.classList.toggle('bi-chevron-up');
}

function submitJournalSelect() {
    let schoolClass = document.getElementById('schoolClass');
    let studyYear = document.getElementById('studyYear');

    window.location.href = '/journals/show/' + schoolClass.value + '/' + studyYear.value;
}

function changeJournalStudyYearSelect(selectedSchoolClassId, isJournalCreateCall) {
    let studyYear = document.getElementById('studyYear');
    let schoolClassSelect = document.getElementById('schoolClass');
    let fullUrl = '/journals/study-year/' + studyYear.value + '/' + isJournalCreateCall;
    let newSchoolClassSelectText = '';

    fetch(fullUrl)
        .then(response => response.json())
        .then(data => {
            for (const index in data) {
                if (data[index]['id'] === selectedSchoolClassId) {
                    newSchoolClassSelectText += '<option selected value="' + data[index]['id'] + '">'
                        + data[index]['name'] + '</option>';
                } else {
                    newSchoolClassSelectText += '<option value="' + data[index]['id'] + '">'
                        + data[index]['name'] + '</option>';
                }
            }
            if(Array.isArray(data) && data.length === 0){
                newSchoolClassSelectText += '<option value="" selected hidden disabled>Клас</option>';
            }
            schoolClassSelect.innerHTML = newSchoolClassSelectText;
            if(!selectedSchoolClassId){ // create journal page
                const event = new Event('change');
                schoolClassSelect.dispatchEvent(event);
            }
        })
        .catch(error => {
            console.log(error);
        });
}

function getFirstNewsImage(route, newsContent) {
    let allLinks = Array.from(document.getElementsByClassName('news-card-image'));
    let selectedLink = allLinks.filter(element => element.href === route)[0];

    let tmpElement = document.createElement("div");
    tmpElement.innerHTML = newsContent;
    let image = tmpElement.querySelector("img");

    if (image === null) {
        selectedLink.innerHTML = '<div class="d-flex justify-content-center align-items-center w-full" ' +
            'style="min-height: 200px; border: black solid 1px;"><i class="bi bi-images fs-1"></i></div>';
    } else {
        image.removeAttribute('style');
        image.removeAttribute('width');
        image.removeAttribute('height');
        image.style = 'width: 100%;\nheight: auto;';
        selectedLink.innerHTML = image.outerHTML;
    }
}

function changeItemsPerPage() {
    document.getElementById("items-per-page-form").submit();
}

function changeSortBy() {
    document.getElementById("sort-by-form").submit();
}

function selectSearchFunction(input) {
    let filter = input.value.toUpperCase();
    let select = input.nextElementSibling;
    let options = select.getElementsByTagName("option");
    let txtValue;

    for (let i = 0; i < options.length; i++) {
        txtValue = options[i].textContent || options[i].innerText;
        options[i].hidden = txtValue.toUpperCase().indexOf(filter) <= -1;
    }
}

function deletionSubmit(confirmationMessage, form) {
    event.preventDefault();

    if (confirm(confirmationMessage)) {
        form.submit();
    }
}

function formSubmitWithConfirmation(confirmationMessage, form) {
    event.preventDefault();

    if (confirm(confirmationMessage)) {
        form.submit();
    }
}

function eventMenuBtnClick(id) {
    let event = document.getElementById('event' + id);
    let menu = document.getElementById('eventMenu' + id);
    let menuOpenBtn = document.getElementById('eventMenuOpenBtn' + id);
    let menuCLoseBtn = document.getElementById('eventMenuCloseBtn' + id);

    event.classList.toggle('d-none');
    menu.classList.toggle('d-none');
    menuOpenBtn.classList.toggle('d-none');
    menuCLoseBtn.classList.toggle('d-none');
}

function newsMenuBtnClick(id) {
    let news = document.getElementById('news' + id);
    let menu = document.getElementById('newsMenu' + id);
    let menuOpenBtn = document.getElementById('newsMenuOpenBtn' + id);
    let menuCLoseBtn = document.getElementById('newsMenuCloseBtn' + id);

    news.classList.toggle('d-none');
    menu.classList.toggle('d-none');
    menuOpenBtn.classList.toggle('d-none');
    menuCLoseBtn.classList.toggle('d-none');
}

function DOMContentLoaded() {
    "use strict";

    /**
     *  Navbar drawer open/close
     */
    let navbar = document.getElementById('navbar');
    let navbarDrawerOpenButtonForUserActions = document.getElementById('open-navbar-drawer-user-actions-btn');
    let navbarDrawerOpenButton = document.getElementById('open-navbar-drawer-btn');
    let navbarDrawerCloseButton = document.getElementById('close-navbar-drawer-btn');
    let navbarContent = document.getElementById('navbar-content');
    let userActionsContent = document.getElementById('user-actions');
    let navbarDrawer = document.getElementById('navbar-drawer');
    let navbarDrawerContent = document.getElementById('navbar-drawer-content');
    let body = document.querySelector('body');

    if (navbarDrawer && navbar && body) {
        function mobileNavbarToggle() {
            navbar.classList.toggle('d-none');
            body.classList.toggle('overflow-hidden');
            navbarDrawer.classList.toggle('d-none');
        }

        if (navbarDrawerOpenButtonForUserActions && navbarDrawerContent && userActionsContent) {
            navbarDrawerOpenButtonForUserActions.addEventListener('click', function (event) {
                event.preventDefault();
                mobileNavbarToggle();
                navbarDrawerContent.innerHTML = userActionsContent.innerHTML;
            });
        }

        if (navbarDrawerOpenButton && navbarDrawerContent && navbarContent) {
            navbarDrawerOpenButton.addEventListener('click', function (event) {
                event.preventDefault();
                mobileNavbarToggle();
                navbarDrawerContent.innerHTML = navbarContent.innerHTML;

                const navDropdowns = document.querySelectorAll('#navbar-drawer .dropdown > span');
                navDropdowns.forEach(element => {
                    element.addEventListener('click', function (event) {
                        element.classList.toggle('active');
                        element.nextElementSibling.classList.toggle('dropdown-active');

                        let dropDownIndicator = this.querySelector('.dropdown-indicator');
                        dropDownIndicator.classList.toggle('bi-chevron-up');
                        dropDownIndicator.classList.toggle('bi-chevron-down');
                    })
                });
            });
        }

        if (navbarDrawerCloseButton) {
            navbarDrawerCloseButton.addEventListener('click', function (event) {
                event.preventDefault();
                mobileNavbarToggle();
            });
        }
    }

    /**
     * Sidebar drawer open/close
     */
    const sidebar = document.getElementById('sidebar');
    const sidebarDrawer = document.getElementById('sidebar-drawer');
    const sidebarDrawerContent = document.getElementById('sidebar-drawer-content');
    const openSidebarDrawerBtn = document.getElementById('open-sidebar-drawer-btn');
    const closeSidebarDrawerBtn = document.getElementById('close-sidebar-drawer-btn');

    if (sidebar && sidebarDrawer && sidebarDrawerContent && openSidebarDrawerBtn && closeSidebarDrawerBtn) {
        if (window.innerWidth < 1200) {
            openSidebarDrawerBtn.classList.add('active');
        } else {
            openSidebarDrawerBtn.classList.remove('active');
        }

        window.addEventListener('resize', function () {
            if (window.innerWidth < 1200) {
                openSidebarDrawerBtn.classList.add('active');
                sidebar.style.display = 'none';
            } else {
                openSidebarDrawerBtn.classList.remove('active');
                sidebar.style.display = 'block';
            }
        });

        openSidebarDrawerBtn.addEventListener('click', function (event) {
            sidebarDrawer.classList.toggle('d-none');
            openSidebarDrawerBtn.classList.toggle('d-none');
            sidebarDrawerContent.innerHTML = sidebar.innerHTML;
        });

        closeSidebarDrawerBtn.addEventListener('click', function (event) {
            sidebarDrawer.classList.toggle('d-none');
            sidebarDrawerContent.classList.toggle('d-none');
        });
    }
}

function initMap() {
    let mapContainer = document.getElementById("map");
    const schoolLocation = {lat: 49.656, lng: 26.971};

    if (mapContainer) {
        const map = new google.maps.Map(mapContainer, {
            zoom: 12,
            center: schoolLocation,
        });

        const marker = new google.maps.Marker({
            position: schoolLocation,
            map: map,
        });
    }
}

window.initMap = initMap;

window.addEventListener('load', windowLoad);

document.addEventListener('DOMContentLoaded', DOMContentLoaded);
