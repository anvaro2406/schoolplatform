<?php

namespace Database\Seeders;

use App\Models\EmployeeProperty;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = User::firstOrCreate(
            ['email' => 'anvaro2406@gmail.com'],
            [
                'photo_filepath' => '',
                'firstname' => 'Андрій',
                'lastname' => 'Романов',
                'patronymic' => 'Валерійович',
                'gender' => 'male',
                'birth_date' => Carbon::parse('2002-06-24'),
                'address' => 'м. Львів, вул. Наукова 50',
                'phone' => '0981179799',
                'password' => Hash::make('4096'),
            ]
        );

        $user->assignRole('admin');

        EmployeeProperty::firstOrCreate(
            ['user_id' => $user->id],
            [
                'position' => 'Системний адміністратор',
                'science_degree' => 'Бакалавр',
                'experience' => '1',
                'biography' => null,
            ]
        );
    }
}
