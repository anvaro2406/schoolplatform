<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::firstOrCreate(['name' => 'create user']);
        Permission::firstOrCreate(['name' => 'update user']);
        Permission::firstOrCreate(['name' => 'delete user']);
        Permission::firstOrCreate(['name' => 'view student']);
        Permission::firstOrCreate(['name' => 'update student']);
        Permission::firstOrCreate(['name' => 'view user list']);
        Permission::firstOrCreate(['name' => 'create news']);
        Permission::firstOrCreate(['name' => 'update news']);
        Permission::firstOrCreate(['name' => 'delete news']);
        Permission::firstOrCreate(['name' => 'create event']);
        Permission::firstOrCreate(['name' => 'update event']);
        Permission::firstOrCreate(['name' => 'delete event']);
        Permission::firstOrCreate(['name' => 'create subject']);
        Permission::firstOrCreate(['name' => 'update subject']);
        Permission::firstOrCreate(['name' => 'delete subject']);
        Permission::firstOrCreate(['name' => 'create class']);
        Permission::firstOrCreate(['name' => 'update class']);
        Permission::firstOrCreate(['name' => 'delete class']);
        Permission::firstOrCreate(['name' => 'create journal']);
        Permission::firstOrCreate(['name' => 'update journal']);
        Permission::firstOrCreate(['name' => 'delete journal']);
        Permission::firstOrCreate(['name' => 'view journal']);
        Permission::firstOrCreate(['name' => 'update lessons progress']);
        Permission::firstOrCreate(['name' => 'view lessons progress']);
        Permission::firstOrCreate(['name' => 'view own schedule']);
        Permission::firstOrCreate(['name' => 'create schedule']);
        Permission::firstOrCreate(['name' => 'update schedule']);
        Permission::firstOrCreate(['name' => 'delete schedule']);
        Permission::firstOrCreate(['name' => 'view own diary']);
        Permission::firstOrCreate(['name' => 'view child diary']);
        Permission::firstOrCreate(['name' => 'create teacher feedback']);
        Permission::firstOrCreate(['name' => 'create study year']);

        Role::firstOrCreate(['name' => 'student'])
            ->givePermissionTo(['view own schedule', 'view own diary']);

        Role::firstOrCreate(['name' => 'parent'])
            ->givePermissionTo(['view child diary']);

        Role::firstOrCreate(['name' => 'teacher'])
            ->givePermissionTo(
                'update lessons progress', 'view lessons progress', 'create teacher feedback',
                'create subject', 'update subject', 'view own schedule'
            );

        Role::firstOrCreate(['name' => 'head-teacher'])
            ->givePermissionTo([
                'view journal', 'view lessons progress', 'view student', 'update student'
            ]);

        Role::firstOrCreate(['name' => 'organizer'])
            ->givePermissionTo([
                'view journal', 'view lessons progress',
                'create news', 'update news', 'delete news',
                'create event', 'update event', 'delete event'
            ]);

        Role::firstOrCreate(['name' => 'educational-deputy'])
            ->givePermissionTo(
                'view journal', 'create journal', 'update journal', 'delete journal', 'view lessons progress',
                'create schedule', 'update schedule', 'delete schedule',
                'create subject', 'update subject',
                'create class', 'update class', 'create study year', 'view student', 'update student'
            );

        $maximumPermissions = Permission::whereNotIn('name', [
            'view child diary', 'view own diary', 'view lessons progress', 'update lessons progress', 'view own schedule',
            'create teacher feedback'
        ])->get();

        Role::firstOrCreate(['name' => 'secretary'])
            ->givePermissionTo($maximumPermissions);

        Role::firstOrCreate(['name' => 'director'])
            ->givePermissionTo($maximumPermissions);

        Role::firstOrCreate(['name' => 'admin'])
            ->givePermissionTo($maximumPermissions);
    }
}
