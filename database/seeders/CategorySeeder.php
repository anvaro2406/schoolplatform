<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::firstOrCreate(
            ['name' => 'Загальне']
        );
        Category::firstOrCreate(
            ['name' => 'Математика']
        );
        Category::firstOrCreate(
            ['name' => 'Футбол']
        );
        Category::firstOrCreate(
            ['name' => 'Бібліотека']
        );
        Category::firstOrCreate(
            ['name' => 'Фізика']
        );
    }
}
