<?php

namespace Database\Seeders;

use App\Models\TimeSlot;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Seeder;

class TimeSlotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TimeSlot::upsert([
                [
                    'id' => 1,
                    'start_time' => '08:00:00',
                    'end_time' => '08:45:00',
                ],
                [
                    'id' => 2,
                    'start_time' => '08:55:00',
                    'end_time' => '09:40:00',
                ],
                [
                    'id' => 3,
                    'start_time' => '09:50:00',
                    'end_time' => '10:35:00',
                ],
                [
                    'id' => 4,
                    'start_time' => '10:55:00',
                    'end_time' => '11:40:00',
                ],
                [
                    'id' => 5,
                    'start_time' => '12:00:00',
                    'end_time' => '12:45:00',
                ],
                [
                    'id' => 6,
                    'start_time' => '12:55:00',
                    'end_time' => '13:40:00',
                ],
                [
                    'id' => 7,
                    'start_time' => '13:50:00',
                    'end_time' => '14:35:00',
                ],
                [
                    'id' => 8,
                    'start_time' => '14:45:00',
                    'end_time' => '15:30:00',
                ],
            ], ['id'], ['start_time', 'end_time']
        );
    }
}
