<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abstract_subjects', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('school_program_id');
            $table->tinyInteger('class_number', false, true);
            $table->decimal('year_studyhours');
            $table->decimal('week_studyhours');
            $table->text('description');
            $table->string('title', 256);//TODO length
            $table->timestamps();
            $table->foreign('school_program_id')->references('id')->on('school_programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abstract_subjects');
    }
};
